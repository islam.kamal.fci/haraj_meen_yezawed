import 'package:flutter/material.dart';
import 'package:haraj/utils/app_colors.dart';


class CustomButton extends StatelessWidget {
  final Color btnColor;
  final String btnLbl;
  final Function onPressedFunction;
  final TextStyle btnStyle;
  final Widget prefixIcon;
  final Widget postfixIcon;
    final enableHorizontalMargin;
  final bool hasGradientColor;

  const CustomButton(
      {Key key,
      this.btnLbl,
       this.enableHorizontalMargin : true,
      this.onPressedFunction,
      this.btnColor,
      this.btnStyle,
      this.prefixIcon,
      this.hasGradientColor = true,
      this.postfixIcon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Container(
           height: 55,
        margin: EdgeInsets.symmetric(
            horizontal: enableHorizontalMargin ?
            MediaQuery.of(context).size.width * 0.07 : 0.0,
            vertical: MediaQuery.of(context).size.height * 0.01),
          child: InkWell(
            onTap: () =>
              onPressedFunction()
            ,
            child: Container(
                decoration: hasGradientColor
                    ? BoxDecoration(
                        borderRadius: BorderRadius.circular(11.0),
                        gradient: LinearGradient(
                          colors: [mainAppColor,
                  mainAppColor],
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                        ),
                        boxShadow: [
                            BoxShadow(
                              color: mainAppColor,
                              offset: Offset(0.0, 1.5),
                              blurRadius: 1.5,
                            ),
                          ]
                          )
                    : BoxDecoration(
                        border: Border.all(width: 1.0, color: mainAppColor),
                        borderRadius: BorderRadius.circular(25.0)),
                alignment: Alignment.center,
                child: prefixIcon != null
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          prefixIcon,
                          Container(
                              margin: EdgeInsets.symmetric(horizontal: 5),
                              child: Text(
                                '$btnLbl',
                                style: btnStyle == null
                                    ? Theme.of(context).textTheme.button
                                    : btnStyle,
                              ))
                        ],
                      )
                    : postfixIcon != null
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                  margin: EdgeInsets.symmetric(horizontal: 5),
                                  child: Text(
                                    '$btnLbl',
                                    style: btnStyle == null
                                        ? Theme.of(context).textTheme.button
                                        : btnStyle,
                                  )),
                              postfixIcon
                            ],
                          )
                        : Text(
                            '$btnLbl',
                            style: btnStyle == null
                                ? Theme.of(context).textTheme.button
                                : btnStyle,
                          )),
          ));
    });
  }
}

// class CustomButton extends StatelessWidget {
//   final Color btnColor;
//   final String btnLbl;
//   final Function onPressedFunction;
//   final TextStyle btnStyle;
//   final enableHorizontalMargin;

//   const CustomButton({
//     Key key,
//     this.btnLbl,
//     this.onPressedFunction,
//     this.btnColor,
//     this.btnStyle, this.enableHorizontalMargin : true,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//         decoration: BoxDecoration(
//           gradient: LinearGradient(
//             colors: [
//               Color(0xffFE585A),
//               Color(0xffFDB970),
//             ],
//             begin: Alignment.bottomCenter,
//             end: Alignment.topCenter,
//           ),
//           boxShadow: [
//             BoxShadow(
//               color: Colors.grey[500],
//               offset: Offset(0.0, 1.5),
//               blurRadius: 1.5,
//             ),
//           ]),
//         height: 60,
//         margin: EdgeInsets.symmetric(
//             horizontal: enableHorizontalMargin ?
//             MediaQuery.of(context).size.width * 0.07 : 0.0,
//             vertical: MediaQuery.of(context).size.height * 0.01),
//         child: Builder(
//             builder: (context) => RaisedButton(
              
//                   onPressed: () =>
//                     onPressedFunction()
//                   ,
//                   elevation: 0,
//                   shape: new RoundedRectangleBorder(
//                       borderRadius: new BorderRadius.circular(12.0)),
//                   // color: btnColor != null
//                   //     ? btnColor
//                   //     : Theme.of(context).primaryColor,
//                   child: Container(
//                       alignment: Alignment.center,
//                       child: new Text(
//                         '$btnLbl',
//                         style: btnStyle == null
//                             ? Theme.of(context).textTheme.button
//                             : btnStyle,
//                       )),
//                 )));
//   }
// }
