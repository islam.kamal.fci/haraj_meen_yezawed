import 'package:carousel_slider/carousel_slider.dart';

import 'package:flutter/material.dart';
import 'package:haraj/utils/app_colors.dart';
// import 'package:riyadhmarket/models/img.dart';

List<T> map<T>(List list, Function handler) {
  List<T> result = [];
  for (var i = 0; i < list.length; i++) {
    result.add(handler(i, list[i]));
  }

  return result;
}

class CarouselWithIndicator extends StatefulWidget {
  final List<dynamic> imgList;

  const CarouselWithIndicator({@required this.imgList});
  @override
  _CarouselWithIndicatorState createState() => _CarouselWithIndicatorState();
}

class _CarouselWithIndicatorState extends State<CarouselWithIndicator> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      CarouselSlider(
        options: CarouselOptions(
          height: 200,
          autoPlay: true,
          enlargeCenterPage: true,
          viewportFraction: 0.9,
          aspectRatio: MediaQuery.of(context).size.aspectRatio * 4.5,
          initialPage: 2,
          onPageChanged: (index, page) {
            setState(() {
              _current = index;
            });
          },
        ),
        items: map<Widget>(
          widget.imgList,
          (index, i) {
            return Container(
              // margin: EdgeInsets.symmetric(vertical: 5),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                child:  Image.network(
                   i.url,
                  // fit: BoxFit.fitWidth,
                  // width: MediaQuery.of(context).size.width,
                ),
              ),
            );
          },
        ).toList(),
      ),
      Positioned(
        bottom: 10,
        left: MediaQuery.of(context).size.width * 0.45,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: map<Widget>(
            widget.imgList,
            (index, url) {
              return Container(
                width: 8.0,
                height: 8.0,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _current == index ? mainAppColor : whiteColor),
              );
            },
          ),
        ),
      ),
    ]);
  }
}
