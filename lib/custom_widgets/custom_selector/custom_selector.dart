import 'package:flutter/material.dart';
import 'package:haraj/utils/app_colors.dart';


class CustomSelector extends StatelessWidget {
  final Widget title;
  final Widget suffixIcon;
   final bool hasHorizontalMargin;

  const CustomSelector({Key key, this.title, this.suffixIcon,
    this.hasHorizontalMargin:true,}) : super(key: key);

 
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Container(
    
        margin: EdgeInsets.symmetric(horizontal: 
        hasHorizontalMargin ?
           width* 0.07 :  0),
        height: 50,
     
        child: Column(
          children: [
            Row(
              children: <Widget>[

      Container(
                    width: width *0.5,
                    child:title
                  ),
               
              Spacer(),
            
              Container(margin: EdgeInsets.symmetric(horizontal: 5),
       child: suffixIcon != null ?
       suffixIcon
       :Container)
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Divider(
              color: hintColor,
              height: 10,
            )
          ],
        ),
      );
    
  }
}
