import 'package:flutter/material.dart';
import 'package:haraj/providers/adding_ad_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:validators/validators.dart';

mixin ValidationMixin<T extends StatefulWidget> on State<T> {
  String _password = '';

  String validateUserName(String userName) {
    if (userName.trim().length == 0) {
      return ' يرجى إدخال اسم المستخدم';
    } else if (userName.trim().length < 2) {
      return ' يرجى إدخال اسم المستخدم بشكل صحيح';
    }
    return null;
  }


  String validateContactReason(String reason) {
    if (reason.trim().length == 0) {
      return ' يرجى إدخال سبب الاتصال';
    }
    return null;
  }

  String validatePassword(String password) {
    _password = password;
    if (password.trim().length == 0)
      return ' يرجى إدخال كلمة المرور';
    else if (password.trim().length < 8) return ' يرجى إدخال كلمه سر أقوى ';

    return null;
  }

  String validateOldPassword(String oldPassword) {
    if (oldPassword.trim().length == 0) return 'يرجي إدخال كلمة المرور القديمة';

    return null;
  }

  String validateConfirmPassword(String confirmPassword) {
    if (confirmPassword.trim().length == 0)
      return ' يرجى إدخال تأكيد كلمة المرور';
    else if (_password != confirmPassword) return 'كلمة المرور غير متطابقة';

    return null;
  }

  String validateMsg(String message) {
    if (message.trim().length == 0) {
      return ' يرجى إدخال  نص الرسالة';
    }
    return null;
  }

  String validateLocation(String message) {
    if (message.trim().length == 0) {
      return ' يرجى إدخال الموقع';
    }
    return null;
  }

  String validateVote(String message) {
    if (message.trim().length == 0) {
      return ' يرجى إدخال  نص التقييم اولا';
    }
    return null;
  }

  String validateComment(String message) {
    if (message.trim().length <= 3) {
      return ' يرجى إدخال نص تعليق واضح اولا';
    }
    return null;
  }

  String validateReason(String message) {
    if (message.trim().length == 0) {
      return 'يرجى كتابه الشكوى اولا';
    }
    return null;
  }

  bool checkValidationOfSignUp(AuthProvider authProvider) {
    if (authProvider.selectedCountry != null) {
      authProvider.setSelectedCountryError(false);
      return true;
    } else {
      authProvider.setSelectedCountryError(true);
      return false;
    }
  }

  bool checkValidationOfAddingAd(AddingAdProvider addingAdProvider) {
    if (addingAdProvider.selectedCity != null)
      addingAdProvider.setSelectedCityError(false);
    else
      addingAdProvider.setSelectedCityError(true);

    if (addingAdProvider.selectedCategory != null)
      addingAdProvider.setSelectedCategoryError(false);
    else
      addingAdProvider.setSelectedCategoryError(true);

    if (addingAdProvider.mainImg != null)
      addingAdProvider.setMainImgError(false);
    else
      addingAdProvider.setMainImgError(true);

    if (addingAdProvider.selectedCarModel != null &&
        (addingAdProvider.selectedCategory.id == 1 ||
            addingAdProvider.selectedCategory.name == 'حراج السيارات')) {
      addingAdProvider.setSelectedCarModelError(false);
    } else
      addingAdProvider.setSelectedCarModelError(true);

    if (addingAdProvider.selectedCity != null &&
        addingAdProvider.selectedCategory != null &&
        addingAdProvider.mainImg != null) {
      if (addingAdProvider.selectedCategory.id == 1 ||
          addingAdProvider.selectedCategory.name == 'حراج السيارات') {
        if (addingAdProvider.selectedCarModel != null)
          return true;
        else
          return false;
      } else
        return true;
    } else
      return false;
  }

  String validateEmail(String email) {
    if (email.trim().length == 0 || !isEmail(email)) {
      return ' يرجى إدخال البريد الالكتروني';
    }
    return null;
  }

  String validatePhoneNo(String value) {
    if(isEmail(value)){
      if (value.trim().length == 0 || !isEmail(value)) {
        return ' يرجى إدخال البريد الالكتروني';
      }
      return null;
    }else{
      Pattern pattern = r'^(05)(0|3|4|5|6|7|8|9)([0-9]{7})?$';
      RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value))
        return ' يرجى إدخال رقم الموبايل / البريد الالكتروني بشكل صحيح';
      else
        return null;
    }

  }

  String validateAddressName(String addressName) {
    if (addressName.trim().length == 0) {
      return ' يرجى إدخال  اسم العنوان';
    }
    return null;
  }

  String validateAdTitle(String title) {
    if (title.trim().length == 0) return ' يرجى إدخال عنوان اعلانك بشكل صحيح';

    return null;
  }

  String validateFuelType(String type) {
    if (type.trim().length == 0) return ' يرجى إدخال  نوع الوقود';

    return null;
  }

  String validatePrice(String price) {
    if (price.trim().length == 0)
      return ' يرجى إدخال سعر الإعلان';
    else if (double.parse(price) <= 0)
      return 'يرجى إدخال سعر الإعلان بشكل صحيح';

    return null;
  }

  String validateKiloMeter(String kiloMeter) {
    if (kiloMeter.trim().length == 0)
      return ' يرجى إدخال عدد الكيلو مترات المقطوعة';
    else if (double.parse(kiloMeter) <= 0)
      return ' يرجى إدخال عدد الكيلو مترات المقطوعة بشكل صحيح';

    return null;
  }

  String validateCommissionAmount(String price) {
    if (price.trim().length < 2)
      return ' يرجى إدخال المبلغ ';
    else if (double.parse(price) <= 0)
      return 'يرجى إدخال مبلغ العمولة بشكل صحيح';

    return null;
  }

  String validateBankName(String value) {
    if (value.trim().length == 0) return 'يرجى إدخال البنك ';

    return null;
  }

  String validateConverterTime(String value) {
    if (value.trim().length == 0) return 'يرجى إدخال وقت التحويل ';

    return null;
  }

  String validateConverterName(String value) {
    if (value.trim().length == 0) return 'يرجى إدخال اسم المحول ';

    return null;
  }

  String validateAdNumber(String value) {
    if (value.trim().length == 0)
      return ' يرجى إدخال رقم الإعلان';
    else if (int.parse(value) <= 0) return 'يرجى إدخال رقم الإعلان بشكل صحيح';

    return null;
  }

  String validateContent(String val) {
    if (val.trim().length == 0) return 'يرجى إدخال محتوى الاعلان اولا';

    return null;
  }

  String validateLandmark(String landmark) {
    if (landmark.trim().length == 0) return ' يرجى إدخال المعلم';

    return null;
  }

  String validateOthers(String others) {
    if (others.trim().length == 0) {
      return ' يرجى إدخال تعليمات اخرى';
    }
    return null;
  }
}
