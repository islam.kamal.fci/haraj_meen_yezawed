import 'package:flutter/material.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/shared_preferences/shared_preferences_helper.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:provider/provider.dart';


class LogoutDialog extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    return  AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              backgroundColor: greenColor,
              title: Text(
                "هل تريد تسجيل الخروج؟",
                style: TextStyle( color: whiteColor,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
              ),
              actions: [
                FlatButton(
                  onPressed: () => Navigator.pop(context),
                  child: Text('إلغاء',
                  style:  TextStyle(
                          color: mainAppColor,
                          fontSize: 14,
                          fontWeight: FontWeight.bold
                        ),),
                ),
                FlatButton(
                      onPressed: () {
                        
          SharedPreferencesHelper.remove("token");              
Navigator.pop(context);
  Navigator.of(context).pushNamedAndRemoveUntil(
         '/login_screen', (Route<dynamic> route) => false);
         authProvider.setCurrentUser(null);
         
                      },
                      child: Text('موافق',
                         style:  TextStyle(
                          color: mainAppColor,
                          fontSize: 14,
                          fontWeight: FontWeight.bold
                        ),),
                    )
              ],
            );
  }
}