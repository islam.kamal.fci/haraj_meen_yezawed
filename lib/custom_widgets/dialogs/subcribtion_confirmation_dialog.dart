
import 'package:flutter/material.dart';
import 'package:haraj/utils/app_colors.dart';




class SubcribtionConfirmationDialog extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    
    return   AlertDialog(
      backgroundColor: Colors.transparent,
   contentPadding: EdgeInsets.fromLTRB(0.0,0.0,0.0,0.0),
      shape: RoundedRectangleBorder(
        
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      content: SingleChildScrollView(
        child:  Column(
        
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
              
              Container(
          margin: EdgeInsets.symmetric(vertical: 10),
                child: Icon(Icons.check_circle,color: mainAppColor,size: 70,))
           ,Text('تم إرسال طلب الإشتراك',style: TextStyle(
             color: mainAppColor,fontWeight: FontWeight.w700,fontSize: 15
           ),),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            child:  Text('تم ارسال طلب اشتراكك إلى الإدارة بنجاح وسيتم التحقق من البيانات اولا',style: TextStyle(
             color: Colors.white,fontSize: 14,
             height: 1.6,
             
             fontWeight: FontWeight.w500
           ),
           textAlign: TextAlign.center,
           ),
          )
          ],
        )),
      
    );
   
  }
}
