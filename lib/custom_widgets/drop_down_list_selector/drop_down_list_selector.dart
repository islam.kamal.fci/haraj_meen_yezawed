import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:haraj/utils/app_colors.dart';


class DropDownListSelector extends StatefulWidget {
  final List<dynamic> dropDownList;
  final String hint;
  final dynamic value;
  final Function onChangeFunc;
  final bool elementHasDefaultMargin;

  const DropDownListSelector(
      {Key key,
      this.dropDownList,
      this.hint,
      this.value,
      this.onChangeFunc,
      this.elementHasDefaultMargin: true})
      : super(key: key);
  @override
  _DropDownListSelectorState createState() => _DropDownListSelectorState();
}

class _DropDownListSelectorState extends State<DropDownListSelector> {
  @override
  Widget build(BuildContext context) {
    return Container(
           height: 48,
        padding: const EdgeInsets.all(8.0),
        margin: widget.elementHasDefaultMargin
            ? EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.07)
            : EdgeInsets.symmetric(horizontal: 0),
        decoration: BoxDecoration(
         color:  greenColor,
          borderRadius: BorderRadius.circular(10.0),
          border: Border.all(color: mainAppColor),
        ),
        child: DropdownButtonHideUnderline(
          child: DropdownButton<dynamic>(
            dropdownColor:  whiteColor,
            isExpanded: true,
            hint: Row(
              children: [
                Image.asset(
                  'assets/images/map-pin-2-line.png',
                  color: mainAppColor,
                ),
                SizedBox(width: 2,),
                Text(
                  widget.hint,
                  style: TextStyle(
                      color: whiteColor,
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      fontFamily: 'ArefRuqaa'),
                ),
              ],
            ),
            focusColor: mainAppColor,
            icon: Icon(
                   Icons.keyboard_arrow_down,
                   size: 20,
                   color: mainAppColor,
                 ),
            style: TextStyle(
                fontSize: 10,
                color: mainAppColor,
                fontWeight: FontWeight.w400,
                fontFamily: 'ArefRuqaa'),
            items: widget.dropDownList,
            onChanged: widget.onChangeFunc,
            value: widget.value,
          ),
        ));
  }
}
