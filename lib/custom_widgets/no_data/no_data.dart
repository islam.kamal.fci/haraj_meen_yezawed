import 'package:flutter/material.dart';
import 'package:haraj/utils/app_colors.dart';

class NoData extends StatelessWidget {
  final String message;

  const NoData({Key key, this.message}) : super(key: key);
  @override
  Widget build(BuildContext context) {

    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.not_interested,
          size: 70,
          color: mainAppColor,
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          child: Text(
            message,
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.w400, fontSize: 15),
          ),
        ),
      ],
    ));
  }
}
