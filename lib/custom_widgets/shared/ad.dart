import 'package:flutter/material.dart';
import 'package:haraj/models/product.dart';
import 'package:haraj/providers/ad_details_provider.dart';
import 'package:haraj/providers/navigation_provider.dart';
import 'package:haraj/providers/view_provider.dart';
import 'package:haraj/ui/home/widgets/chip_ad.dart';
import 'package:haraj/ui/profile/announced_profile_screen.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:provider/provider.dart';

class Ad extends StatefulWidget {
  final Product product;
  final bool fromMyAds;
  final bool isFavourite;
  final Function onPressDelete;

  const Ad(
      {Key key,
      this.fromMyAds: false,
      this.product,
      this.isFavourite: false,
      this.onPressDelete})
      : super(key: key);
  @override
  _AdState createState() => _AdState();
}

class _AdState extends State<Ad> {
  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    NavigationProvider navigationProvider =
        Provider.of<NavigationProvider>(context);
    final width = MediaQuery.of(context).size.width;
    ViewProvider viewProvider = Provider.of<ViewProvider>(context);

    return Container(
      color: viewProvider.viewedProducts.contains(widget.product.id)
          ? greenColor
          : greenColor,
      child: InkWell(
        onTap: () {
          Provider.of<AdDetailsProvider>(context, listen: false)
              .setProductId(widget.product.id);
          if (widget.isFavourite) {
            navigationProvider.upadateNavigationIndex(0, context);
            Navigator.pushNamed(context, '/ad_details_screen');
          } else
            Navigator.pushNamed(context, '/ad_details_screen');
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,

          children: [
            Container(
                margin: EdgeInsets.symmetric(horizontal: 15,vertical: 15),
                width: 95,
                height: 95,

                child: ClipRRect(
                  borderRadius: BorderRadius.circular(15.0),
                  child: Image.network(
                    widget.product.image,
                    fit: BoxFit.fill,
                  ),
                )),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 7),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                          width: width * 0.4,
                          child: Text(
                            widget.product.title,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                height: 1.4,
                                color: whiteColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 14),
                          ),
                        ),
                        widget.product.isSpecial
                            ? Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: width * 0.04),
                                child: Icon(
                                  Icons.star,
                                  color: Color(0xffF7B900),
                                ))
                            : Container(),
                        widget.isFavourite
                            ? InkWell(
                                child: Icon(
                                  Icons.delete,
                                  color: whiteColor,
                                  size: 30,
                                ),
                                onTap: () => widget.onPressDelete(),
                              )
                            : Container()
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        widget.product.addressName,
                        style: TextStyle(color: whiteColor, fontSize: 13),
                      ),
                    ),
                    Row(
                        children: [
                      ChipAd(
                        color: secondary_color,
                        imgUrl: 'assets/images/time-line.png',
                        label: widget.product.createdAt,
                        txtColor: whiteColor,

                      ),
                      InkWell(
                          onTap: () {
                            if (authProvider.currentUser == null)
                              Navigator.pushNamed(context, '/login_screen');
                            else {
                              if (widget.fromMyAds)
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            AnnouncedProfileScreen(
                                              id: widget.product.userId
                                                  .toString(),
                                              name: widget.product.userName,
                                            )));
                              else
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          AnnouncedProfileScreen(
                                            id: widget.product.userId
                                                .toString(),
                                            name: widget.product.userName,
                                          )),
                                );
                            }
                          },
                          child: Container(
                            margin: EdgeInsets.only(right: 5),
                            child: ChipAd(
                              color: secondary_color,
                              imgUrl: 'assets/images/user-line.png',
                              label: widget.product.userName,
                              txtColor: whiteColor,
                            ),
                          ))
                    ])
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
