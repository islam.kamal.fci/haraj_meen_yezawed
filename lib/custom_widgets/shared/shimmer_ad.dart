import 'package:flutter/material.dart';
import 'package:haraj/ui/home/widgets/chip_design.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerAd extends StatefulWidget {
  @override
  _ShimmerAdState createState() => _ShimmerAdState();
}

class _ShimmerAdState extends State<ShimmerAd> {
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Shimmer.fromColors(
          baseColor: hintColor,
          highlightColor: Colors.grey[350],
          child: Container(
            //margin: EdgeInsets.only(right: 20),
            height: 95,
            width: 60,
            decoration: BoxDecoration(
                color: hintColor, borderRadius: BorderRadius.circular(10.0)),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 7),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Shimmer.fromColors(
                baseColor: hintColor,
                highlightColor: Colors.grey[350],
                child: ChipDesign(
                  color: hintColor,
                  label: '                                                  ',
                  txtColor: mainAppColor,
                ),
              ),
              Row(children: [
                Shimmer.fromColors(
                  baseColor: hintColor,
                  highlightColor: Colors.grey[350],
                  child: ChipDesign(
                    color: hintColor,
                    label: '                  ',
                    txtColor: mainAppColor,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 5),
                  child: Shimmer.fromColors(
                    baseColor: hintColor,
                    highlightColor: Colors.grey[350],
                    child: ChipDesign(
                      color: hintColor,
                      label: '                 ',
                      txtColor: mainAppColor,
                    ),
                  ),
                )
              ])
            ],
          ),
        )
      ],
    );
  }
}
