import 'package:flutter/material.dart';
import 'package:haraj/ui/home/widgets/chip_design.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerChip extends StatefulWidget {
  @override
  _ShimmerChipState createState() => _ShimmerChipState();
}

class _ShimmerChipState extends State<ShimmerChip> {
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: hintColor,
      highlightColor: Colors.grey[350],
      child: ChipDesign(
        color: hintColor,
        label: '                  ',
        txtColor: mainAppColor,
      ),
    );
  }
}
