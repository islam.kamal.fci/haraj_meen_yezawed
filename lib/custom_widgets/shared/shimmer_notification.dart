import 'package:flutter/material.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerNotification extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Shimmer.fromColors(
          baseColor: hintColor,
          highlightColor: Colors.grey[300],
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: width *0.04),
            height: 50,
            width: 50,
            decoration: BoxDecoration(
                color: hintColor, borderRadius: BorderRadius.circular(10.0)),
          ),
        ),
         Shimmer.fromColors(
                baseColor: hintColor,
                highlightColor: Colors.grey[350],
                child
              : Container(
                  decoration: BoxDecoration(
                      color: hintColor,
              borderRadius: BorderRadius.circular(5.0)),
          
                height: 50,
                width: width * 0.75,
              
              )
    
              ),
     
      ],
    );
  }
}
