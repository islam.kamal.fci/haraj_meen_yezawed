import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:haraj/locale/app_localizations.dart';
import 'package:haraj/providers/adding_ad_provider.dart';
import 'package:haraj/providers/app_language.dart';
import 'package:haraj/providers/notification_provider.dart';
import 'package:haraj/providers/rate_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/providers/ad_details_provider.dart';
import 'package:haraj/providers/cities_provider.dart';
import 'package:haraj/providers/favourite_provider.dart';
import 'package:haraj/providers/following_provider.dart';
import 'package:haraj/providers/home_provider.dart';
import 'package:haraj/providers/navigation_provider.dart';
import 'package:haraj/providers/static_pages_provider.dart';
import 'package:haraj/providers/subscription_provider.dart';
import 'package:haraj/providers/user_ads_provider.dart';
import 'package:haraj/providers/editing_ad_provider.dart';
import 'package:haraj/providers/chat_provider.dart';
import 'package:haraj/providers/view_provider.dart';
import 'package:haraj/providers/bank_transfer_provider.dart';
import 'package:haraj/test.dart';
import 'package:haraj/theme/style.dart';
import 'package:haraj/utils/routes.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]).then((_) {
    run();
  });
}

void run() async {
  AppLanguage appLanguage = AppLanguage();
  await appLanguage.fetchLocale();
  runApp(MyApp());
  // DevicePreview(
  //   builder: (context) => MyApp(),
  // );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (_) => AppLanguage(),
          ),
          ChangeNotifierProvider(
            create: (_) => NavigationProvider(),
          ),
          ChangeNotifierProvider(
            create: (_) => ViewProvider(),
          ),
          ChangeNotifierProvider(
            create: (_) => AuthProvider(),
          ),
          ChangeNotifierProxyProvider<AuthProvider, RateProvider>(
            create: (_) => RateProvider(),
            update: (_, auth, appState) => appState..update(auth),
          ),
          ChangeNotifierProvider(
            create: (_) => CitiesProvider(),
          ),
          ChangeNotifierProxyProvider<AuthProvider, HomeProvider>(
            create: (ctx) => HomeProvider(),
            update: (_, auth, home) => home..update(auth),
          ),
          ChangeNotifierProxyProvider<AuthProvider, FavouriteProvider>(
            create: (_) => FavouriteProvider(),
            update: (_, auth, fav) => fav..update(auth),
          ),
          ChangeNotifierProxyProvider<AuthProvider, StaticPagesProvider>(
            create: (_) => StaticPagesProvider(),
            update: (_, auth, pages) => pages..update(auth),
          ),
          ChangeNotifierProxyProvider<AuthProvider, UserAdsProvider>(
            create: (_) => UserAdsProvider(),
            update: (_, auth, ads) => ads..update(auth),
          ),
          ChangeNotifierProxyProvider<AuthProvider, AdDetailsProvider>(
            create: (_) => AdDetailsProvider(),
            update: (_, auth, product) => product..update(auth),
          ),
          ChangeNotifierProxyProvider<AuthProvider, FollowingProvider>(
            create: (_) => FollowingProvider(),
            update: (_, auth, following) => following..update(auth),
          ),
          ChangeNotifierProxyProvider<AuthProvider, AddingAdProvider>(
            create: (_) => AddingAdProvider(),
            update: (_, auth, addingAdProvider) =>
                addingAdProvider..update(auth),
          ),
          ChangeNotifierProxyProvider<AuthProvider, BankTransferProvider>(
            create: (_) => BankTransferProvider(),
            update: (_, auth, bankTranserProvider) =>
                bankTranserProvider..update(auth),
          ),
          ChangeNotifierProxyProvider<AuthProvider, EditingAdProvider>(
            create: (_) => EditingAdProvider(),
            update: (_, auth, editingAdProvider) =>
                editingAdProvider..update(auth),
          ),
          ChangeNotifierProxyProvider<AuthProvider, EditingAdProvider>(
            create: (_) => EditingAdProvider(),
            update: (_, auth, editingAdProvider) =>
                editingAdProvider..update(auth),
          ),
          ChangeNotifierProxyProvider<AuthProvider, NotificationProvider>(
            create: (_) => NotificationProvider(),
            update: (_, auth, notificationProvider) =>
                notificationProvider..update(auth),
          ),
          ChangeNotifierProxyProvider<AuthProvider, SubscriptionProvider>(
            create: (_) => SubscriptionProvider(),
            update: (_, auth, subscriptionProvider) =>
                subscriptionProvider..update(auth),
          ),
          ChangeNotifierProxyProvider<AuthProvider, ChatProvider>(
            create: (_) => ChatProvider(),
            update: (_, auth, chats) => chats..update(auth),
          ),
        ],
        child: Consumer<AppLanguage>(builder: (context, model, child) {
          return MaterialApp(
            locale: model.appLocal,
            supportedLocales: [
              Locale('en', 'US'),
              Locale('ar', ''),
            ],
            localizationsDelegates: [
              AppLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              DefaultCupertinoLocalizations.delegate
            ],
            debugShowCheckedModeBanner: false,
            title: 'حراج مين يزود',
            theme: themeData(),
            routes: routes,
     //       home: MyStatefulWidget(),
          );
        }));
  }
}
