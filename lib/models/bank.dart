class Bank {
    Bank({
        this.id,
        this.accountName,
        this.bankName,
        this.bankImage,
        this.accountNumber,
        this.internationalAccount,
        this.isSelected : false
    });

    int id;
    String accountName;
    String bankName;
    String bankImage;
    String accountNumber;
    bool isSelected;
    String internationalAccount;

    factory Bank.fromJson(Map<String, dynamic> json) => Bank(
        id: json["id"],
        accountName: json["account_name"],
        bankName: json["bank_name"],
        bankImage: json["bank_image"],
        accountNumber: json["account_number"],
        internationalAccount: json["International_Account"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "account_name": accountName,
        "bank_name": bankName,
        "bank_image": bankImage,
        "account_number": accountNumber,
        "International_Account": internationalAccount,
    };
}
