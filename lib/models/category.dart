// import 'package:haraj/models/product.dart';
// class Category {
//   Category({
//     this.id,
// this.isSelected: false,
//     this.name,
//     this.niceName,
//     this.products,
//   });

// bool isSelected;
//   int id;
//   String name;
//   String niceName;
//   List<Product> products;

//   factory Category.fromJson(Map<String, dynamic> json) => Category(
//         id: json["id"],
//         name: json["name"],
//         niceName: json["nice_name"],
//         products: json["products"] != null ?  List<Product>.from(
//             json["products"].map((x) => Product.fromJson(x))) : null,
//       );

//   Map<String, dynamic> toJson() => {
//         "id": id,
//         "name": name,
//         "nice_name": niceName,
//         "products": List<dynamic>.from(products.map((x) => x.toJson())),
//       };
// }

class Category {
    Category({
        this.id,
        this.name,
         this.isSelected: false,
        this.niceName,
        this.categoryImage,
        this.subCategories,
    });

bool isSelected;
    int id;
    String name;
    String niceName;
    String categoryImage;
    List<SubCategoryElement> subCategories;

    factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        name: json["name"],
        niceName: json["nice_name"],
        categoryImage: json["category_image"],
        subCategories:json["sub_categories"] != null ?
         List<SubCategoryElement>.from(json["sub_categories"].map((x) 
         => SubCategoryElement.fromJson(x))) : null,
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "nice_name": niceName,
        "category_image": categoryImage,
        "sub_categories": List<dynamic>.from(subCategories.map((x) => x.toJson())),
    };
}

class SubCategoryElement {
    SubCategoryElement({
        this.id,
        this.name,
          this.isSelected: false,
        this.niceName,
        this.childCategory,
    });
bool isSelected;
    int id;
    String name;
    String niceName;
    List<SubCategoryElement> childCategory;

    factory SubCategoryElement.fromJson(Map<String, dynamic> json) => SubCategoryElement(
        id: json["id"],
        name: json["name"],
        niceName: json["nice_name"],
        childCategory: json["child_category"] == null ? null : List<SubCategoryElement>.from(json["child_category"].map((x) => SubCategoryElement.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "nice_name": niceName,
        "child_category": childCategory == null ? null : List<dynamic>.from(childCategory.map((x) => x.toJson())),
    };
}