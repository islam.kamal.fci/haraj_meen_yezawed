class ChatModel {
  ChatModel({
    this.receiverId,
    this.name,
    this.image,
    this.lastMsg,
    this.createdAt,
  });

  var receiverId;
  var name;
  var image;
  var lastMsg;
  var createdAt;

  factory ChatModel.fromJson(Map<String, dynamic> json) => ChatModel(
        receiverId: json["receiver_id"],
        name: json["name"],
        image: json["image"],
        lastMsg: json["last_msg"],
        createdAt: json["created_at"],
      );

  Map<String, dynamic> toJson() => {
        "receiver_id": receiverId,
        "name": name,
        "image": image,
        "last_msg": lastMsg,
        "created_at": createdAt,
      };
}
