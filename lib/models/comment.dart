class Comment {
  Comment({
    this.userName,
    this.createdAt,
    this.body,
    this.createdAtTimeStamp,
  });

  String userName;
  String createdAt;
  String body;
  DateTime createdAtTimeStamp;

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
        userName: json["user_name"],
        createdAt: json["created_at"],
        body: json["body"],
        createdAtTimeStamp: DateTime.parse(json["created_at_time_stamp"]),
      );

  Map<String, dynamic> toJson() => {
        "user_name": userName,
        "created_at": createdAt,
        "body": body,
        "created_at_time_stamp": createdAtTimeStamp.toIso8601String(),
      };
}
