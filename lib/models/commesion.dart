class Commesion {
  Commesion({
    this.netCommission,
    this.appName,
    this.commercialRegister,
    this.taxNumber,
  });

  String netCommission;
  String appName;
  String commercialRegister;
  String taxNumber;

  factory Commesion.fromJson(Map<String, dynamic> json) => Commesion(
        netCommission: json["net_commission"],
        appName: json["app_name"],
        commercialRegister: json["commercial_register"],
        taxNumber: json["tax_number"],
      );

  Map<String, dynamic> toJson() => {
        "net_commission": netCommission,
        "app_name": appName,
        "commercial_register": commercialRegister,
        "tax_number": taxNumber,
      };
}
