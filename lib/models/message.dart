class Message {
  Message({
    this.content,
    this.senderId,
    this.receiverId,
    this.createdAt,
  });

  var content;
  var senderId;
  var receiverId;
  DateTime createdAt;

  factory Message.fromJson(Map<String, dynamic> json) => Message(
        content: json["content"].toString(),
        senderId: json["sender_id"].toString(),
        receiverId: json["receiver_id"].toString(),
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "content": content,
        "sender_id": senderId,
        "receiver_id": receiverId,
      };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Message &&
          runtimeType == other.runtimeType &&
          content == other.content &&
          receiverId == other.receiverId &&
          createdAt == other.createdAt &&
          senderId == other.senderId;

  @override
  String toString() {
    return 'Message{content: $content, senderId: $senderId, receiverId: $receiverId, createdAt: $createdAt}';
  }
}
