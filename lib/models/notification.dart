class NotificationModel {
    NotificationModel({
        this.id,
        this.title,
        this.body,
        this.type,
        this.createdAt,
        this.status,
        this.date,
    });

    int id;
    String title;
    String body;
    String type;
    DateTime createdAt;
    int status;
    String date;

    factory NotificationModel.fromJson(Map<String, dynamic> json) => NotificationModel(
        id: json["id"],
        title: json["title"],
        body: json["body"],
        type: json["type"],
        createdAt: DateTime.parse(json["created_at"]),
        status: json["status"],
        date: json["date"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "body": body,
        "type": type,
        "created_at": createdAt.toIso8601String(),
        "status": status,
        "date": date,
    };
}
