class PageContent {
  PageContent({
    this.pageName,
    this.content,
  });

  String pageName;
  List<Content> content;

  factory PageContent.fromJson(Map<String, dynamic> json) => PageContent(
        pageName: json["page_name"],
        content:
            List<Content>.from(json["content"].map((x) => Content.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "page_name": pageName,
        "content": List<dynamic>.from(content.map((x) => x.toJson())),
      };
}

class Content {
  Content({
    this.title,
    this.body,
  });

  String title;
  String body;

  factory Content.fromJson(Map<String, dynamic> json) => Content(
        title: json["title"],
        body: json["body"],
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "body": body,
      };
}
