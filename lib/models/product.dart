class DataModel {
  int currentPage;
  List<Product> data;
  int from;
  int lastPage;
  Null nextPageUrl;
  String path;
  String perPage;
  Null prevPageUrl;
  int to;
  int total;

  DataModel(
      {this.currentPage,
      this.data,
      this.from,
      this.lastPage,
      this.nextPageUrl,
      this.path,
      this.perPage,
      this.prevPageUrl,
      this.to,
      this.total});

  DataModel.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    if (json['data'] != null) {
      data = new List<Product>();
      json['data'].forEach((v) {
        data.add(new Product.fromJson(v));
      });
    }
    from = json['from'];
    lastPage = json['last_page'];
    nextPageUrl = json['next_page_url'];
    path = json['path'];
    perPage = json['per_page'];
    prevPageUrl = json['prev_page_url'];
    to = json['to'];
    total = json['total'];
  }
}

class Product {
  Product(
      {this.id,
      this.title,
      this.image,
      this.createdAt,
      this.userName,
      this.userId,
      this.addressId,
      this.latitude,
      this.longitude,
      this.addressName,
      this.createdAtTimeStamp,
      this.productPhone,
      this.productPrice,
      this.productDescription,
      this.hidePhone,
      this.hidePrice,
      this.productSubImages,
      this.categoryId,
      this.categoryName,
      this.isSpecial,
      this.isFavourite,
      this.isFollow,
      this.subCategoryId,
      this.childCategoryId,
      this.subCategoryName,
      this.childCategoryName,
      this.used,
      this.fuelType,
      this.kiloMeter,
      this.modelYear});

  int id;
  String title;
  int subCategoryId;
  int childCategoryId;
  String image;
  String createdAt;
  String userName;
  int userId;
  String addressId;
  String latitude;
  String longitude;
  String addressName;
  DateTime createdAtTimeStamp;
  String productPhone;
  String productPrice;
  String productDescription;
  bool hidePhone;
  bool hidePrice;
  List<ProductSubImage> productSubImages;
  int categoryId;
  String categoryName;
  bool isSpecial;
  bool isFavourite;
  bool isFollow;
  dynamic subCategoryName;
  dynamic childCategoryName;
  bool used;
  String fuelType;
  String kiloMeter;
  int modelYear;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
      id: json["id"],
      title: json["title"],
      image: json["image"],
      createdAt: json["created_at"],
      userName: json["user_name"],
      userId: json["user_id"],
      addressId: json["address_id"],
      latitude: json["latitude"],
      longitude: json["longitude"],
      addressName: json["address_name"],
      createdAtTimeStamp: DateTime.parse(json["created_at_time_stamp"]),
      productPhone: json["product_phone"],
      productPrice: json["product_price"],
      productDescription: json["product_description"],
      hidePhone: json["hide_phone"],
      hidePrice: json["hide_price"],
      productSubImages: List<ProductSubImage>.from(
          json["product_sub_images"].map((x) => ProductSubImage.fromJson(x))),
      categoryId: json["category_id"],
      categoryName: json["category_name"],
      isSpecial: json["is_special"],
      isFavourite: json["is_favourite"],
      isFollow: json["is_follow"],
      subCategoryId: json["sub_category_id"],
      childCategoryId: json["child_category_id"],
      subCategoryName: json["sub_category_name"],
      childCategoryName: json["child_category_name"],
      used: json["used"],
      fuelType: json["fuel_type"],
      kiloMeter: json["kilo_meter"],
      modelYear: json["model_year"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "image": image,
        "created_at": createdAt,
        "user_name": userName,
        "user_id": userId,
        "address_id": addressId,
        "latitude": latitude,
        "longitude": longitude,
        "address_name": addressName,
        "created_at_time_stamp": createdAtTimeStamp.toIso8601String(),
        "product_phone": productPhone,
        "product_price": productPrice,
        "product_description": productDescription,
        "hide_phone": hidePhone,
        "hide_price": hidePrice,
        "product_sub_images":
            List<dynamic>.from(productSubImages.map((x) => x.toJson())),
        "category_id": categoryId,
        "category_name": categoryName,
        "is_special": isSpecial,
        "is_favourite": isFavourite,
        "is_follow": isFollow,
        "sub_category_id": subCategoryId,
        "child_category_id": childCategoryId,
        "sub_category_name": subCategoryName,
        "child_category_name": childCategoryName,
        "used": used,
        "fuel_type": fuelType,
        "kilo_meter": kiloMeter,
        "model_year": modelYear
      };
}

class ProductSubImage {
  ProductSubImage({
    this.id,
    this.url,
  });

  int id;
  String url;

  factory ProductSubImage.fromJson(Map<String, dynamic> json) =>
      ProductSubImage(
        id: json["id"],
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "url": url,
      };
}
