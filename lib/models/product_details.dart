import 'package:haraj/models/comment.dart';

class ProductDetails {
  ProductDetails(
      {this.isFavourite,
      this.isFollow,
      this.categoryName,
      this.productId,
      this.productTitle,
      this.productDescription,
      this.productCreatedFrom,
      this.productImage,
      this.productCreatorName,
      this.productAddress,
      this.productCreatorId,
      this.comments,
      this.productSubImages,
      this.customProductsLikeThis,
      this.productPrice,
      this.productPhone,
      this.hidePrice,
      this.hidePhone,
      this.used,
      this.fuelType,
      this.kiloMeter,
      this.modelYear});

  bool isFavourite;
  bool isFollow;
  String categoryName;
  int productId;
  String productTitle;
  String productDescription;
  String productCreatedFrom;
  String productImage;
  String productCreatorName;
  String productAddress;
  int productCreatorId;
  List<Comment> comments;
  List<ProductSubImage> productSubImages;
  List<LikenProducts> customProductsLikeThis;
  String productPrice;
  String productPhone;
  bool used;
  bool hidePhone;
  bool hidePrice;
  String fuelType;
  String kiloMeter;
  int modelYear;
     

  factory ProductDetails.fromJson(Map<String, dynamic> json) => ProductDetails(
      isFavourite: json["is_favourite"],
      isFollow: json["is_follow"],
      categoryName: json["category_name"],
      productId: json["product_id"],
      productTitle: json["product_title"],
      productDescription: json["product_description"],
      productCreatedFrom: json["product_created_from"],
      productImage: json["product_image"],
      productCreatorName: json["product_creator_name"],
      productAddress: json["product_address"],
      productCreatorId: json["product_creator_id"],
      comments:
          List<Comment>.from(json["comments"].map((x) => Comment.fromJson(x))),
      customProductsLikeThis: List<LikenProducts>.from(
          json["custom_products_like_this"]
              .map((x) => LikenProducts.fromJson(x))),
      productSubImages: List<ProductSubImage>.from(
          json["product_sub_images"].map((x) => ProductSubImage.fromJson(x))),
      productPrice: json["product_price"],
      productPhone: json["product_phone"],
      used: json["used"],
      fuelType: json["fuel_type"],
      kiloMeter: json["kilo_meter"],
      modelYear: json["model_year"],
      hidePhone: json["hide_phone"],
      hidePrice: json["hide_price"],
       
      );

  Map<String, dynamic> toJson() => {
        "is_favourite": isFavourite,
        "is_follow": isFollow,
        "category_name": categoryName,
        "product_id": productId,
        "product_title": productTitle,
        "product_description": productDescription,
        "product_created_from": productCreatedFrom,
        "product_image": productImage,
        "product_creator_name": productCreatorName,
        "product_address": productAddress,
        "product_creator_id": productCreatorId,
        "comments": List<dynamic>.from(comments.map((x) => x.toJson())),
        "product_sub_images":
            List<dynamic>.from(productSubImages.map((x) => x.toJson())),
        "custom_products_like_this":
            List<dynamic>.from(customProductsLikeThis.map((x) => x.toJson())),
        "product_price": productPrice,
        "product_phone": productPhone,
        "used": used,
        "fuel_type": fuelType,
        "kilo_meter": kiloMeter,
        "model_year": modelYear,
           "hide_phone": hidePhone,
        "hide_price": hidePrice,
      };
}

class LikenProducts {
  LikenProducts({
    this.id,
    this.image,
  });

  int id;
  String image;

  factory LikenProducts.fromJson(Map<String, dynamic> json) => LikenProducts(
        id: json["id"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "image": image,
      };
}

class ProductSubImage {
  ProductSubImage({
    this.id,
    this.url,
  });

  int id;
  String url;

  factory ProductSubImage.fromJson(Map<String, dynamic> json) =>
      ProductSubImage(
        id: json["id"],
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "url": url,
      };
}
