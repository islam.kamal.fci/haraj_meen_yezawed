class Rate {
  Rate({
    this.id,
    this.vote,
    this.ratingText,
    this.voter,
    this.buyingProduct,
    this.createdAt,
    this.createdAtTimeStamp,
  });

  int id;
  int vote;
  String voter;
  String ratingText;
  int buyingProduct;
  String createdAt;
  DateTime createdAtTimeStamp;

  factory Rate.fromJson(Map<String, dynamic> json) => Rate(
        id: json["id"],
        vote: json["vote"],
        ratingText: json["rating_text"],
        voter: json["name"],
        buyingProduct: json["buying_product"],
        createdAt: json["created_at"],
        createdAtTimeStamp: DateTime.parse(json["created_at_time_stamp"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "vote": vote,
        "rating_text": ratingText,
        "buying_product": buyingProduct,
        "created_at": createdAt,
        "name": voter,
        "created_at_time_stamp": createdAtTimeStamp.toIso8601String(),
      };
}
