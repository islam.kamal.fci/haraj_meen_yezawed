class SubscriptionPlan {
    SubscriptionPlan({
        this.id,
        this.name,
        this.price,
        this.priceAfterDiscount,
        this.durationInDays,
        this.description,
        this.adsNumPeerDay,
    });

    int id;
    String name;
    String price;
    String priceAfterDiscount;
    int durationInDays;
    String description;
    int adsNumPeerDay;

    factory SubscriptionPlan.fromJson(Map<String, dynamic> json) => SubscriptionPlan(
        id: json["id"],
        name: json["name"],
        price: json["price"],
        priceAfterDiscount: json["price_after_discount"],
        durationInDays: json["duration_in_days"],
        description: json["description"],
        adsNumPeerDay: json["ads_num_peer_day"] ,
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "price": price,
        "price_after_discount": priceAfterDiscount,
        "duration_in_days": durationInDays,
        "description": description,
        "ads_num_peer_day": adsNumPeerDay,
    };
}
