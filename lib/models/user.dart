class User {
  User(
      {this.id,
      this.name,
      this.email,
      this.phone,
      this.addressId,
      this.accessToken,
      this.createdAt});

  int id;
  String name;
  String email;
  String phone;
  int addressId;
  String accessToken;
  String createdAt;

  factory User.fromJson(Map<String, dynamic> json) => User(
      id: json["id"],
      name: json["name"],
      email: json["email"],
      phone: json["phone"],
      addressId: json["address_id"],
      accessToken: json["access_token"],
      createdAt: json["created_at"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "phone": phone,
        "address_id": addressId,
        "access_token": accessToken,
        "created_at": createdAt
      };
}
