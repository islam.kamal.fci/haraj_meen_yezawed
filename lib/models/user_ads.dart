import 'package:haraj/models/product.dart';

class UserAds {
  UserAds({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.addressId,
    this.createdAt,
    this.following,
    this.products,
  });

  int id;
  String name;
  String email;
  String phone;
  int addressId;
  String createdAt;
  int following;
  List<Product> products;

  factory UserAds.fromJson(Map<String, dynamic> json) => UserAds(
        id: json["id"],
        name: json["name"],
        email: json["email"],
        phone: json["phone"],
        addressId: json["address_id"],
        createdAt: json["created_at"],
        following: json["following"],
        products: List<Product>.from(
            json["products"].map((x) => Product.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "phone": phone,
        "address_id": addressId,
        "created_at": createdAt,
        "following": following,
        "products": List<dynamic>.from(products.map((x) => x.toJson())),
      };
}
