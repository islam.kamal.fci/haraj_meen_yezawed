import 'package:flutter/material.dart';
import 'package:haraj/models/product_details.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/utils/urls.dart';
import 'package:url_launcher/url_launcher.dart';

class AdDetailsProvider extends ChangeNotifier {
  ApiProvider _apiProvider = ApiProvider();
  User _currentUser;
  void update(AuthProvider authProvider) {
    _currentUser = authProvider.currentUser;
  }

  int _productId;
  void setProductId(int value) {
    _productId = value;
    notifyListeners();
  }

  int get productId => _productId;

  ProductDetails _productDetails;

    ProductDetails get productDetails => _productDetails;

  Future<ProductDetails> getProductDetails() async {
  var response;
  if(_currentUser == null)
     response = await _apiProvider
        .get(Urls.SHOW_PRODUCT_URL + productId.toString(), header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
  
    });
    else
     response = await _apiProvider
        .get(Urls.SHOW_AUTH_PRODUCT_URL + productId.toString(), header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });

    if (response['status_code'] == 200) 
      _productDetails = ProductDetails.fromJson(response['response']['data']);
    
    return _productDetails;
  }

  //Action Call
  Future<void> makePhoneCall(String phone) async {
    if (await canLaunch('tel:$phone')) {
      await launch('tel:$phone');
    } else {
      throw 'Could not launch $phone';
    }
  }
}
