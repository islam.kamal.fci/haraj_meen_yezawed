import 'dart:io';
import 'dart:typed_data';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:haraj/models/category.dart';
import 'package:haraj/models/city.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class AddingAdProvider extends ChangeNotifier {
  bool _hideMobile = false;
  bool _hidePrice = false;

  User _currentUser;

  void update(AuthProvider authProvider) {
    _currentUser = authProvider.currentUser;
  }


  List<Asset> _adAssets = List<Asset>();



  City _selectedCity;

  void setSelectedCity(City value ,{bool notifyListener = true}) {
    _selectedCity = value;
   if(notifyListener)  notifyListeners();
  }

  City get selectedCity => _selectedCity;


  
    bool _selectedCityError = false;

  void setSelectedCityError (bool value ,{bool notifyListener = true}) {
    _selectedCityError = value;
   if(notifyListener)  notifyListeners();
  }

  bool get selectedCityError => _selectedCityError;


 Category _selectedCategory;

  void setSelectedCategory(Category 
  category ,{bool notifyListener = true}) {
    _selectedCategory = category;
   if(notifyListener)  notifyListeners();
  }

  Category get selectedCategory => _selectedCategory;



 Category _subSelectedCategory;

  void setSubSelectedCategory(Category 
  category ,{bool notifyListener = true}) {
    _subSelectedCategory = category;
   if(notifyListener)  notifyListeners();
  }

  Category get subSelectedCategory => _subSelectedCategory;


   Category _childSelectedCategory;

  void setChildSelectedCategory(Category 
  category ,{bool notifyListener = true}) {
    _childSelectedCategory = category;
   if(notifyListener)  notifyListeners();
  }

  Category get childSelectedCategory => _childSelectedCategory;
  
    bool _selectedCategoryError = false;

  void setSelectedCategoryError (bool value ,{bool notifyListener = true}) {
    _selectedCategoryError = value;
   if(notifyListener)  notifyListeners();
  }

  bool get selectedCategoryError => _selectedCategoryError;

     String _selectedCarModel;

  void setSelectedCarModel(String 
  carModel ,{bool notifyListener = true}) {
    _selectedCarModel = carModel;
   if(notifyListener)  notifyListeners();
  }

  String get selectedCarModel => _selectedCarModel;
  
    bool _selectedCarModelError = false;

  void setSelectedCarModelError (bool value ,{bool notifyListener = true}) {
    _selectedCarModelError = value;
   if(notifyListener)  notifyListeners();
  }

  bool get selectedCarModelError => _selectedCarModelError;
  
  //
  void setHideMobile(bool value,{bool notifyListener = true}) {
    _hideMobile = value;
      if(notifyListener)  notifyListeners();
  }

  bool get hideMobile => _hideMobile;


    bool _carIsUsed = false;

    void setCarIsUsed(bool value,{bool notifyListener = true}) {
    _carIsUsed = value;
      if(notifyListener)  notifyListeners();
  }

  bool get carIsUsed => _carIsUsed;

//
  void setHidePrice(bool value,{bool notifyListener = true}) {
    _hidePrice = value;
      if(notifyListener)  notifyListeners();
  }

  bool get hidePrice => _hidePrice;

  void setListOfAssets(List<Asset> assetList ,{bool notifyListener = true}) {
    _adAssets = assetList;
     if(notifyListener)  notifyListeners();
  }

  List<Asset> get listOfAssets => _adAssets;


    File _mainImg;


void setMainImg(File image,{bool notifyListener = true}) {
    _mainImg = image;
  if(notifyListener)  notifyListeners();
  }

  File get mainImg => _mainImg;


     bool _mainImageError = false;

  void setMainImgError (bool value ,{bool notifyListener = true}) {
    _mainImageError = value;
   if(notifyListener)  notifyListeners();
  }

  bool get mainImageError => _mainImageError;


  //pick imgs
  Future<void> loadAssets(BuildContext context) async {
    List<Asset> resultList = List<Asset>();
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 30,
        enableCamera: true,
        selectedAssets: listOfAssets,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#3F72DC",
          actionBarTitle: "select photos",
          allViewTitle: "All Photos",
          useDetailsView: false,
          statusBarColor: "#3F72DC",
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      print(e.toString());
    }
    
    // if(resultList.length > _allowedImgsNo)
    //   Commons.showToast(message:  '* يرجى اختيار صور فرعية لاتزيد عن  ' +
    //                                                 _allowedImgsNo
    //                                                     .toString(),
    //                                                      context: context);
    // else  
     setListOfAssets(resultList);
 

  
  }

//convert from assets to uploaded file images
  // Future<List<MultipartFile>> convertAssets(List<Asset> imgAssets) async {
  //   final imagesBytes = {};
  //     List<MultipartFile> images = new List<MultipartFile>();
  //   for (Asset asset in imgAssets) {
  //     final bytes = await asset.getByteData();
  //     imagesBytes[asset.identifier] = bytes.buffer.asUint8List();
  //   }
  //   images = imgAssets
  //       .map((a) => MultipartFile.fromBytes(imagesBytes[a.identifier],
  //           filename: a.name))
  //       .toList();
  //   return images;
  // }
  
  //  Future<List<MultipartFile>> convertAssets(List<Asset> imgs) async {
  //     List<MultipartFile> multipartImageList = new List<MultipartFile>();
 

  //   if (imgs != null) {
  //     for (Asset asset in imgs) {
  //       ByteData byteData = await asset.getByteData();
  //       List<int> imageData = byteData.buffer.asUint8List();
  //       MultipartFile multipartFile = new MultipartFile.fromBytes(
  //         imageData,
  //         filename: 'load_image',
  //             contentType: MediaType("image", "jpg"),
  //       );
  //       multipartImageList.add(multipartFile);
  //     }
  //   }
    
  //   return multipartImageList;
  // }

  
    Future<List<MultipartFile>> convertAssets(List<Asset> imgs) async {
       List<MultipartFile> multipartImageList = new List<MultipartFile>();
          if (null != imgs) {
            for (Asset asset in imgs) {
              ByteData byteData = await asset.getByteData();
              List<int> imageData = byteData.buffer.asUint8List();
              MultipartFile multipartFile = new MultipartFile.fromBytes(
                imageData,
                filename:  asset.identifier,
            //    contentType: MediaType("image", "jpg"),
              );
              multipartImageList.add(multipartFile);
            }
 

       }
        return multipartImageList;
    }
 
    

  

    void removeItemFromAssets(int index) {
    _adAssets.removeAt(index);
    notifyListeners();
  }

   

  // int _allowedImgsNo;


  // int get allowedImgsNo => _allowedImgsNo;

 

  // Future<int> getNoOfAllowedImgsOfAd() async {
  //   final response = await _apiProvider
  //       .get(Urls.ALLOWED_IMGS_NO_URL , header: {
  //     'Accept': 'application/json',
  //     'Content-Type': 'application/json',
  //     'Authorization': 'Bearer ${_currentUser.accessToken}'
  //   });

  //   if (response['status_code'] == 200) {
  //   _allowedImgsNo =  response['response']['data'];
  //   }

  //   return _allowedImgsNo;
   
  // }


}
