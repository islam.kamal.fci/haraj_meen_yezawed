import 'package:flutter/material.dart';
import 'package:haraj/shared_preferences/shared_preferences_helper.dart';

class AppLanguage extends ChangeNotifier {
  Locale _appLocale = Locale('ar');

  Locale get appLocal => _appLocale ?? Locale("ar");

  fetchLocale() async {
    String lang = await SharedPreferencesHelper.getUserLang();
    _appLocale = Locale(lang);
    return Null;
  }

  void changeLanguage(Locale type) async {
    if (_appLocale == type) {
      return;
    }
    if (type == Locale("ar")) {
      _appLocale = Locale("ar");
      await SharedPreferencesHelper.setUserLang('ar');
    } else {
      _appLocale = Locale("en");
      await SharedPreferencesHelper.setUserLang('en');
    }
    notifyListeners();
  }
}
