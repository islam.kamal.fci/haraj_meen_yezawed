import 'package:flutter/material.dart';
import 'package:haraj/models/city.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/shared_preferences/shared_preferences_helper.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';

class AuthProvider extends ChangeNotifier {
  User _currentUser;

  String _confimationCode;

  ApiProvider _apiProvider = ApiProvider();

  void setCurrentUser(User user) {
    _currentUser = user;
    notifyListeners();
  }

  User get currentUser => _currentUser;

  void updateUserName(String userName) {
    _currentUser.name = userName;
    notifyListeners();
  }



  void updateUserPhone(String userPhone) {
    _currentUser.phone = userPhone;
    notifyListeners();
  }
 

  void setConfirmationCode(String code) {
    _confimationCode = code;
    notifyListeners();
  }

  String get confirmationCode => _confimationCode;

  void updateUserEmail(String userEmail) {
    _currentUser.email = userEmail;
    notifyListeners();
  }

  void signOut(BuildContext context,) async {
    final response = await _apiProvider.post(Urls.LOGOUT_URL, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });
    if (response['status_code'] == 200) {
      Navigator.pop(context);
      Navigator.of(context).pushNamedAndRemoveUntil(
          '/login_screen', (Route<dynamic> route) => false);
      SharedPreferencesHelper.remove("user");
      Commons.showToast(
          message: "تم تسجيل الخروج", context: context, color: hintColor);
    } else 
      Commons.showToast(
          message: response['response']['error'],
          context: context,
          color: hintColor);
    
  }


  // Sign up 
   String _userPhone;
     void setUserPhone(String userPhone) {
    _userPhone = userPhone;
    notifyListeners();
  }
   
  String get userPhone => _userPhone;


    City _selectedCountry;

  void setSelectedCountry(City value ,{bool notifyListener = true}) {
    _selectedCountry = value;
   if(notifyListener)  notifyListeners();
  }

  City get selectedCountry => _selectedCountry;


  
    bool _selectedCountryError = false;

  void setSelectedCountryError (bool value ,{bool notifyListener = true}) {
    _selectedCountryError = value;
   if(notifyListener)  notifyListeners();
  }

  bool get selectedCountryError => _selectedCountryError;


}

