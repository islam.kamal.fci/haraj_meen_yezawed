import 'package:haraj/models/user.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:flutter/material.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/utils/urls.dart';
import 'package:haraj/models/bank.dart';

import '../shared_preferences/shared_preferences_helper.dart';
import '../utils/commons.dart';

class BankTransferProvider extends ChangeNotifier {
User _currentUser;

  void update(AuthProvider authProvider) {
    _currentUser = authProvider.currentUser;
  }
  ApiProvider _apiProvider = ApiProvider();

  List<Bank> _bankList = List<Bank>();

  List<Bank> get bankList => _bankList;

  int _index = 0;

  int get index => _index;

  void updateChangesOnBankList(int index) {
    _index = index;
    for (int i = 0; i < _bankList.length; i++) {
      _bankList[i].isSelected = false;
    }
    _bankList[index].isSelected = true;
    notifyListeners();
  }

  Future<List<Bank>> getBankList(BuildContext context) async {
    final response = await _apiProvider.get(Urls.ALL_BANKS_URL, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
            'Authorization': 'Bearer ${_currentUser.accessToken}'
    });
    if (response['status_code'] == 200) {
      Iterable iterable = response['response']['data'];
      _bankList = iterable.map((model) => Bank.fromJson(model)).toList();
      _bankList[0].isSelected = true;
   
    }
    else if(response["status_code" ] == 401){
      Commons.showError(context: context,message: 'يرجى تسجيل الدخول مجدداً',
      onTapOk: (){
 Navigator.pop(context);

                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login_screen', (Route<dynamic> route) => false);
                        SharedPreferencesHelper.remove("user");
      });
   
      } 
    return _bankList;
  }

 
}
