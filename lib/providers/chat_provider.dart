import 'package:flutter/material.dart';
import 'package:haraj/models/chat_model.dart';
import 'package:haraj/models/message.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/utils/urls.dart';
import 'package:rxdart/rxdart.dart';

import '../shared_preferences/shared_preferences_helper.dart';
import '../utils/commons.dart';

class ChatProvider extends ChangeNotifier {
  ApiProvider _apiProvider = ApiProvider();
  User _currentUser;

  List<Message> _listMessages = List<Message>();
  Message msg = Message();

  BehaviorSubject<Message> messageSubject = BehaviorSubject();

  Stream<Message> get messageStream => messageSubject.stream;

  Function(Message) get addMessageToStream => messageSubject.sink.add;

  void update(AuthProvider authProvider) {
    _currentUser = authProvider.currentUser;
  }

  void addToList(Message message) {
    _listMessages.add(message);
    notifyListeners();
  }

  void setListMessages(List<Message> msgs) {
    _listMessages = msgs;
    notifyListeners();
  }

  List<Message> get listOfMessages => _listMessages;

  Future<List<ChatModel>> getAllChats(BuildContext context) async {
    final response = await _apiProvider.get(Urls.ALL_USERS_CHATS, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });
    List<ChatModel> _list = List<ChatModel>();
    if (response['status_code'] == 200) {
      Iterable iterable = response['response']['data'];
      _list = iterable.map((model) => ChatModel.fromJson(model)).toList();

      print('list of city is :$_list');
    }else if(response["status_code" ] == 401){
      Commons.showError(context: context,message: 'يرجى تسجيل الدخول مجدداً',
      onTapOk: (){
 Navigator.pop(context);

                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login_screen', (Route<dynamic> route) => false);
                        SharedPreferencesHelper.remove("user");
      });
   
      }
    return _list;
  }

  Future<List<Message>> getSingleChat(String id) async {
    final response = await _apiProvider.get(Urls.SINGLE_CHAT_URL + id, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });
    messageSubject.skip(1);
    if (response['status_code'] == 200) {
      Iterable iterable = response['response']['data'];
      _listMessages = iterable.map((model) => Message.fromJson(model)).toList();
      print('list of msg :$_listMessages');
    }
    if (response['status_code'] == 400) {
      _listMessages = [];
    }
    _listMessages.forEach((msg) {
      addMessageToStream(msg);
    });
    return _listMessages;
  }

  void sendMessage(int senderId, int reciverId, String content) async {
    final response = await _apiProvider.post(Urls.SEND_CHAT_URL, body: {
      "receiver_id": reciverId,
      "sender_id": senderId,
      "content": content
    }, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });

    if (response['status_code'] == 200) {
      msg = Message.fromJson(response['response']['data']);
      addToList(msg);
      addMessageToStream(msg);
      print('msg :$msg');
    }
  }

  @override
  void dispose() {
    messageSubject.close();
  }
}
