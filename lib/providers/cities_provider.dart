import 'package:flutter/material.dart';
import 'package:haraj/models/city.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/utils/urls.dart';

import '../shared_preferences/shared_preferences_helper.dart';
import '../utils/commons.dart';

class CitiesProvider extends ChangeNotifier {
  ApiProvider _apiProvider = ApiProvider();

  Future<List<City>> getCityList(BuildContext context ,{
    bool allCitiesIsEnabled:  true
  }) async {
    final response = await _apiProvider.get(Urls.ALL_CITIES_URL, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    });
    List<City> cityList = List<City>();
    if (response['status_code'] == 200) {
      Iterable iterable = response['response']['data'];
      cityList = iterable.map((model) => City.fromJson(model)).toList();
       if(allCitiesIsEnabled) cityList.insert(
          0,
        City(
          name: 'كل المناطق'
        ));
    }
    else if(response["status_code" ] == 401)
      Commons.showError(context: context,message: 'يرجى تسجيل الدخول مجدداً',
      onTapOk: (){
 Navigator.pop(context);

                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login_screen', (Route<dynamic> route) => false);
                        SharedPreferencesHelper.remove("user");
      });
   
       
    return cityList;
  }

  Future<List<City>> getCountriesLists(BuildContext context) async {
    final response = await _apiProvider.get(Urls.ALL_COUNTRIES_URL, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    });
    List<City> _cityList = List<City>();
    if (response['status_code'] == 200) {
      Iterable iterable = response['response']['data'];
      _cityList = iterable.map((model) => City.fromJson(model)).toList();
    }else if(response["status_code" ] == 401){
      Commons.showError(context: context,message: 'يرجى تسجيل الدخول مجدداً',
      onTapOk: (){
 Navigator.pop(context);

                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login_screen', (Route<dynamic> route) => false);
                        SharedPreferencesHelper.remove("user");
      });
   
      }
    return _cityList;
  }
}
