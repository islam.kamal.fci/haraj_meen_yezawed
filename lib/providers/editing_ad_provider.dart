import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:haraj/models/product.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class EditingAdProvider extends ChangeNotifier {


  User _currentUser;

  void update(AuthProvider authProvider) {
    _currentUser = authProvider.currentUser;
  }
  ApiProvider _apiProvider = ApiProvider();

  List<Asset> _adAssets = List<Asset>();


  
  
  void updateHideMobile() {
    _product.hidePhone = !_product.hidePhone;
        notifyListeners();
  }

    void updateCarIsUsed() {
    _product.used= !_product.used;
        notifyListeners();
  }

  void updateProductDesc(String productDescription) {
    _product.productDescription = productDescription ;
        notifyListeners();
  }


  void updateHidePrice() {
       _product.hidePrice = ! _product.hidePrice;
        notifyListeners();
  }



  
   Product _product;

  void setProduct(Product 
  product ) {
    _product = product;
    notifyListeners();
  }

    Product get product => _product;

   void updateCity(int addressId,String addressName){
   _product.addressId = addressId.toString();
   _product.addressName = addressName;
   notifyListeners();
      }


void updateProductSubCategory(int subCategoryId,String subCategoryName){
   _product.subCategoryId = subCategoryId;
   _product.subCategoryName = subCategoryName;
   notifyListeners();
      }

      void updateProductChildCategory(int childCategoryId,String childCategoryName){
   _product.childCategoryId = childCategoryId;
   _product.childCategoryName = childCategoryName;
   notifyListeners();
      }

   void updateCategory(int catId,String catName){
   _product.categoryId = catId;
   _product.categoryName = catName;
   notifyListeners();
      }

       void updateProductTitle(String title){
   _product.title = title;

   notifyListeners();
      }


      void updateProductFuelType(String fueType){
   _product.fuelType = fueType;

   notifyListeners();
      }

      
      void updateProductKiloMeters(String kiloMeters){
   _product.kiloMeter = kiloMeters;

   notifyListeners();
      }


        void updateProductCarModel(int carModel){
   _product.modelYear = carModel;

   notifyListeners();
      }

   void updateProductPhone(String phoneNo){
   _product.productPhone = phoneNo;

   notifyListeners();
      }

       void updateProductPrice(String price){
   _product.productPrice = price;

   notifyListeners();
      }

       void removeImgOfProduct(int index) {
    _product.productSubImages.removeAt(index);
    notifyListeners();
  }

  void setListOfAssets(List<Asset> assetList ,{bool notifyListener = true}) {
    _adAssets = assetList;
     if(notifyListener)  notifyListeners();
  }

  List<Asset> get listOfAssets => _adAssets;


    File _mainImg;


void setMainImg(File image,{bool notifyListener = true}) {
    _mainImg = image;
  if(notifyListener)  notifyListeners();
  }

  File get mainImg => _mainImg;


     bool _mainImageError = false;

  void setMainImgError (bool value ,{bool notifyListener = true}) {
    _mainImageError = value;
   if(notifyListener)  notifyListeners();
  }

  bool get mainImageError => _mainImageError;


  //pick imgs
  Future<void> loadAssets(BuildContext context) async {
    List<Asset> resultList = List<Asset>();
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 30,
        enableCamera: true,
        selectedAssets: listOfAssets,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#3F72DC",
          actionBarTitle: "select photos",
          allViewTitle: "All Photos",
          useDetailsView: false,
          statusBarColor: "#3F72DC",
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      print(e.toString());
    }
    // if(resultList.length  + _product.productSubImages.length > _allowedImgsNo)
    //   Commons.showToast(message:  '* يرجى اختيار صور فرعية لاتزيد عن  ' +
    //                                                 _allowedImgsNo
    //                                                     .toString(),
    //                                                      context: context);
    // else  
     setListOfAssets(resultList);
 

  
  }

//convert from assets to uploaded file images
  Future<List<MultipartFile>> convertAssets(List<Asset> imgs) async {
      List<MultipartFile> multipartImageList = new List<MultipartFile>();
 

    if (imgs != null) {
      for (Asset asset in imgs) {
        ByteData byteData = await asset.getByteData();
        List<int> imageData = byteData.buffer.asUint8List();
        MultipartFile multipartFile = new MultipartFile.fromBytes(
          imageData,
          filename: 'load_image',
        );
        multipartImageList.add(multipartFile);
      }
    }
    return multipartImageList;
  }

    void removeItemFromAssets(int index) {
    _adAssets.removeAt(index);
    notifyListeners();
  }

   



}
