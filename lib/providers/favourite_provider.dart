import 'package:flutter/material.dart';
import 'package:haraj/models/product.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';

import '../shared_preferences/shared_preferences_helper.dart';

class FavouriteProvider extends ChangeNotifier {
  ApiProvider _apiProvider = ApiProvider();
  User _currentUser;

  void update(AuthProvider authProvider) {
    _currentUser = authProvider.currentUser;
  }

  List<int> _favList = List<int>();

  void removeFavItem(int id) {
    _favList.remove(id);
    notifyListeners();
  }

  void addFavItem(int id) {
    _favList.add(id);
    notifyListeners();
  }

  void updateFavList(int id) {
    _favList.add(id);
  }

  List<int> get favList => _favList;

  Future<List<Product>> getFavouriteList(BuildContext context) async {
    final response = await _apiProvider.get(Urls.FAVOURITE_URL, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });
    List<Product> _list = List<Product>();
    if (response['status_code'] == 200) {
      Iterable iterable = response['response']['data'];
      _list = iterable.map((model) => Product.fromJson(model)).toList();
      _list
          .forEach((e) => !_favList.contains(e.id) ? _favList.add(e.id) : null);
      print('list of city is :$_list');
    }else if(response["status_code" ] == 401){
      Commons.showError(context: context,message: 'يرجى تسجيل الدخول مجدداً',
      onTapOk: (){
 Navigator.pop(context);

                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login_screen', (Route<dynamic> route) => false);
                        SharedPreferencesHelper.remove("user");
      });
   
      }
    return _list;
  }

  Future<void> toggleFavItem(BuildContext context, int id) async {
    final response =
        await _apiProvider.put(Urls.TOGGLE_FAV_URL + id.toString(), header: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });
    if (response['status_code'] == 200) {
      print('sucess');
    }
  }
}
