import 'package:flutter/material.dart';
import 'package:haraj/models/product.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/utils/urls.dart';

import '../shared_preferences/shared_preferences_helper.dart';
import '../utils/commons.dart';

class FollowingProvider with ChangeNotifier {
  ApiProvider _apiProvider = ApiProvider();
  User _currentUser;
  List<int> _adsList = List<int>();
  List<int> _followingList = List<int>();
  void update(AuthProvider authProvider) {
    _currentUser = authProvider.currentUser;
  }

//ads
  void addToListAds(int id) {
    _adsList.add(id);
    notifyListeners();
  }

  void removeFromListAds(int id) {
    _adsList.remove(id);
    notifyListeners();
  }

  List<int> get listIdsOfAds => _adsList;
//following users
  void addToListFollowing(int id) {
    _followingList.add(id);
    notifyListeners();
  }

  void removeFromListFollowing(int id) {
    _followingList.remove(id);
    notifyListeners();
  }

  List<int> get listIdsOfFollowing => _followingList;

  Future<List<User>> getFollowingUsers(BuildContext context) async {
    final response = await _apiProvider.get(Urls.FOLLOWING_USERS_URL, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });
    List<User> _list = List<User>();
    if (response['status_code'] == 200) {
      Iterable iterable = response['response']['data'];
      _list = iterable.map((model) => User.fromJson(model)).toList();
      _list.forEach((e) =>
          !_followingList.contains(e.id) ? _followingList.add(e.id) : null);
      print('following list : $_followingList');
    }
    else if(response["status_code" ] == 401){
      Commons.showError(context: context,message: 'يرجى تسجيل الدخول مجدداً',
      onTapOk: (){
 Navigator.pop(context);

                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login_screen', (Route<dynamic> route) => false);
                        SharedPreferencesHelper.remove("user");
      });
   
      }
    return _list;
  }

  Future<List<Product>> getFollowingAds() async {
    final response = await _apiProvider.get(Urls.FOLLOWING_ADS_URL, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });
    List<Product> _list = List<Product>();
    if (response['status_code'] == 200) {
      Iterable iterable = response['response']['data'];
      _list = iterable.map((model) => Product.fromJson(model)).toList();
      _list
          .forEach((e) => !_adsList.contains(e.id) ? _adsList.add(e.id) : null);
      print('ads list: $_adsList');
    }
    return _list;
  }

  Future<void> toggleFollowing(String id) async {
    final response =
        await _apiProvider.post(Urls.TOGGLE_FOLLOWING_URL + id, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });
    if (response['status_code'] == 200) {
    } else {}
    notifyListeners();
  }

  Future<void> toggleAd(String id) async {
    final response = await _apiProvider.post(Urls.TOGGLE_AD_URL + id, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });
    if (response['status_code'] == 200) {
    } else {}
    notifyListeners();
  }
}
