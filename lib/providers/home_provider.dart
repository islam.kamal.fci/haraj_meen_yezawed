import 'package:flutter/material.dart';
import 'package:haraj/models/category.dart';
import 'package:haraj/models/city.dart';
import 'package:haraj/models/product.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/utils/urls.dart';
import 'package:rxdart/rxdart.dart';
import '../shared_preferences/shared_preferences_helper.dart';
import '../utils/commons.dart';
import 'dart:async';

class HomeProvider extends ChangeNotifier {
  ApiProvider _apiProvider = ApiProvider();
  User _currentUser;

  void update(AuthProvider authProvider) {
    _currentUser = authProvider.currentUser;
  }

//filter
  bool _islatest = false;

  void enableTheLatest(bool value) {
    _islatest = value;
    notifyListeners();
  }

  bool get isLatest => _islatest;

  bool _hideSelection = false;

  void setHideSelection(bool value, {bool isNotifyListener: true}) {
    _hideSelection = value;
    if (isNotifyListener) notifyListeners();
  }

  bool get hideSelection => _hideSelection;

  double _userLatitude;

  void updateUserLatitude(double value, {bool isNotifyListener: false}) {
    _userLatitude = value;
    if (isNotifyListener) notifyListeners();
  }

  double get userLatitude => _userLatitude;

  double _userLongitude;

  void updateUserLongitude(double value, {bool isNotifyListener: false}) {
    _userLongitude = value;
    if (isNotifyListener) notifyListeners();
  }

  double get userLongitude => _userLongitude;

  City _selectedCity;

  void setSelectedCity(City city, {bool isNotifyListener: false}) {
    _selectedCity = city;
    if (isNotifyListener) notifyListeners();
  }

  City get selectedCity => _selectedCity;

  //  Category _lastSelectedCategory;

  List<Category> _categoryList = List<Category>();

  List<Category> get categoryList => _categoryList;

  int _mainCategoryIndex = 0;

  int get mainCategoryIndex => _mainCategoryIndex;

  void setMainCategoryIndex(int index) {
    _mainCategoryIndex = index;
  }

  void updateChangesOnMainCategoriesList(int index) {
    if (index != 0)
      _enableSearch = true;
    else {
      _enableSearch = false;
      _selectedCity = null;
      _subject.addStream(getProductList(homecontext).asStream());

      _controller.listen((notification) => loadProducts(notification));
    }
    _mainCategoryIndex = index;
    _subCategoryIndex = null;
    _childCategoryIndex = null;
    for (int i = 0; i < _categoryList.length; i++) {
      _categoryList[i].isSelected = false;
    }
    _categoryList[index].isSelected = true;
    _currentPageSearch = 1;
    // _subCategoryIndex = 0;
    // if (_categoryList[index].subCategories.length > 0)
    //   updateChangesOnSubCategoriesList(0);
    clearProductList();
    clearSearchResultList();
    // _childCategoryIndex =0;
    // if(categoryList[index].subCategories[_subCategoryIndex].childCategory.length >0)

    notifyListeners();
  }

  int _currentPageSearch = 1;

  int get currentPageSearch => _currentPageSearch;
  void setCurrentPageSearch(int value, {bool notifyListener = true}) {
    _currentPageSearch = value;
    if (notifyListener) notifyListeners();
  }

  int _noOfPagesSearch = 1;

  int get noOfPagesSearch => _noOfPagesSearch;
  void setNoOfPagesSearch(int value) {
    _noOfPagesSearch = value;
    notifyListeners();
  }

  int _currentPageHome = 1;

  int get currentPageHome => _currentPageHome;
  void setCurrentPageHome(int value, {bool notifyListener = true}) {
    _currentPageHome = value;
    if (notifyListener) notifyListeners();
  }

  int _noOfPagesHome = 1;

  int get noOfPagesHome => _noOfPagesHome;
  void setNoOfPagesHome(int value) {
    _noOfPagesHome = value;
    notifyListeners();
  }

  int pageNumber = 1;
  double pixels = 0.0;

  ReplaySubject<List<Product>> _subject = ReplaySubject();

  final ReplaySubject<ScrollNotification> _controller = ReplaySubject();

 // Observable<List<Product>> get stream => _subject.stream;
  Sink<ScrollNotification> get sink => _controller.sink;

  BuildContext homecontext;

  // void addStream(BuildContext ctx) {
  //   homecontext = ctx;
  //   // if (_subject != null || _controller != null) {
  //   _subject.close();
  //   _controller.close();
  //   _subject.addStream(Observable.fromFuture(getProductList(ctx)));
  //   _controller.listen((notification) => loadProducts(notification));
  //   // }
  // }
  // HomeProvider(BuildContext ctx) {
  //   homecontext = ctx;
  //   _subject.addStream(Observable.fromFuture(getProductList(ctx)));

  //   _controller.listen((notification) => loadProducts(notification));
  // }

  Future<void> loadProducts([
    ScrollNotification notification,
  ]) async {
    if (notification.metrics.pixels == notification.metrics.maxScrollExtent &&
        pixels != notification.metrics.pixels) {
      pixels = notification.metrics.pixels;

      pageNumber++;
      List<Product> list = await getProductList(homecontext);
      _subject.sink.add(list);
    }
  }

  // void dispose() {
  //   _controller.close();
  //   _subject.close();
  // }

  int _subCategoryIndex;

  int get subCategoryIndex => _subCategoryIndex;

  void updateChangesOnSubCategoriesList(int index) {
    _subCategoryIndex = index;
    for (int i = 0;
        i < _categoryList[_mainCategoryIndex].subCategories.length;
        i++) {
      _categoryList[_mainCategoryIndex].subCategories[i].isSelected = false;
    }
    _categoryList[_mainCategoryIndex].subCategories[index].isSelected = true;

    // _childCategoryIndex = 0;
    // if (_categoryList[_mainCategoryIndex]
    //         .subCategories[_subCategoryIndex]
    //         .childCategory
    //         .length >
    //     0) updateChangesOnChildCategoriesList(0);

    clearProductList();
    clearSearchResultList();
    // _categoryList[_mainCategoryIndex].subCategories[_subCategoryIndex].childCategory[_childCategoryIndex].isSelected = true;
    notifyListeners();
  }

  int _childCategoryIndex;

  int get childCategoryIndex => _childCategoryIndex;

  void updateChangesOnChildCategoriesList(int index) {
    _childCategoryIndex = index;
    for (int i = 0;
        i <
            _categoryList[_mainCategoryIndex]
                .subCategories[_subCategoryIndex]
                .childCategory
                .length;
        i++) {
      _categoryList[_mainCategoryIndex]
          .subCategories[_subCategoryIndex]
          .childCategory[i]
          .isSelected = false;
    }
    _categoryList[_mainCategoryIndex]
        .subCategories[_subCategoryIndex]
        .childCategory[index]
        .isSelected = true;
    clearProductList();
    clearSearchResultList();
    notifyListeners();
  }

  List<Product> _productList = List<Product>();

  List<Product> get productList => _productList;

  void clearProductList() {
    _productList.clear();
  }

  Future<List<Product>> getProductList(BuildContext context) async {
    var response;
    print("products pageNumber $pageNumber");
    if (_currentUser == null)
      response = await _apiProvider.get(
          Urls.ALL_PRODUCTS_UNAUTHENTCATED_URL + _currentPageHome.toString(),
          header: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          });
    else
      response = await _apiProvider.get(
          Urls.ALL_PRODUCTS_AUTH_URL + _currentPageHome.toString(),
          header: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ${_currentUser.accessToken}'
          });

    if (response['status_code'] == 200) {
      Iterable iterable = response['response']['data']['data'];

      _productList
          .addAll(iterable.map((model) => Product.fromJson(model)).toList());
      _noOfPagesHome = response['response']['data']['last_page'];
      notifyListeners();
    } else if (response["status_code"] == 401) {
      Commons.showError(
          context: context,
          message: 'يرجى تسجيل الدخول مجدداً',
          onTapOk: () {
            Navigator.pop(context);

            Navigator.of(context).pushNamedAndRemoveUntil(
                '/login_screen', (Route<dynamic> route) => false);
            SharedPreferencesHelper.remove("user");
          });
    }
    return _productList;
  }

  Future<List<Category>> getCategoriesList(BuildContext context,
      {bool mainCategoryIsEnable: true}) async {
    final response = await _apiProvider.get(Urls.ALL_CATEGORIES_URL, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    });

    if (response['status_code'] == 200) {
      Iterable iterable = response['response']['data'];
      _categoryList =
          iterable.map((model) => Category.fromJson(model)).toList();
      if (mainCategoryIsEnable)
        _categoryList.insert(
            0,
            Category(
                name: 'الرئيسية',
                isSelected: true,
                subCategories: List<SubCategoryElement>()));
      _mainCategoryIndex = 0;
    } else if (response["status_code"] == 401) {
      Commons.showError(
          context: context,
          message: 'يرجى تسجيل الدخول مجدداً',
          onTapOk: () {
            Navigator.pop(context);

            Navigator.of(context).pushNamedAndRemoveUntil(
                '/login_screen', (Route<dynamic> route) => false);
            SharedPreferencesHelper.remove("user");
          });
    }
    return _categoryList;
  }

  Future<List<Category>> getSubCategoriesList(
      BuildContext context, int mainCategoryId) async {
    final response = await _apiProvider
        .get(Urls.SUB_CATEGORIES_URL + mainCategoryId.toString(), header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    });
    List<Category> subCategoryList = List<Category>();
    if (response['status_code'] == 200) {
      Iterable iterable = response['response']['data'];
      subCategoryList =
          iterable.map((model) => Category.fromJson(model)).toList();
    } else if (response["status_code"] == 401) {
      Commons.showError(
          context: context,
          message: 'يرجى تسجيل الدخول مجدداً',
          onTapOk: () {
            Navigator.pop(context);

            Navigator.of(context).pushNamedAndRemoveUntil(
                '/login_screen', (Route<dynamic> route) => false);
            SharedPreferencesHelper.remove("user");
          });
    }
    return subCategoryList;
  }

  Future<List<Category>> getChildCategoriesList(
      BuildContext context, int subCategoryId) async {
    final response = await _apiProvider
        .get(Urls.CHILD_CATEGORIES_URL + subCategoryId.toString(), header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    });
    List<Category> subCategoryList = List<Category>();
    if (response['status_code'] == 200) {
      Iterable iterable = response['response']['data'];
      subCategoryList =
          iterable.map((model) => Category.fromJson(model)).toList();
    } else if (response["status_code"] == 401) {
      Commons.showError(
          context: context,
          message: 'يرجى تسجيل الدخول مجدداً',
          onTapOk: () {
            Navigator.pop(context);

            Navigator.of(context).pushNamedAndRemoveUntil(
                '/login_screen', (Route<dynamic> route) => false);
            SharedPreferencesHelper.remove("user");
          });
    }
    return subCategoryList;
  }

  //search
  String _keySearch = '';

  void setKeySearch(String keySeacrh, {bool isNotifyListener: false}) {
    _keySearch = keySeacrh;
    if (isNotifyListener) notifyListeners();
  }

  String get keySearch => _keySearch;

  bool _enableLocation = false;

  void detectLocation(bool value, {bool isNotifyListener: false}) {
    _enableLocation = value;
    if (isNotifyListener) notifyListeners();
  }

  bool get enableLocation => _enableLocation;

  bool _enableSearch = false;

  void updateEnableSearch(bool value, {bool isNotifyListener: false}) {
    _enableSearch = value;
    print("_enableSearch $_enableSearch");
    if (isNotifyListener) notifyListeners();
  }

  bool get enableSearch => _enableSearch;

  //grid view
  bool _grid = false;
  setGrid(bool val) {
    _grid = val;
    notifyListeners();
  }

  bool get grid => _grid;
  List<Product> _searchResultList = List<Product>();
  List<Product> get searchResultList => _searchResultList;

  void clearSearchResultList() {
    _searchResultList.clear();
  }

  int _selectedCarModel;

  void setSelectedCarModel(int carModel, {bool notifyListener = true}) {
    _selectedCarModel = carModel;
    if (notifyListener) notifyListeners();
  }

  int get selectedCarModel => _selectedCarModel;

  Future<List<Product>> getSearchResultList(BuildContext context) async {
    Map<String, dynamic> map = new Map<String, dynamic>();
    {
      map['latest'] = _islatest;
      if (_mainCategoryIndex != 0) {
        map['category_id'] = _categoryList[_mainCategoryIndex].id.toString();

        if (_categoryList[_mainCategoryIndex].subCategories.length > 0 &&
            subCategoryIndex != null) {
          map['sub_category_id'] = _categoryList[_mainCategoryIndex]
              .subCategories[subCategoryIndex]
              .id
              .toString();

          if (_categoryList[_mainCategoryIndex]
                      .subCategories[subCategoryIndex]
                      .childCategory
                      .length >
                  0 &&
              childCategoryIndex != null)
            map['child_category_id'] = _categoryList[_mainCategoryIndex]
                .subCategories[subCategoryIndex]
                .childCategory[childCategoryIndex]
                .id
                .toString();
        }
      }

      map['text'] = _keySearch;

      if (_selectedCity != null && _selectedCity.name != 'كل المناطق')
        map['city_id'] = _selectedCity.id;

      if (_enableLocation) {
        map['lat'] = _userLatitude.toString();
        map['long'] = _userLongitude.toString();
      }

      if ((_categoryList[mainCategoryIndex].id == 1 ||
              _categoryList[mainCategoryIndex].name == 'حراج السيارات') &&
          _selectedCarModel != null)
        map['model_year'] = _selectedCarModel.toString();
      print("_currentPageSearch $_currentPageSearch");
      var response;
      if (_currentUser == null)
        response = await _apiProvider.post(
            Urls.SEARCH_ALL_UNAUTH_URL + _currentPageSearch.toString(),
            body: map,
            header: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            });
      else
        response = await _apiProvider.post(
            Urls.SEARCH_ALL_AUTH_URL + _currentPageSearch.toString(),
            body: map,
            header: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ${_currentUser.accessToken}'
            });

      if (response['status_code'] == 200) {
        Iterable iterable = response['response']['data']['data'];
        _noOfPagesSearch = response['response']['data']['last_page'];
        _searchResultList
            .addAll(iterable.map((model) => Product.fromJson(model)).toList());
        notifyListeners();
        print("_searchResultList $_searchResultList");
        print("_noOfPagesSearch $_noOfPagesSearch");
      } else if (response["status_code"] == 401)
        Commons.showError(
            context: context,
            message: 'يرجى تسجيل الدخول مجدداً',
            onTapOk: () {
              Navigator.pop(context);

              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/login_screen', (Route<dynamic> route) => false);
              SharedPreferencesHelper.remove("user");
            });

      return _searchResultList;
    }
  }
}
