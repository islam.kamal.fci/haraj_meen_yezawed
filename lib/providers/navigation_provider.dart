import 'package:flutter/material.dart';
import 'package:haraj/providers/home_provider.dart';
import 'package:haraj/ui/ad/adding_ad_screen.dart';
import 'package:haraj/ui/chat/chat_screen.dart';
import 'package:haraj/ui/favorites/favorites_screen.dart';
import 'package:haraj/ui/home/home_screen.dart';
import 'package:haraj/ui/more/more_screen.dart';
import 'package:provider/provider.dart';

class NavigationProvider extends ChangeNotifier {
  int _navigationIndex = 0;

  void upadateNavigationIndex(int value, BuildContext ctx) {
    _navigationIndex = value;
    if (value == 0) {
      Provider.of<HomeProvider>(ctx, listen: false).pageNumber = 1;
    }
    notifyListeners();
  }

  int get navigationIndex => _navigationIndex;

  List<Widget> get screens => _screens;

  List<Widget> _screens = [
    HomeScreen(),
    FavoritesScreen(),
    AddingAdScreen(),
    ChatScreen(),
    MoreScreen()
  ];

  Widget get selectedContent => _screens[_navigationIndex];
}
