import 'package:flutter/material.dart';
import 'package:haraj/models/notification.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';

import '../shared_preferences/shared_preferences_helper.dart';

class NotificationProvider extends ChangeNotifier {
  ApiProvider _apiProvider = ApiProvider();
  User _currentUser;

  void update(AuthProvider authProvider) {
    _currentUser = authProvider.currentUser;
  }

  

  Future<List<NotificationModel>> getNotificationList(BuildContext context) async {
    final response = await _apiProvider.get(Urls.NOTIFICATION_URL, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Accept-Language':'ar',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });
     List<NotificationModel> notificationList = List<NotificationModel>();
    if (response['status_code'] == 200) {
      Iterable iterable = response['response']['notifications'];
      notificationList =
          iterable.map((model) => NotificationModel.fromJson(model)).toList();
    }
    else if(response["status_code" ] == 401)
      Commons.showError(context: context,message: 'يرجى تسجيل الدخول مجدداً',
      onTapOk: (){
 Navigator.pop(context);

                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login_screen', (Route<dynamic> route) => false);
                        SharedPreferencesHelper.remove("user");
      });
   
      
    return notificationList;
  }


}
