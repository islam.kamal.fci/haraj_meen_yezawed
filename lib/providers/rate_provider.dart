import 'package:flutter/material.dart';
import 'package:haraj/models/rate.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/utils/urls.dart';

import '../shared_preferences/shared_preferences_helper.dart';
import '../utils/commons.dart';

class RateProvider extends ChangeNotifier {
  bool _isSwear = false;
  bool _buyer = true;
  bool _recommended = true;
  ApiProvider _apiProvider = ApiProvider();
  User _currentUser;
  void update(AuthProvider authProvider) {
    _currentUser = authProvider.currentUser;
  }

  //
  void setIsSwear(bool value) {
    _isSwear = value;
    notifyListeners();
  }

  bool get isSwear => _isSwear;

//
  void setBuyer(bool value) {
    _buyer = value;
    notifyListeners();
  }

  bool get buyer => _buyer;
  //
  void setRecommended(bool value) {
    _recommended = value;
    notifyListeners();
  }

  bool get recommended => _recommended;
  reset() {
    _isSwear = false;
    _buyer = true;
    _recommended = true;
    notifyListeners();
  }

  Future<List<Rate>> getRates(String id, BuildContext context) async {
    final response =
        await _apiProvider.get(Urls.GET_ANNOUNCED_RATINGS + id, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });

    List<Rate> _list = List<Rate>();
    if (response['status_code'] == 200) {
      Iterable iterable = response['response']['data'];
      _list = iterable.map((model) => Rate.fromJson(model)).toList();
    }else if(response["status_code" ] == 401){
      Commons.showError(context: context,message: 'يرجى تسجيل الدخول مجدداً',
      onTapOk: (){
 Navigator.pop(context);

                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login_screen', (Route<dynamic> route) => false);
                        SharedPreferencesHelper.remove("user");
      });
   
      }
    return _list;
  }
}
