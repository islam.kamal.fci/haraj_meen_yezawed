import 'package:flutter/material.dart';
import 'package:haraj/models/commesion.dart';
import 'package:haraj/models/page_content.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/utils/urls.dart';

import '../shared_preferences/shared_preferences_helper.dart';
import '../utils/commons.dart';

class StaticPagesProvider extends ChangeNotifier {
  ApiProvider _apiProvider = ApiProvider();
  User _currentUser;
  void update(AuthProvider authProvider) {
    _currentUser = authProvider.currentUser;
  }

  Future<List<PageContent>> getStaticPage(BuildContext context) async {
    final response = await _apiProvider.get(Urls.STATIC_PAGES_URL, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });

    List<PageContent> _list = List<PageContent>();
    if (response['status_code'] == 200) {
      Iterable iterable = response['response']['data'];
      _list = iterable.map((model) => PageContent.fromJson(model)).toList();
    }else if(response["status_code" ] == 401){
      Commons.showError(context: context,message: 'يرجى تسجيل الدخول مجدداً',
      onTapOk: (){
 Navigator.pop(context);

                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login_screen', (Route<dynamic> route) => false);
                        SharedPreferencesHelper.remove("user");
      });
   
      }
    return _list;
  }

  Future<PageContent> getTerms(BuildContext context) async {
    final response = await _apiProvider.get(Urls.TERMS_URL, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    });
    PageContent _content;
    if (response['status_code'] == 200) {
      _content = PageContent.fromJson(response['response']['data']);
    }else if(response["status_code" ] == 401){
      Commons.showError(context: context,message: 'يرجى تسجيل الدخول مجدداً',
      onTapOk: (){
 Navigator.pop(context);

                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login_screen', (Route<dynamic> route) => false);
                        SharedPreferencesHelper.remove("user");
      });
   
      }
    return _content;
  }

  Future<Commesion> getCommesionDetails(BuildContext context) async {
    final response =
        await _apiProvider.get(Urls.GET_COMMETION_DETAILS, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });
    Commesion _content;
    if (response['status_code'] == 200) {
      _content = Commesion.fromJson(response['response']['data']);
    }else if(response["status_code" ] == 401){
      Commons.showError(context: context,message: 'يرجى تسجيل الدخول مجدداً',
      onTapOk: (){
 Navigator.pop(context);

                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login_screen', (Route<dynamic> route) => false);
                        SharedPreferencesHelper.remove("user");
      });
   
      }
    return _content;
  }
}
