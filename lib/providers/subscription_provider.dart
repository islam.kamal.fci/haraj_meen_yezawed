import 'package:flutter/material.dart';
import 'package:haraj/models/sub_plan.dart';
import 'package:haraj/models/subscription_plan.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/shared_preferences/shared_preferences_helper.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';

class SubscriptionProvider with ChangeNotifier {
  ApiProvider _apiProvider = ApiProvider();
  User _currentUser;
  void update(AuthProvider authProvider) =>
      _currentUser = authProvider.currentUser;
  

   String _endDate = '';

  void setEndDate(String endDate, {bool isNotifyListener: false}) {
    _endDate = endDate;
    if (isNotifyListener) notifyListeners();
  }

  String get endDate => _endDate;

  Future<SubscriptionPlan> getCurrentSubscriptionPlan(BuildContext context) async {
    final response = await _apiProvider.get(Urls.CURRENT_PLAN_URL, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });
    SubscriptionPlan subscriptionPlan ;
    if (response['status_code'] == 200 &&
     response['response']['data'] != null) {
         subscriptionPlan =
          SubscriptionPlan.fromJson(response['response']['data']);      
        _endDate =   response['response']['end_at'];
      
    } else if (response["status_code"] == 401)
      Commons.showError(
          context: context,
          message: 'يرجى تسجيل الدخول مجدداً',
          onTapOk: () {
            Navigator.pop(context);

            Navigator.of(context).pushNamedAndRemoveUntil(
                '/login_screen', (Route<dynamic> route) => false);
            SharedPreferencesHelper.remove("user");
          });

    return subscriptionPlan;
  }

  Future<List<SubPlan>> getSubScriptionPlanList(BuildContext context) async {
    final response = await _apiProvider.get(Urls.PLANS_URL, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Accept-Language':'ar',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });
     List<SubPlan> subscriptionPlanList = List<SubPlan>();
    if (response['status_code'] == 200) {
      Iterable iterable = response['response']['data'];
      subscriptionPlanList =
          iterable.map((model) => SubPlan.fromJson(model)).toList();
    }
    else if(response["status_code" ] == 401)
      Commons.showError(context: context,message: 'يرجى تسجيل الدخول مجدداً',
      onTapOk: (){
 Navigator.pop(context);

                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login_screen', (Route<dynamic> route) => false);
                        SharedPreferencesHelper.remove("user");
      });
   
      
    return subscriptionPlanList;
  }

   bool _bankTransferIsEnabled = false;

  void setBankTransferIsEnabled(bool value, {bool isNotifyListener: true}) {
    _bankTransferIsEnabled = value;
    if (isNotifyListener) notifyListeners();
  }

  bool get bankTransferIsEnabled => _bankTransferIsEnabled;

}
