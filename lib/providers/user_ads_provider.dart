import 'package:flutter/material.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/models/user_ads.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/utils/urls.dart';
import 'package:haraj/models/product.dart';

import '../shared_preferences/shared_preferences_helper.dart';
import '../utils/commons.dart';

class UserAdsProvider with ChangeNotifier {
  ApiProvider _apiProvider = ApiProvider();
  UserAds _userAds;
  User _currentUser;
  void update(AuthProvider authProvider) {
    _currentUser = authProvider.currentUser;
  }

  Future<UserAds> getUserAds(BuildContext context) async {
    final response = await _apiProvider
        .get(Urls.ANNOUNCED_ADS_URL + _currentUser.id.toString(), header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });

    if (response['status_code'] == 200) {
      _userAds = UserAds.fromJson(response['response']['data']);
    }
    else if(response["status_code" ] == 401){
      Commons.showError(context: context,message: 'يرجى تسجيل الدخول مجدداً',
      onTapOk: (){
 Navigator.pop(context);

                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login_screen', (Route<dynamic> route) => false);
                        SharedPreferencesHelper.remove("user");
      });
   
      }
    return _userAds;
  }

Future<List<Product>> getMyProductList(BuildContext context) async {
    print("============== here ==========");
    final response = await _apiProvider
        .get(Urls.ALL_MY_PRODUCTS, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });
 List<Product> productList = List<Product>();
    if (response['status_code'] == 200) {
      Iterable iterable = response['response']['data'];
      productList = iterable.map((model) => Product.fromJson(model)).toList();
    } else if (response["status_code"] == 401) {
      Commons.showError(
          context: context,
          message: 'يرجى تسجيل الدخول مجدداً',
          onTapOk: () {
            Navigator.pop(context);

            Navigator.of(context).pushNamedAndRemoveUntil(
                '/login_screen', (Route<dynamic> route) => false);
            SharedPreferencesHelper.remove("user");
          });
    }
    return productList;
  }
  

  Future<UserAds> getAnnouncedProfile(String id,BuildContext context) async {
    final response =
        await _apiProvider.get(Urls.ANNOUNCED_ADS_URL + id, header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_currentUser.accessToken}'
    });

    if (response['status_code'] == 200) 
      _userAds = UserAds.fromJson(response['response']['data']);
    
    return _userAds;
  }
}
