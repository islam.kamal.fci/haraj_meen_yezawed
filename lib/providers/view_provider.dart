import 'package:flutter/material.dart';

class ViewProvider extends ChangeNotifier {
  List<int> _viewedProducts = [];

  List<int> get viewedProducts => _viewedProducts;

  addItemToViewedProductsList(int id) =>
    _viewedProducts.add(id);
  

  
}
