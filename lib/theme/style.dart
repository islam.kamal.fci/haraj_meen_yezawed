import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:haraj/utils/app_colors.dart';

ThemeData themeData() {
  return ThemeData(
      primaryColor: greenColor,
      hintColor: hintColor,
      brightness: Brightness.light,
      buttonColor: greenColor,
      scaffoldBackgroundColor: Color(0xffFFFFFF),
      fontFamily: 'ArefRuqaa',
      cursorColor: greenColor,
      textTheme: TextTheme(
        // app bar style
        headline1: TextStyle(
            color: whiteColor, fontSize: 15, fontWeight: FontWeight.bold),
        // caption
        caption: TextStyle(
            fontFamily: 'ArefRuqaa',
            fontSize: 13,
            color: Colors.white,
            fontWeight: FontWeight.w700),
        //bold headline text
        bodyText1: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),

        // hint style of text form
        headline2: TextStyle(
            color: greenColor, fontSize: 17, fontWeight: FontWeight.w400),

        button: TextStyle(
            color: Colors.black, fontWeight: FontWeight.w700, fontSize: 15.0),
      ));
}
