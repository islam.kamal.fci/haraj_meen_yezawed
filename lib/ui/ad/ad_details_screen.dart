import 'dart:async';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/validation_mixin.dart';
import 'package:haraj/custom_widgets/no_data/no_data.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/models/product_details.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/ad_details_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/providers/favourite_provider.dart';
import 'package:haraj/providers/following_provider.dart';
import 'package:haraj/providers/navigation_provider.dart';
import 'package:haraj/providers/view_provider.dart';
import 'package:haraj/ui/ad/widgets/comment_widget.dart';
import 'package:haraj/ui/chat/chat_details_screen.dart';
import 'package:haraj/ui/home/widgets/chip_ad.dart';
import 'package:haraj/ui/ad/showing_all_comments_screen.dart';
import 'package:haraj/ui/profile/announced_profile_screen.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';
import 'package:provider/provider.dart';
import 'package:sliver_fab/sliver_fab.dart';

import '../../shared_preferences/shared_preferences_helper.dart';
import '../../utils/commons.dart';
import 'package:html/parser.dart' show parse;

class AdDetailsScreen extends StatefulWidget {
  @override
  _AdDetailsScreenState createState() => _AdDetailsScreenState();
}

class _AdDetailsScreenState extends State<AdDetailsScreen>
    with ValidationMixin {
  double _height, _width;
  Future<ProductDetails> _productDetails;
  bool initialRun = true, _isLoading = false, _isLoading2 = false;
  ApiProvider _apiProvider = ApiProvider();
  String _comment;
  String _reportAd;
  final _formKey = GlobalKey<FormState>();
  final _formReportKey = GlobalKey<FormState>();
  AuthProvider _authProvider;
  NavigationProvider _navigationProvider;

  String _parseHtmlString(String htmlString) {
    var document = parse(htmlString);
    String parsedString = parse(document.body.text).documentElement.text;
    return parsedString;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (initialRun) {
      _productDetails = Provider.of<AdDetailsProvider>(context, listen: false)
          .getProductDetails();
      _authProvider = Provider.of<AuthProvider>(context);
      _navigationProvider = Provider.of<NavigationProvider>(context);
      initialRun = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;

    return NetworkIndicator(
        child: PageContainer(
            child: Scaffold(
                backgroundColor: greenColor, body: _buildBodyWidget())));
  }

  Widget _buildBodyWidget() {
    ViewProvider viewProvider = Provider.of<ViewProvider>(context);
    final favouriteProvider = Provider.of<FavouriteProvider>(context);
    return FutureBuilder<ProductDetails>(
        future: _productDetails,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.active:
              return Text('');
            case ConnectionState.waiting:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.done:
              if (snapshot.hasError) {
                print(snapshot.error.toString());
                return Center(
                  child: Text('حدث خطأ ما'),
                );
              } else {
                viewProvider
                    .addItemToViewedProductsList(snapshot.data.productId);
                return Builder(
                    builder: (context) => SliverFab(
                          floatingWidget: Container(),
                          floatingPosition:
                              FloatingPosition(right: 12, left: 12),
                          slivers: [
                            SliverAppBar(
                              pinned: true,
                              expandedHeight: 200,
                              flexibleSpace: Stack(
                                children: <Widget>[
                                  Positioned.fill(
                                      child: Image.network(
                                          snapshot.data.productImage ?? '',
                                          fit: BoxFit.cover
                                          //  fit: BoxFit.fitHeight,
                                          ))
                                ],
                              ),
                              leading: InkWell(
                                onTap: () {
                                  _navigationProvider.upadateNavigationIndex(
                                      0, context);
                                  Navigator.pop(context);

                                  // Navigator.of(context).pushNamedAndRemoveUntil(
                                  //     '/navigation',
                                  //     (Route<dynamic> route) => false);
                                },
                                child:
                                    Image.asset('assets/images/arrow_back.png'),
                              ),
                              actions: [
                                InkWell(
                                  onTap: () {
                                    if (_authProvider.currentUser == null)
                                      Navigator.pushNamed(
                                          context, '/login_screen');
                                    else {
                                      if (favouriteProvider.favList
                                          .contains(snapshot.data.productId))
                                        favouriteProvider.removeFavItem(
                                            snapshot.data.productId);
                                      else
                                        favouriteProvider.addFavItem(
                                            snapshot.data.productId);

                                      favouriteProvider.toggleFavItem(
                                          context, snapshot.data.productId);
                                    }
                                  },
                                  child: Consumer<FavouriteProvider>(
                                    builder: (context, provider, _) {
                                      return Image.asset(
                                        provider.favList.contains(
                                                snapshot.data.productId)
                                            ? 'assets/images/favefull.png'
                                            : 'assets/images/heart.png',
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                            SliverList(
                              delegate: SliverChildListDelegate(<Widget>[
                                Container(
                                    margin: EdgeInsets.all(15),
                                    child: Text(
                                      snapshot.data.categoryName ?? '',
                                      style: TextStyle(
                                          color: whiteColor, fontSize: 12),
                                    )),
                                Container(
                                    margin:
                                        EdgeInsets.symmetric(horizontal: 15),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          width: _width * 0.8,
                                          child: Text(
                                            snapshot.data.productTitle ?? '',
                                            style: TextStyle(
                                                height: 1.4,
                                                color: Colors.white,
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        InkWell(
                                            onTap: () {
                                              _authProvider.currentUser == null
                                                  ? Navigator.pushNamed(
                                                      context, '/login_screen')
                                                  : showDialog(
                                                      context: context,
                                                      builder:
                                                          (context) =>
                                                              AlertDialog(
                                                                backgroundColor:
                                                                    greenColor,
                                                                shape: RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        new BorderRadius.circular(
                                                                            15)),
                                                                content: Form(
                                                                  key:
                                                                      _formReportKey,

                                                                  child:
                                                                      TextFormField(
                                                                    onChanged:
                                                                        (String
                                                                            val) {
                                                                      _reportAd =
                                                                          val;
                                                                    },
                                                                    validator:
                                                                        validateMsg,
                                                                    keyboardType:
                                                                        TextInputType
                                                                            .text,
                                                                    decoration: InputDecoration(
                                                                        focusColor:
                                                                            mainAppColor,fillColor: whiteColor,
                                                                        labelStyle: TextStyle(color:whiteColor ),
                                                                        labelText:
                                                                            'سبب الأبلاغ'),
                                                                  ),
                                                                ),
                                                                actions: <
                                                                    Widget>[
                                                                  _isLoading2
                                                                      ? Container(
                                                                          width:
                                                                              35,
                                                                          height:
                                                                              35,
                                                                          child:
                                                                              SpinKitRipple(color: mainAppColor),
                                                                        )
                                                                      : FlatButton(
                                                                          child:
                                                                              Text(
                                                                            "ارسال",
                                                                            style:
                                                                                TextStyle(fontSize: 13, color: mainAppColor),
                                                                          ),
                                                                          textColor:
                                                                              Colors.black,
                                                                          onPressed: () => _addReport(snapshot
                                                                              .data
                                                                              .productId),
                                                                        ),
                                                                ],
                                                              ));
                                            },
                                            child: Image.asset(
                                              'assets/images/report.png',
                                              color: mainAppColor,
                                            ))
                                      ],
                                    )),
                                snapshot.data.used != null
                                    ? Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 15, vertical: 10),
                                        child: Row(
                                          children: [
                                            Text(
                                              'نوع السيارة : ',
                                              style: TextStyle(
                                                  height: 1.4,
                                                  color: Colors.white,
                                                  fontSize: 13,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              snapshot.data.used
                                                  ? 'سيارة مستخدمة'
                                                  : 'سيارة جديدة',
                                              style: TextStyle(
                                                  height: 1.4,
                                                  color: Colors.white,
                                                  fontSize: 13,
                                                  fontWeight: FontWeight.bold),
                                            )
                                          ],
                                        ))
                                    : Container(),
                                snapshot.data.fuelType != null
                                    ? Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 15),
                                        child: Row(
                                          children: [
                                            Text(
                                              'نوع الوقود : ',
                                              style: TextStyle(
                                                  height: 1.4,
                                                  color: Colors.white,
                                                  fontSize: 13,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              snapshot.data.fuelType,
                                              style: TextStyle(
                                                  height: 1.4,
                                                  color: Colors.white,
                                                  fontSize: 13,
                                                  fontWeight: FontWeight.bold),
                                            )
                                          ],
                                        ))
                                    : Container(),
                                snapshot.data.kiloMeter != null
                                    ? Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 15, vertical: 10),
                                        child: Row(
                                          children: [
                                            Text(
                                              ' عدد الكيلو مترات : ',
                                              style: TextStyle(
                                                  height: 1.4,
                                                  color: Colors.white,
                                                  fontSize: 13,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              snapshot.data.kiloMeter,
                                              style: TextStyle(
                                                  height: 1.4,
                                                  color: Colors.white,
                                                  fontSize: 13,
                                                  fontWeight: FontWeight.bold),
                                            )
                                          ],
                                        ))
                                    : Container(),
                                snapshot.data.modelYear != null
                                    ? Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 15),
                                        child: Row(
                                          children: [
                                            Text(
                                              ' الموديل : ',
                                              style: TextStyle(
                                                  height: 1.4,
                                                  color: Colors.white,
                                                  fontSize: 13,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              snapshot.data.modelYear
                                                  .toString(),
                                              style: TextStyle(
                                                  height: 1.4,
                                                  color: Colors.white,
                                                  fontSize: 13,
                                                  fontWeight: FontWeight.bold),
                                            )
                                          ],
                                        ))
                                    : Container(),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 15, vertical: 10),
                                  child: Text(
                                    snapshot.data.productAddress ?? '',
                                    style: TextStyle(
                                        color: hintColor, fontSize: 13),
                                  ),
                                ),
                                Container(
                                    margin:
                                        EdgeInsets.symmetric(horizontal: 15),
                                    child: Text(
                                      "#" + snapshot.data.productId.toString(),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white,
                                          fontSize: 16),
                                    )),
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 15),
                                  child: Row(children: [
                                    ChipAd(
                                      color: secondary_color,
                                      imgUrl: 'assets/images/time-line.png',
                                      label: snapshot.data.productCreatedFrom,
                                      txtColor: whiteColor,
                                    ),
                                    InkWell(
                                      onTap: () =>
                                          _authProvider.currentUser == null
                                              ? Navigator.pushNamed(
                                                  context, '/login_screen')
                                              : Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        AnnouncedProfileScreen(
                                                      id: snapshot
                                                          .data.productCreatorId
                                                          .toString(),
                                                      name: snapshot.data
                                                          .productCreatorName,
                                                    ),
                                                  ),
                                                ),
                                      child: Container(
                                        margin: EdgeInsets.only(right: 5),
                                        child: ChipAd(
                                          color: secondary_color,
                                          imgUrl: 'assets/images/user-line.png',
                                          label:
                                              snapshot.data.productCreatorName,
                                          txtColor: whiteColor,
                                        ),
                                      ),
                                    )
                                  ]),
                                ),
                                snapshot.data.productPrice != ""
                                    ? Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 15, vertical: 5),
                                        child: Text(
                                          snapshot.data.productPrice + ' ريال',
                                          style: TextStyle(
                                              color: whiteColor,
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold),
                                        ))
                                    : Container(),
                                Container(
                                    width: _width,
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 15, vertical: 10),
                                    child: Text(
                                      _parseHtmlString(snapshot.data.productDescription) ,
                                      textAlign: TextAlign.justify,
                                      style: TextStyle(
                                          height: 1.5,
                                          color: whiteColor,
                                          fontSize: 16),
                                    )),
                                snapshot.data.productSubImages.length > 0
                                    ? Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                              margin: EdgeInsets.symmetric(
                                                  horizontal: 15, vertical: 10),
                                              child: Text(
                                                'صور فرعية',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              )),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          ListView.builder(
                                              shrinkWrap: true,
                                              physics:
                                                  NeverScrollableScrollPhysics(),
                                              itemCount: snapshot
                                                  .data.productSubImages.length,
                                              itemBuilder: (context, index) {
                                                return Container(
                                                  // width: _width,
                                                  //  height: 500,
                                                  margin: EdgeInsets.symmetric(
                                                    vertical: 10,
                                                    // horizontal:
                                                    //     _width *
                                                    //         0.01
                                                  ),
                                                  child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  10.0)),
                                                      child: InteractiveViewer(
                                                          minScale: 0.5,
                                                          maxScale: 10,
                                                          child: Image.network(
                                                            snapshot
                                                                .data
                                                                .productSubImages[
                                                                    index]
                                                                .url,
                                                            // height:
                                                            //     _height,
                                                            // width:
                                                            //     _width,
                                                            fit: BoxFit.fill,
                                                          ))),
                                                );
                                              }),
                                          SizedBox(
                                            height: 10,
                                          )
                                        ],
                                      )
                                    : Container(),
                                Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: _width * 0.05,
                                        vertical: 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'التعليقات',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            if (snapshot.data.comments.length >
                                                0)
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          ShowingAllCommentsScreen()));
                                          },
                                          child: Text(
                                            'عرض المزيد',
                                            style: TextStyle(
                                                color: whiteColor,
                                                fontSize: 13),
                                          ),
                                        )
                                      ],
                                    )),
                                snapshot.data.comments.length > 0
                                    ? ListView.builder(
                                        shrinkWrap: true,
                                        physics: NeverScrollableScrollPhysics(),
                                        itemCount:
                                            snapshot.data.comments.length > 3
                                                ? 3
                                                : snapshot.data.comments.length,
                                        itemBuilder: (context, index) {
                                          return CommentWidget(
                                              comment: snapshot
                                                  .data.comments[index]);
                                        })
                                    : Center(
                                        child: NoData(
                                        message: 'لا توجد تعليقات',
                                      )),
                                _authProvider.currentUser == null
                                    ? Container()
                                    : Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal: 15, vertical: 10),
                                        child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              SizedBox(width: 20),
                                              InkWell(
                                                onTap: () {
                                                  final provider = Provider.of<
                                                          FollowingProvider>(
                                                      context,
                                                      listen: false);
                                                  //1
                                                  provider.toggleAd(snapshot
                                                      .data.productId
                                                      .toString());
                                                  if (provider.listIdsOfAds
                                                      .contains(snapshot
                                                          .data.productId)) {
                                                    //2
                                                    provider.removeFromListAds(
                                                        snapshot
                                                            .data.productId);
                                                    Commons.showToast(
                                                        message:
                                                            'تم حذف الاعلان من قائمه المتابعه',
                                                        context: context);
                                                  } else {
                                                    provider.addToListAds(
                                                        snapshot
                                                            .data.productId);
                                                    Commons.showToast(
                                                        message:
                                                            'تم أضافه الاعلان لقائمه المتابعه',
                                                        context: context);
                                                  }
                                                },
                                                child: Container(
                                                    child: Row(
                                                  children: [
                                                    Image.asset(
                                                      'assets/images/rss-fill.png',
                                                      color: mainAppColor,
                                                    ),
                                                    Consumer<FollowingProvider>(
                                                        builder: (context,
                                                            provider, _) {
                                                      return Text(
                                                        !provider.listIdsOfAds
                                                                .contains(snapshot
                                                                    .data
                                                                    .productId)
                                                            ? 'متابعة الردود'
                                                            : 'الغاء متابعة',
                                                        style: TextStyle(
                                                            color: whiteColor,
                                                            fontSize: 14),
                                                      );
                                                    })
                                                  ],
                                                )),
                                              )
                                            ]),
                                      ),
                                _authProvider.currentUser == null
                                    ? Container()
                                    : Form(
                                        key: _formKey,
                                        child: _isLoading
                                            ? Container(
                                                width: 25,
                                                height: 25,
                                                child: SpinKitRipple(
                                                    color: mainAppColor),
                                              )
                                            : Container(
                                                margin: EdgeInsets.symmetric(
                                                    vertical: 10),
                                                child: CustomTextFormField(
                                                  fillColor: whiteColor,
                                                  enableBorder: true,
                                                  inputData: TextInputType.text,
                                                  validationFunc:
                                                      validateComment,
                                                  labelText: 'أضف تعليق',
                                                  suffixIcon: InkWell(
                                                      onTap: () => _addComment(
                                                          snapshot
                                                              .data.productId),
                                                      child: Column(
                                                        children: [
                                                          SizedBox(
                                                            height: 15,
                                                          ),
                                                          Text(
                                                            'ارسال',
                                                            style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                color:
                                                                    mainAppColor,
                                                                fontSize: 12),
                                                          ),
                                                        ],
                                                      )),
                                                  onChangedFunc: (String val) {
                                                    _comment = val;
                                                  },
                                                ),
                                              ),
                                      ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      snapshot.data.productPhone != ""
                                          ? InkWell(
                                              onTap: () => Provider.of<
                                                          AdDetailsProvider>(
                                                      context,
                                                      listen: false)
                                                  .makePhoneCall(snapshot
                                                      .data.productPhone),
                                              child: Container(
                                                height: 50,
                                                width: _width * 0.4,
                                                decoration: BoxDecoration(
                                                  color:whiteColor,
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(
                                                              10.0)),
                                                ),
                                                child: Center(
                                                  child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        Image.asset(
                                                            'assets/images/phone-fill.png',
                                                            color:
                                                                mainAppColor),
                                                        Container(
                                                          margin: EdgeInsets
                                                              .symmetric(
                                                                  horizontal:
                                                                      8),
                                                          child: Text(
                                                              snapshot.data
                                                                  .productPhone,
                                                              style: TextStyle(
                                                                  fontSize: 14,
                                                                  color:
                                                                      mainAppColor)),
                                                        ),
                                                      ]),
                                                ),
                                              ),
                                            )
                                          : Container(),
                                      InkWell(
                                        onTap: () {
                                          if (_authProvider.currentUser == null)
                                            Navigator.pushNamed(
                                                context, '/login_screen');
                                          else {
                                            if (_authProvider.currentUser.id ==
                                                snapshot.data.productCreatorId)
                                              Commons.showToast(
                                                  message:
                                                      'أنت صاحب الإعلان ، لايمكنك محادثة نفسك',
                                                  context: context);
                                            else
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          ChatDetailsScreen(
                                                            name: snapshot.data
                                                                .productCreatorName,
                                                            id: snapshot.data
                                                                .productCreatorId
                                                                .toString(),
                                                          )));
                                          }
                                        },
                                        child: Container(
                                          height: 50,
                                          width: _width * 0.2,
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 15),
                                          decoration: BoxDecoration(
                                            color: whiteColor,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10.0)),
                                          ),
                                          child: Center(
                                            child: Image.asset(
                                              'assets/images/mail_line.png',
                                              color: mainAppColor,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 15, vertical: 10),
                                    child: Text(
                                      'إعلانات مشابهة',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    )),
                                snapshot.data.customProductsLikeThis.length > 0
                                    ? Container(
                                        // margin: EdgeInsets.only(
                                        //   bottom: 40
                                        // ),
                                        height: ((snapshot
                                                        .data
                                                        .customProductsLikeThis
                                                        .length /
                                                    3)
                                                .ceil()) *
                                            150.0,
                                        child: GridView.builder(
                                          physics:
                                              NeverScrollableScrollPhysics(),
                                          itemCount: snapshot.data
                                              .customProductsLikeThis.length,
                                          gridDelegate:
                                              SliverGridDelegateWithFixedCrossAxisCount(
                                            crossAxisCount: 3,
                                            childAspectRatio: 1.0,
                                          ),
                                          itemBuilder: (context, index) {
                                            String imgUrl = snapshot
                                                .data
                                                .customProductsLikeThis[index]
                                                .image;
                                            return InkWell(
                                              onTap: () {
                                                Provider.of<AdDetailsProvider>(
                                                        context,
                                                        listen: false)
                                                    .setProductId(snapshot
                                                        .data
                                                        .customProductsLikeThis[
                                                            index]
                                                        .id);

                                                Navigator.pushNamed(context,
                                                    '/ad_details_screen');
                                              },
                                              child: Container(
                                                margin: EdgeInsets.all(6),
                                                child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                10.0)),
                                                    child: Image.network(
                                                      imgUrl,
                                                      fit: BoxFit.cover,
                                                    )),
                                              ),
                                            );
                                          },
                                        ))
                                    : Center(
                                        child: NoData(
                                        message: 'لاتوجد إعلانات متشابهة',
                                      )),
                                SizedBox(
                                  height: 20,
                                )
                              ]),
                            ),
                          ],
                        ));
              }
          }
          return Center(
            child: SpinKitDoubleBounce(
              color: mainAppColor,
            ),
          );
        });
  }

  void _addComment(int id) async {
    if (_formKey.currentState.validate()) {
      setState(() {
        _isLoading = true;
      });
      var result = await _apiProvider.post(Urls.ADD_COMMENT, body: {
        "prod_id": id,
        "body": _comment
      }, header: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${_authProvider.currentUser.accessToken}'
      });
      setState(() {
        _isLoading = false;
      });
      if (result["status_code"] == 200) {
        Commons.showToast(
            message: result['response']['msg'],
            context: context,
            color: hintColor);
        Navigator.pushReplacementNamed(context, '/ad_details_screen');
      } else
        Commons.showToast(
            message: result['response']['error'],
            context: context,
            color: hintColor);
    }
  }

  void _addReport(int id) async {
    if (_formReportKey.currentState.validate()) {
      setState(() {
        _isLoading2 = true;
      });
      var result = await _apiProvider.post(Urls.REPORT_PRODUCT, body: {
        "product_id": id,
        "message": _reportAd,
      }, header: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer ${_authProvider.currentUser.accessToken}'
      });
      setState(() {
        _isLoading2 = false;
      });
      if (result["status_code"] == 200) {
        Commons.showToast(
            message: 'تم الارسال', context: context, color: hintColor);
        Navigator.pop(context);
      } else if (result["status_code"] == 401) {
        Commons.showError(
            context: context,
            message: 'يرجى تسجيل الدخول مجدداً',
            onTapOk: () {
              Navigator.pop(context);

              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/login_screen', (Route<dynamic> route) => false);
              SharedPreferencesHelper.remove("user");
            });
      } else {
        Commons.showToast(
            message: result['response']['error'],
            context: context,
            color: hintColor);
      }
    }
  }
}
