import 'package:dio/dio.dart';
import 'dart:io';
import 'dart:async';

import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:haraj/custom_widgets/custom_selector/custom_selector.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/providers/navigation_provider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/shared_preferences/shared_preferences_helper.dart';
import 'package:haraj/ui/ad/choosing_child_category_screen.dart';
import 'package:haraj/ui/ad/choosing_sub_category_screen.dart';
import 'package:haraj/ui/auth/cities_screen.dart';
import 'package:haraj/utils/urls.dart';
import 'package:image_picker/image_picker.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/validation_mixin.dart';
import 'package:haraj/providers/adding_ad_provider.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:location/location.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:provider/provider.dart';
import 'package:haraj/ui/ad/choosing_category_screen.dart';

class AddingAdScreen extends StatefulWidget {
  @override
  _AddingAdScreenState createState() => _AddingAdScreenState();
}

class _AddingAdScreenState extends State<AddingAdScreen> with ValidationMixin {
  double _height, _width;
  AppBar _appBar;
  bool _initialRun = true;
  String _adTitle = '',
      _adPhone = '',
      _content = '',
      _price = '',
      _fuelType = '',
      _kiloMeters = '';
  bool _isLoading = false;
  final _picker = ImagePicker();
  Location _location = Location();
  bool _locationNotAvail = false;
  final _formKey = GlobalKey<FormState>();
  NavigationProvider _navigationProvider;
  ApiProvider _apiProvider = ApiProvider();
  AuthProvider _authProvider;
  AddingAdProvider _addingAdProvider;
  LocationData _locData;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void didChangeDependencies() {
    if (_initialRun) {
      _authProvider = Provider.of<AuthProvider>(context);
      _addingAdProvider = Provider.of<AddingAdProvider>(context);

      _addingAdProvider.setSelectedCity(null, notifyListener: false);
      _addingAdProvider.setSelectedCategory(null, notifyListener: false);
      _addingAdProvider.setSelectedCategoryError(false, notifyListener: false);
      _addingAdProvider.setSelectedCityError(false, notifyListener: false);
      _addingAdProvider.setMainImg(null, notifyListener: false);
      _addingAdProvider.setListOfAssets(List<Asset>(), notifyListener: false);
      _addingAdProvider.setMainImgError(false, notifyListener: false);
      _addingAdProvider.setHideMobile(false, notifyListener: false);
      _addingAdProvider.setHidePrice(false, notifyListener: false);
      _addingAdProvider.setCarIsUsed(false, notifyListener: false);
      _addingAdProvider.setSelectedCarModel(null, notifyListener: false);
      _addingAdProvider.setSelectedCarModelError(false, notifyListener: false);
      _locationSetup();
      _initialRun = false;
    }
    super.didChangeDependencies();
  }

  void _chooseCity() async {
    FocusScope.of(context).requestFocus(FocusNode());
    final city = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CitiesScreen()),
    );
    if (city != null) _addingAdProvider.setSelectedCity(city);
  }

  void _chooseCategory() async {
    FocusScope.of(context).requestFocus(FocusNode());
    final category = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ChoosingCategoryScreen()),
    );
    if (category != null) {
      _addingAdProvider.setSelectedCategory(category);
      _addingAdProvider.setSubSelectedCategory(null);
      _addingAdProvider.setChildSelectedCategory(null);
    }
  }

  void _chooseSubCategory() async {
    FocusScope.of(context).requestFocus(FocusNode());
    final category = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ChoosingSubCategoryScreen(
                categoryId: _addingAdProvider.selectedCategory.id,
              )),
    );
    if (category != null) {
      _addingAdProvider.setSubSelectedCategory(category);
      _addingAdProvider.setChildSelectedCategory(null);
    }
  }

  void _chooseChildCategory() async {
    FocusScope.of(context).requestFocus(FocusNode());
    final category = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ChoosingChildCategoryScreen(
                categoryId: _addingAdProvider.subSelectedCategory.id,
              )),
    );
    if (category != null) _addingAdProvider.setChildSelectedCategory(category);
  }

  void _chooseCarModel() {
    FocusScope.of(context).requestFocus(FocusNode());
    showDialog<int>(
        context: context,
        builder: (BuildContext context) {
          return new NumberPickerDialog.integer(
            minValue: DateTime.now().year - 40,
            maxValue: DateTime.now().year,
            title: new Text(
              'قم باختيار موديل السيارة',
              style: TextStyle(fontSize: 16),
            ),
            selectedTextStyle: TextStyle(color: mainAppColor, fontSize: 24),
            initialIntegerValue: DateTime.now().year,
            confirmWidget: Text(
              'موافق',
              style: TextStyle(color: mainAppColor, fontSize: 14),
            ),
            cancelWidget: Text(
              'إلغاء',
              style: TextStyle(color: mainAppColor, fontSize: 14),
            ),
          );
        }).then((int value) {
      if (value != null) {
        _addingAdProvider.setSelectedCarModel(value.toString());
      }
    });
  }

  Widget _buildBodyWidget() {
    return SingleChildScrollView(
        child: Container(
            child: Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 20,
          ),
          InkWell(
              onTap: () => _chooseCity(),
              child: CustomSelector(
                  title: Text(
                    _addingAdProvider.selectedCity != null
                        ? _addingAdProvider.selectedCity.name
                        : 'المدينة',
                    style: TextStyle(
                        color: _addingAdProvider.selectedCity == null
                            ? whiteColor
                            : whiteColor),
                  ),
                  suffixIcon: InkWell(
                    child: Icon(
                      Icons.gps_fixed,
                      color: _locData != null ? mainAppColor : hintColor,
                    ),
                    onTap: () => _locationSetup(),
                  ))),
          _addingAdProvider.selectedCityError
              ? Container(
                  margin: EdgeInsets.only(
                    right: _width * 0.07,
                    left: _width * 0.07,
                  ),
                  child: Text(
                    'يرجى اختيار المدينة',
                    style: TextStyle(color: Colors.red[700], fontSize: 11),
                  ))
              : Container(),
          SizedBox(
            height: 20,
          ),
          InkWell(
              onTap: () => _chooseCategory(),
              child: CustomSelector(
                title: Text(
                  _addingAdProvider.selectedCategory != null
                      ? _addingAdProvider.selectedCategory.name
                      : 'القسم',
                  style: TextStyle(
                      color: _addingAdProvider.selectedCategory == null
                          ? whiteColor
                          : whiteColor),
                ),
                suffixIcon: Icon(
                  Icons.arrow_forward_ios,
                  size: 17,
                ),
              )),
          _addingAdProvider.selectedCategoryError
              ? Container(
                  margin: EdgeInsets.only(
                    right: _width * 0.07,
                    left: _width * 0.07,
                  ),
                  child: Text(
                    'يرجى اختيار القسم',
                    style: TextStyle(color: Colors.red[700], fontSize: 11),
                  ))
              : Container(),
          _addingAdProvider.selectedCategory != null
              ? Container(
                  margin: EdgeInsets.only(top: 15),
                  child: InkWell(
                      onTap: () => _chooseSubCategory(),
                      child: CustomSelector(
                        title: Text(
                          _addingAdProvider.subSelectedCategory != null
                              ? _addingAdProvider.subSelectedCategory.name
                              : 'قسم فرعي',
                          style: TextStyle(
                              color:
                                  _addingAdProvider.subSelectedCategory == null
                                      ? whiteColor
                                      : whiteColor),
                        ),
                        suffixIcon: Icon(
                          Icons.arrow_forward_ios,
                          size: 17,
                        ),
                      )),
                )
              : Container(),
          _addingAdProvider.subSelectedCategory != null
              ? Container(
                  margin: EdgeInsets.only(top: 15),
                  child: InkWell(
                      onTap: () => _chooseChildCategory(),
                      child: CustomSelector(
                        title: Text(
                          _addingAdProvider.childSelectedCategory != null
                              ? _addingAdProvider.childSelectedCategory.name
                              : 'قسم فرعي',
                          style: TextStyle(
                              color: _addingAdProvider.childSelectedCategory ==
                                      null
                                  ? whiteColor
                                  : whiteColor),
                        ),
                        suffixIcon: Icon(
                          Icons.arrow_forward_ios,
                          size: 17,
                        ),
                      )),
                )
              : Container(),
          _addingAdProvider.selectedCategory != null &&
                  (_addingAdProvider.selectedCategory.id == 1 ||
                      _addingAdProvider.selectedCategory.name ==
                          'حراج السيارات')
              ? Container(
                  height: 40,
                  margin: EdgeInsets.symmetric(horizontal: _width * 0.03),
                  child: Row(
                    children: [
                      Consumer<AddingAdProvider>(
                          builder: (context, addAdProvider, child) {
                        return addAdProvider.carIsUsed
                            ? InkWell(
                                onTap: () => addAdProvider.setCarIsUsed(false),
                                child: Container(
                                  width: 20,
                                  height: 20,
                                  margin: EdgeInsets.symmetric(
                                    horizontal: _width * 0.02,
                                  ),
                                  child: Icon(
                                    Icons.check,
                                    color: whiteColor,
                                    size: 17,
                                  ),
                                  decoration: BoxDecoration(
                                    color: mainAppColor,
                                    border: Border.all(
                                      color: mainAppColor,
                                    ),
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                ),
                              )
                            : InkWell(
                                onTap: () => addAdProvider.setCarIsUsed(true),
                                child: Container(
                                  width: 20,
                                  height: 20,
                                  margin: EdgeInsets.symmetric(
                                    horizontal: _width * 0.02,
                                  ),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: hintColor,
                                    ),
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                ),
                              );
                      }),
                      Text('السيارة مستخدمة',
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ))
                    ],
                  ),
                )
              : Container(),
          SizedBox(
            height: 5,
          ),
          _addingAdProvider.selectedCategory != null &&
                  (_addingAdProvider.selectedCategory.id == 1 ||
                      _addingAdProvider.selectedCategory.name ==
                          'حراج السيارات')
              ? Container(
                  margin: EdgeInsets.only(bottom: 10),
                  height: 60,
                  child: CustomTextFormField(
                    enableBorder: false,
                    inputData: TextInputType.text,
                    hintTxt: 'نوع الوقود',
                    validationFunc: validateFuelType,
                    onChangedFunc: (String value) {
                      _fuelType = value;
                    },
                  ),
                )
              : Container(),
          _addingAdProvider.selectedCategory != null &&
                  (_addingAdProvider.selectedCategory.id == 1 ||
                      _addingAdProvider.selectedCategory.name ==
                          'حراج السيارات')
              ? Container(
                  margin: EdgeInsets.only(bottom: 10),
                  height: 60,
                  child: CustomTextFormField(
                    enableBorder: false,
                    inputData: TextInputType.number,
                    hintTxt: 'عدد الكيلومترات المقطوعة',
                    validationFunc: validateKiloMeter,
                    onChangedFunc: (String value) {
                      _kiloMeters = value;
                    },
                  ),
                )
              : Container(),
          _addingAdProvider.selectedCategory != null &&
                  (_addingAdProvider.selectedCategory.id == 1 ||
                      _addingAdProvider.selectedCategory.name ==
                          'حراج السيارات')
              ? Container(
                  margin: EdgeInsets.only(top: 15),
                  child: InkWell(
                      onTap: () => _chooseCarModel(),
                      child: CustomSelector(
                        title: Text(
                          _addingAdProvider.selectedCarModel != null
                              ? _addingAdProvider.selectedCarModel
                              : 'موديل السيارة',
                          style: TextStyle(
                              color: _addingAdProvider.selectedCarModel == null
                                  ? whiteColor
                                  : whiteColor),
                        ),
                        suffixIcon: Icon(
                          Icons.arrow_forward_ios,
                          size: 17,
                        ),
                      )),
                )
              : Container(),
          _addingAdProvider.selectedCarModelError &&
                  _addingAdProvider.selectedCategory != null &&
                  (_addingAdProvider.selectedCategory.id == 1 ||
                      _addingAdProvider.selectedCategory.name ==
                          'حراج السيارات')
              ? Container(
                  margin: EdgeInsets.only(
                      right: _width * 0.07, left: _width * 0.07, bottom: 10),
                  child: Text(
                    'يرجى اختيار موديل السيارة',
                    style: TextStyle(color: Colors.red[700], fontSize: 11),
                  ))
              : Container(),

          Container(
            height: 60,
            child: CustomTextFormField(
              enableBorder: false,
              inputData: TextInputType.text,
              hintTxt: 'عنوان الأعلان',
              fillColor: whiteColor,
              validationFunc: validateAdTitle,
              onChangedFunc: (String value) {
                _adTitle = value;
              },
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            height: 60,
            child: CustomTextFormField(
              enableBorder: false,
              hintTxt: 'رقم الجوال : 0534678924',
              validationFunc: validatePhoneNo,
              fillColor: whiteColor,
              inputData: TextInputType.number,
              onChangedFunc: (String val) {
                _adPhone = val;
              },
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            height: 40,
            margin: EdgeInsets.symmetric(horizontal: _width * 0.03),
            child: Row(
              children: [
                Consumer<AddingAdProvider>(builder: (context, provider, child) {
                  return provider.hideMobile
                      ? GestureDetector(
                          onTap: () => provider.setHideMobile(false),
                          child: Container(
                            width: 20,
                            height: 20,
                            margin: EdgeInsets.symmetric(
                              horizontal: _width * 0.02,
                            ),
                            child: Icon(
                              Icons.check,
                              color: whiteColor,
                              size: 17,
                            ),
                            decoration: BoxDecoration(
                              color: mainAppColor,
                              border: Border.all(
                                color: mainAppColor,
                              ),
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        )
                      : GestureDetector(
                          onTap: () => provider.setHideMobile(true),
                          child: Container(
                            width: 20,
                            height: 20,
                            margin: EdgeInsets.symmetric(
                              horizontal: _width * 0.02,
                            ),
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: hintColor,
                              ),
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                        );
                }),
                Text('أخفاء رقم الجوال',
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ))
              ],
            ),
          ),
          Container(
            height: 60,
            child: CustomTextFormField(
              enableBorder: false,
              hintTxt: 'سعر الأعلان',
              fillColor: whiteColor,

              validationFunc: validatePrice,
              inputData: TextInputType.number,
              onChangedFunc: (String val) {
                _price = val;
              },
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 40,
            margin: EdgeInsets.symmetric(horizontal: _width * 0.03),
            child: Row(
              children: [
                _addingAdProvider.hidePrice
                    ? InkWell(
                        onTap: () => _addingAdProvider.setHidePrice(false),
                        child: Container(
                          width: 20,
                          height: 20,
                          margin: EdgeInsets.symmetric(
                            horizontal: _width * 0.02,
                          ),
                          child: Icon(
                            Icons.check,
                            color: whiteColor,
                            size: 17,
                          ),
                          decoration: BoxDecoration(
                            color: mainAppColor,
                            border: Border.all(
                              color: mainAppColor,
                            ),
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                      )
                    : InkWell(
                        onTap: () => _addingAdProvider.setHidePrice(true),
                        child: Container(
                          width: 20,
                          height: 20,
                          margin: EdgeInsets.symmetric(
                            horizontal: _width * 0.02,
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: hintColor,
                            ),
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                      ),
                Text('أخفاء سعر الاعلان',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                        fontWeight: FontWeight.bold))
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 60,
            child: CustomTextFormField(
              maxLines: null,
              enableBorder: false,
              hintTxt: 'نص الأعلان',
              fillColor: whiteColor,

              validationFunc: validateContent,
              onChangedFunc: (String val) {
                _content = val;
              },
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
              height: 130,
              width: _width,
              margin:
                  EdgeInsets.symmetric(vertical: 10, horizontal: _width * 0.07),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'الصورة الاساسية للإعلان',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 80,
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      children: [
                        InkWell(
                          onTap: () => getImage(),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/images/upload.png',
                                height: 30,
                                color: mainAppColor,
                              ),
                              Text(
                                _addingAdProvider.mainImg == null
                                    ? 'أختر صورة'
                                    : 'تغيير الصورة',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 80,
                          margin: EdgeInsets.symmetric(horizontal: 10),
                          child: ClipRRect(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.00)),
                            child: _addingAdProvider.mainImg == null
                                ? Container()
                                : Image.file(
                                    _addingAdProvider.mainImg,
                                    width: _width * 0.25,
                                    fit: BoxFit.fill,
                                  ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  _addingAdProvider.mainImageError
                      ? Container(
                          margin: EdgeInsets.only(
                            right: _width * 0.07,
                            left: _width * 0.07,
                          ),
                          child: Text(
                            '* يرجى اختيار صورة رئيسية',
                            style:
                                TextStyle(color: Colors.red[700], fontSize: 11),
                          ))
                      : Container(),
                ],
              )),
          Divider(),
          Container(
              height: 130,
              width: _width,
              margin:
                  EdgeInsets.only(top: 10, bottom: 10, right: _width * 0.07),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'الصور الفرعية (اختياري)',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 80,
                    child: Row(
                      children: [
                        InkWell(
                          onTap: () => _addingAdProvider.loadAssets(context),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/images/upload.png',
                                height: 30,
                                color: mainAppColor,
                              ),
                              Text(
                                'أختر الصور',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: _addingAdProvider.listOfAssets.length,
                            itemBuilder: (context, index) {
                              return Stack(
                                children: [
                                  Container(
                                    width: _width * 0.3,
                                    margin: EdgeInsets.symmetric(horizontal: 5),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.00)),
                                      child: AssetThumb(
                                        asset: _addingAdProvider
                                            .listOfAssets[index],
                                        width: 100,
                                        height: 100,
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                      top: -10,
                                      left: 0,
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.close,
                                          color: mainAppColor,
                                        ),
                                        onPressed: () => _addingAdProvider
                                            .removeItemFromAssets(index),
                                      ))
                                ],
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
          Divider(),
          Container(
              margin: EdgeInsets.symmetric(vertical: 30),
              child: CustomButton(
                  btnLbl: 'نشر الاعلان',
                  btnStyle: TextStyle(color: whiteColor),
                  onPressedFunction: () async {
                    print('ad' + _adTitle);
                    FocusScope.of(context).requestFocus(FocusNode());
                    if (_formKey.currentState.validate() &
                        checkValidationOfAddingAd(_addingAdProvider)) {
                      if (_locData != null) {
                        Commons.showModelSheet(
                            msg: 'هل انت متأكد من نشر الأعلان؟',
                            imgUrl: 'assets/images/megaphone.png',
                            context: context,
                            height: _height,
                            width: _width,
                            onPressCancel: () {
                              Navigator.pop(context);
                              _navigationProvider.upadateNavigationIndex(
                                  0, context);
                            },
                            ok: () async {
                              FocusScope.of(context).requestFocus(FocusNode());
                              setState(() {
                                _isLoading = true;
                              });
                              FormData formData =
                                  await getFormData(_addingAdProvider);
                              print('phone' + _adPhone);
                              var result = await _apiProvider.postWithDio(
                                  Urls.CREATE_PRODUCT,
                                  body: formData,
                                  headers: {
                                    "Accept": "application/json",
                                    "Authorization":
                                        'Bearer ${_authProvider.currentUser.accessToken}'
                                  });
                              setState(() {
                                _isLoading = false;
                              });
                              if (result["status_code"] == 200) {
                                print(result['response']);
                                _navigationProvider.upadateNavigationIndex(
                                    0, context);
                                Commons.showToast(
                                    message: result['response']['msg'],
                                    context: context,
                                    color: hintColor);
                              } else if (result["status_code"] == 401)
                                Commons.showError(
                                    context: context,
                                    message: 'يرجى تسجيل الدخول مجدداً',
                                    onTapOk: () {
                                      Navigator.pop(
                                          _scaffoldKey.currentContext);

                                      Navigator.of(_scaffoldKey.currentContext)
                                          .pushNamedAndRemoveUntil(
                                              '/login_screen',
                                              (Route<dynamic> route) => false);
                                      SharedPreferencesHelper.remove("user");
                                    });
                              else
                                Commons.showMessageDialog(
                                    context: context,
                                    msg: result['response']['error']);
                            });
                      } else {
                        Commons.showToast(
                            message:
                                'يرجى إتاحة إمكانية تحديد المكان والضغط على الإيقونة الخاصة بها فى الحقل الخاص بالمدينة',
                            context: context);
                        _locationSetup();
                      }
                    }
                  }))
        ],
      ),
    )));
  }

  void _locationSetup() async {
    bool serviceEnabled = await _location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await _location.requestService();
      if (!serviceEnabled) {
        _locationNotAvail = true;
        return;
      }
    }

    PermissionStatus permissionGranted = await _location.hasPermission();
    if (permissionGranted == PermissionStatus.denied) {
      permissionGranted = await _location.requestPermission();
      if (permissionGranted != PermissionStatus.granted) {
        _locationNotAvail = true;
        return;
      }
    }
    _locData = await Location().getLocation();
    if (_locData != null)
      Commons.showToast(message: 'تم تحديد موقعك', context: context);
  }

  Future<FormData> getFormData(AddingAdProvider addingAdProvider) async {
    MultipartFile img =
        await MultipartFile.fromFile(_addingAdProvider.mainImg.path);
    List<MultipartFile> multipartImageList = List<MultipartFile>();
    if (addingAdProvider.listOfAssets.length > 0)
      multipartImageList =
          await addingAdProvider.convertAssets(addingAdProvider.listOfAssets);

    Map<String, dynamic> map = Map<String, dynamic>();
    map["image"] = img;
    map["latitude"] = _locData.latitude;
    map["longitude"] = _locData.longitude;
    map["title"] = _adTitle;
    map["category_id"] = _addingAdProvider.selectedCategory.id.toString();
    map["phone"] = _adPhone;
    map["content"] = _content;
    map["price"] = _price;
    map["hide_phone"] = addingAdProvider.hideMobile ? '1' : '0';
    map["hide_price"] = addingAdProvider.hidePrice ? '1' : '0';
    map["address_id"] = _addingAdProvider.selectedCity.id.toString();
    for (int i = 0; i < multipartImageList.length; i++) {
      map['sub_images$i'] = multipartImageList[i];
    }

    if (_addingAdProvider.subSelectedCategory != null)
      map['sub_category_id'] =
          _addingAdProvider.subSelectedCategory.id.toString();

    if (_addingAdProvider.childSelectedCategory != null)
      map['child_category_id'] =
          _addingAdProvider.childSelectedCategory.id.toString();

    if (_addingAdProvider.selectedCategory != null &&
        (_addingAdProvider.selectedCategory.id == 1 ||
            _addingAdProvider.selectedCategory.name == 'حراج السيارات')) {
      map['used'] = _addingAdProvider.carIsUsed ? '1' : '0';
      map['fuel_type'] = _fuelType;
      map['kilo_meter'] = _kiloMeters;
      map['model_year'] = _addingAdProvider.selectedCarModel;
    }

    return FormData.fromMap(map);
  }

  Future getImage() async {
    FocusScope.of(context).requestFocus(FocusNode());
    final pickedFile = await _picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) _addingAdProvider.setMainImg(File(pickedFile.path));
  }

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;
    _navigationProvider = Provider.of<NavigationProvider>(context);
    _appBar = AppBar(
      backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        'تفاصيل الأعلان',
        style: Theme.of(context).textTheme.headline1,
      ),
    );
    return Scaffold(
        backgroundColor: greenColor,
        appBar: _appBar,
        key: _scaffoldKey,
        body: Stack(
          children: [
            _buildBodyWidget(),
            _isLoading
                ? Center(
                    child: SpinKitFadingCircle(color: mainAppColor),
                  )
                : Container(),
          ],
        ));
  }
}
