import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/no_data/no_data.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/models/category.dart';
import 'package:haraj/providers/home_provider.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:provider/provider.dart';

class ChoosingChildCategoryScreen extends StatefulWidget {
  final int categoryId;

  const ChoosingChildCategoryScreen({Key key, this.categoryId}) : super(key: key);

  @override
  _ChoosingChildCategoryScreenState createState() =>
      _ChoosingChildCategoryScreenState();
}

class _ChoosingChildCategoryScreenState extends State<ChoosingChildCategoryScreen> {
  @override
  Widget build(BuildContext context) {
    AppBar appBar = AppBar(
      backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        'أختر القسم',
        style: Theme.of(context).textTheme.headline1,
      ),
      leading: InkWell(
        onTap: () => Navigator.pop(context),
        child: Image.asset('assets/images/arrow_back.png'),
      ),
    );
    return NetworkIndicator(
        child: PageContainer(
            child: Scaffold(
               backgroundColor:greenColor,
      appBar: appBar,
      body: _buildBodyWidget(),
    )));
  }

  Widget _buildBodyWidget() {
    return FutureBuilder<List<Category>>(
        future: Provider.of<HomeProvider>(context, listen: false)
            .getChildCategoriesList(context, widget.categoryId),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return SpinKitDoubleBounce(color: mainAppColor);
            case ConnectionState.active:
              return Text('');
            case ConnectionState.waiting:
              return SpinKitDoubleBounce(color: mainAppColor);
            case ConnectionState.done:
              if (snapshot.hasError) {
                print(snapshot.error.toString());
                return Center(
                  child: Text('حدث خطأ ما'),
                );
              } else if (snapshot.data.length == 0) {
                return Center(
                  child: NoData(
                    message: 'لا توجد اقسام',
                  ),
                );
              } else {
                return Container(
                  margin: EdgeInsets.only(top: 10),
                  child: ListView.separated(
                    physics: BouncingScrollPhysics(),
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () =>
                            Navigator.pop(context, snapshot.data[index]),
                        child: Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: 15, vertical: 10),
                            child: Text(snapshot.data[index].name,
                             style: TextStyle(
                                color: Colors.white
                              ),
                            )),
                      );
                    },
                    separatorBuilder: (context, index) {
                      return Divider(
                        color: Colors.white,
                      );
                    },
                  ),
                );
              }
          }
          return SpinKitDoubleBounce(color: mainAppColor);
        });
  }
}
