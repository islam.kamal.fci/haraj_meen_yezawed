import 'package:dio/dio.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/custom_selector/custom_selector.dart';
import 'package:haraj/custom_widgets/no_data/no_data.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/providers/editing_ad_provider.dart';
import 'package:haraj/providers/navigation_provider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/shared_preferences/shared_preferences_helper.dart';
import 'package:haraj/ui/ad/my_ads_screen.dart';
import 'package:haraj/ui/auth/cities_screen.dart';
import 'package:haraj/utils/urls.dart';
import 'package:image_picker/image_picker.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/validation_mixin.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:location/location.dart';
import 'package:haraj/ui/ad/choosing_sub_category_screen.dart';
import 'package:numberpicker/numberpicker.dart';
import 'choosing_child_category_screen.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:provider/provider.dart';
import 'package:haraj/ui/ad/choosing_category_screen.dart';

class EditingAdScreen extends StatefulWidget {
  @override
  _EditingAdScreenState createState() => _EditingAdScreenState();
}

class _EditingAdScreenState extends State<EditingAdScreen>
    with ValidationMixin {
  double _height, _width;
  AppBar _appBar;
  bool _initialRun = true;

  bool _isLoading = false;
  final _picker = ImagePicker();
  final _formKey = GlobalKey<FormState>();
  NavigationProvider _navigationProvider;
  ApiProvider _apiProvider = ApiProvider();
  AuthProvider _authProvider;
  EditingAdProvider _editingAdProvider;
  LocationData _locData;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void didChangeDependencies() {
    if (_initialRun) {
      _authProvider = Provider.of<AuthProvider>(context);
      _editingAdProvider = Provider.of<EditingAdProvider>(context);
      _editingAdProvider.setListOfAssets(List<Asset>(), notifyListener: false);
      _getCurrentUserLocation();
      _initialRun = false;
    }
    super.didChangeDependencies();
  }

  void _chooseCity() async {
    FocusScope.of(context).requestFocus(FocusNode());
    final city = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CitiesScreen()),
    );
    if (city != null) _editingAdProvider.updateCity(city.id, city.name);
  }

  void _chooseCategory() async {
    FocusScope.of(context).requestFocus(FocusNode());
    final category = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ChoosingCategoryScreen()),
    );
    if (category != null)
      _editingAdProvider.updateCategory(category.id, category.name);
  }

  void _chooseSubCategory() async {
    FocusScope.of(context).requestFocus(FocusNode());
    final category = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ChoosingSubCategoryScreen(
              categoryId: _editingAdProvider.product.categoryId)),
    );
    if (category != null) {
      _editingAdProvider.updateProductSubCategory(category.id, category.name);
      _editingAdProvider.updateProductChildCategory(null, null);
    }
  }

  void _chooseChildCategory() async {
    FocusScope.of(context).requestFocus(FocusNode());
    final category = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ChoosingChildCategoryScreen(
                categoryId: _editingAdProvider.product.subCategoryId,
              )),
    );
    if (category != null)
      _editingAdProvider.updateProductChildCategory(category.id, category.name);
  }

  void _chooseCarModel() {
    FocusScope.of(context).requestFocus(FocusNode());
    showDialog<int>(
        context: context,
        builder: (BuildContext context) {
          return NumberPickerDialog.integer(
            minValue: DateTime.now().year - 40,
            maxValue: DateTime.now().year,
            title: new Text(
              'قم باختيار موديل السيارة',
              style: TextStyle(fontSize: 16),
            ),
            selectedTextStyle: TextStyle(color: mainAppColor, fontSize: 24),
            initialIntegerValue: _editingAdProvider.product.modelYear,
            confirmWidget: Text(
              'موافق',
              style: TextStyle(color: mainAppColor, fontSize: 14),
            ),
            cancelWidget: Text(
              'إلغاء',
              style: TextStyle(color: mainAppColor, fontSize: 14),
            ),
          );
        }).then((int value) {
      if (value != null) {
        _editingAdProvider.updateProductCarModel(value);
      }
    });
  }

  Widget _buildBodyWidget() {
    return SingleChildScrollView(
        child: Container(
            child: Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 20,
          ),
          InkWell(
              onTap: () => _chooseCity(),
              child: CustomSelector(
                  title: Text(
                    _editingAdProvider.product.addressName,
                    style: TextStyle(color: whiteColor),
                  ),
                  suffixIcon: InkWell(
                    child: Icon(
                      Icons.gps_fixed,
                      color: _locData != null ? mainAppColor : hintColor,
                    ),
                    onTap: () => _getCurrentUserLocation(),
                  ))),
          SizedBox(
            height: 20,
          ),
          InkWell(
              onTap: () => _chooseCategory(),
              child: CustomSelector(
                title: Text(
                  _editingAdProvider.product.categoryName,
                  style: TextStyle(color: whiteColor),
                ),
                suffixIcon: Icon(
                  Icons.arrow_forward_ios,
                  size: 17,
                  color: whiteColor,
                ),
              )),
          _editingAdProvider.product.categoryId != null
              ? Container(
                  margin: EdgeInsets.only(top: 15),
                  child: InkWell(
                      onTap: () => _chooseSubCategory(),
                      child: CustomSelector(
                        title: Text(
                          _editingAdProvider.product.subCategoryId != null
                              ? _editingAdProvider.product.subCategoryName
                              : 'قسم فرعي',
                          style: TextStyle(
                              color: _editingAdProvider.product.subCategoryId ==
                                      null
                                  ? hintColor
                                  : whiteColor),
                        ),
                        suffixIcon: Icon(
                          Icons.arrow_forward_ios,
                          size: 17,
                          color: whiteColor,
                        ),
                      )),
                )
              : Container(),
          _editingAdProvider.product.subCategoryId != null
              ? Container(
                  margin: EdgeInsets.only(top: 15),
                  child: InkWell(
                      onTap: () => _chooseChildCategory(),
                      child: CustomSelector(
                        title: Text(
                          _editingAdProvider.product.childCategoryId != null
                              ? _editingAdProvider.product.childCategoryName
                              : 'قسم فرعي',
                          style: TextStyle(
                              color:
                                  _editingAdProvider.product.childCategoryId ==
                                          null
                                      ? hintColor
                                      : whiteColor),
                        ),
                        suffixIcon: Icon(
                          Icons.arrow_forward_ios,
                          size: 17,
                          color: whiteColor,
                        ),
                      )),
                )
              : Container(),
          _editingAdProvider.product.used != null
              ? Container(
                  height: 40,
                  margin: EdgeInsets.symmetric(horizontal: _width * 0.03),
                  child: Row(
                    children: [
                      InkWell(
                        onTap: () => _editingAdProvider.updateCarIsUsed(),
                        child: Container(
                          width: 20,
                          height: 20,
                          margin: EdgeInsets.symmetric(
                            horizontal: _width * 0.02,
                          ),
                          child: _editingAdProvider.product.used
                              ? Icon(
                                  Icons.check,
                                  color: whiteColor,
                                  size: 17,
                                )
                              : Container(),
                          decoration: BoxDecoration(
                            color: _editingAdProvider.product.used
                                ? mainAppColor
                                : Colors.white,
                            border: Border.all(
                              color: _editingAdProvider.product.used
                                  ? mainAppColor
                                  : hintColor,
                            ),
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                        ),
                      ),
                      Text('السيارة مستخدمة',
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ))
                    ],
                  ),
                )
              : Container(),
          SizedBox(
            height: 5,
          ),
          _editingAdProvider.product.fuelType != null
              ? Container(
                  margin: EdgeInsets.only(bottom: 10),
                  height: 60,
                  child: CustomTextFormField(
                      initialValue: _editingAdProvider.product.fuelType,
                      enableBorder: false,
                      inputData: TextInputType.text,
                      hintTxt: 'نوع الوقود',
                      validationFunc: validateFuelType,
                      onChangedFunc: (String value) =>
                          _editingAdProvider.updateProductFuelType(value)),
                )
              : Container(),
          _editingAdProvider.product.kiloMeter != null
              ? Container(
                  margin: EdgeInsets.only(bottom: 10),
                  height: 60,
                  child: CustomTextFormField(
                      initialValue: _editingAdProvider.product.kiloMeter,
                      enableBorder: false,
                      inputData: TextInputType.number,
                      hintTxt: 'عدد الكيلومترات المقطوعة',
                      validationFunc: validateKiloMeter,
                      onChangedFunc: (String value) =>
                          _editingAdProvider.updateProductKiloMeters(value)),
                )
              : Container(),
          _editingAdProvider.product.modelYear != null
              ? Container(
                  margin: EdgeInsets.only(top: 15),
                  child: InkWell(
                      onTap: () => _chooseCarModel(),
                      child: CustomSelector(
                        title: Text(
                          _editingAdProvider.product.modelYear.toString(),
                          style: TextStyle(color: whiteColor),
                        ),
                        suffixIcon: Icon(
                          Icons.arrow_forward_ios,
                          size: 17,
                          color: whiteColor,
                        ),
                      )),
                )
              : Container(),
          SizedBox(
            height: 5,
          ),
          Container(
            height: 60,
            child: CustomTextFormField(
                initialValue: _editingAdProvider.product.title,
                enableBorder: false,
                fillColor: whiteColor,
                inputData: TextInputType.text,
                hintTxt: 'عنوان الأعلان',
                validationFunc: validateAdTitle,
                onChangedFunc: (String value) =>
                    _editingAdProvider.updateProductTitle(value)),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            height: 60,
            child: CustomTextFormField(
                initialValue: _editingAdProvider.product.productPhone,
                enableBorder: false,
                hintTxt: 'رقم الجوال',
                fillColor: whiteColor,

                validationFunc: validatePhoneNo,
                inputData: TextInputType.number,
                onChangedFunc: (String value) =>
                    _editingAdProvider.updateProductPhone(value)),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            height: 40,
            margin: EdgeInsets.symmetric(horizontal: _width * 0.03),
            child: Row(
              children: [
                InkWell(
                  onTap: () => _editingAdProvider.updateHideMobile(),
                  child: Container(
                    width: 20,
                    height: 20,
                    margin: EdgeInsets.symmetric(
                      horizontal: _width * 0.02,
                    ),
                    child: _editingAdProvider.product.hidePhone
                        ? Icon(
                            Icons.check,
                            color: whiteColor,
                            size: 17,
                          )
                        : Container(),
                    decoration: BoxDecoration(
                      color: _editingAdProvider.product.hidePhone
                          ? mainAppColor
                          : Colors.white,
                      border: Border.all(
                        color: _editingAdProvider.product.hidePhone
                            ? mainAppColor
                            : hintColor,
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
                Text('أخفاء رقم الجوال',
                    style: TextStyle(
                      color: whiteColor,
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                    ))
              ],
            ),
          ),
          Container(
            height: 60,
            child: CustomTextFormField(
              initialValue: _editingAdProvider.product.productPrice,
              enableBorder: false,
              hintTxt: 'سعر الأعلان',
              fillColor: whiteColor,

              validationFunc: validatePrice,
              inputData: TextInputType.number,
              onChangedFunc: (String val) =>
                  _editingAdProvider.updateProductPrice(val),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 40,
            margin: EdgeInsets.symmetric(horizontal: _width * 0.03),
            child: Row(
              children: [
                InkWell(
                  onTap: () => _editingAdProvider.updateHidePrice(),
                  child: Container(
                    width: 20,
                    height: 20,
                    margin: EdgeInsets.symmetric(
                      horizontal: _width * 0.02,
                    ),
                    child: _editingAdProvider.product.hidePrice
                        ? Icon(
                            Icons.check,
                            color: whiteColor,
                            size: 17,
                          )
                        : Container(),
                    decoration: BoxDecoration(
                      color: _editingAdProvider.product.hidePrice
                          ? mainAppColor
                          : Colors.white,
                      border: Border.all(
                        color: _editingAdProvider.product.hidePrice
                            ? mainAppColor
                            : hintColor,
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                ),
                Text('أخفاء سعر الاعلان',
                    style: TextStyle(
                        color: whiteColor,
                        fontSize: 12,
                        fontWeight: FontWeight.bold))
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 60,
            child: CustomTextFormField(
                initialValue: _editingAdProvider.product.productDescription,
                enableBorder: false,
                maxLines: null,
                fillColor: whiteColor,
                hintTxt: 'نص الأعلان',
                validationFunc: validateContent,
                onChangedFunc: (String val) =>
                    _editingAdProvider.updateProductDesc(val)),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
              height: 130,
              width: _width,
              margin:
                  EdgeInsets.symmetric(vertical: 10, horizontal: _width * 0.07),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'الصورة الاساسية للإعلان',
                    style: TextStyle(
                        color: whiteColor,
                        fontSize: 13,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 80,
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      children: [
                        InkWell(
                          onTap: () => getImage(),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/images/upload.png',
                                height: 30,
                                color: whiteColor,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'تغيير الصورة',
                                style: TextStyle(
                                    color: whiteColor,
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 80,
                          margin: EdgeInsets.symmetric(horizontal: 10),
                          child: ClipRRect(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.00)),
                            child: _editingAdProvider.mainImg == null
                                ? Image.network(
                                    _editingAdProvider.product.image,
                                    width: _width * 0.25,
                                    fit: BoxFit.fill,
                                  )
                                : Image.file(
                                    _editingAdProvider.mainImg,
                                    width: _width * 0.25,
                                    fit: BoxFit.fill,
                                  ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                ],
              )),
          Divider(),
          Container(
              height: 145,
              width: _width,
              margin:
                  EdgeInsets.only(top: 10, bottom: 10, right: _width * 0.07),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'الصور الفرعية (المرفوعة)',
                    style: TextStyle(
                        color: whiteColor,
                        fontSize: 13,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  _editingAdProvider.product.productSubImages.length > 0
                      ? Container(
                          height: 80,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: _editingAdProvider
                                .product.productSubImages.length,
                            itemBuilder: (context, index) {
                              return Stack(
                                children: [
                                  Container(
                                    height: 80,
                                    margin: EdgeInsets.symmetric(horizontal: 5),
                                    child: ClipRRect(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.00)),
                                        child: Image.network(
                                          _editingAdProvider.product
                                              .productSubImages[index].url,
                                          width: _width * 0.25,
                                          fit: BoxFit.fill,
                                        )),
                                  ),
                                  Positioned(
                                      top: -10,
                                      left: 0,
                                      child: IconButton(
                                          icon: Icon(
                                            Icons.close,
                                            color: mainAppColor,
                                          ),
                                          onPressed: () => _deleteImg(
                                              index: index,
                                              imgId: _editingAdProvider.product
                                                  .productSubImages[index].id
                                                  .toString())))
                                ],
                              );
                            },
                          ))
                      : NoData(
                          message: 'لاتوجد صور فرعية مرفوعة حالياً',
                        )
                ],
              )),
          Divider(),
          Container(
              height: 130,
              width: _width,
              margin:
                  EdgeInsets.only(top: 10, bottom: 10, right: _width * 0.07),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'إضافة صور فرعية جديدة',
                    style: TextStyle(
                        color: whiteColor,
                        fontSize: 13,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 80,
                    child: Row(
                      children: [
                        InkWell(
                          onTap: () => _editingAdProvider.loadAssets(context),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/images/upload.png',
                                height: 30,
                                color: whiteColor,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                'أخترالصور',
                                style: TextStyle(
                                    color: whiteColor,
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: _editingAdProvider.listOfAssets.length,
                            itemBuilder: (context, index) {
                              return Stack(
                                children: [
                                  Container(
                                    width: _width * 0.3,
                                    margin: EdgeInsets.symmetric(horizontal: 5),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10.00)),
                                      child: AssetThumb(
                                        asset: _editingAdProvider
                                            .listOfAssets[index],
                                        width: 100,
                                        height: 100,
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                      top: -10,
                                      left: 0,
                                      child: IconButton(
                                        icon: Icon(
                                          Icons.close,
                                          color: mainAppColor,
                                        ),
                                        onPressed: () => _editingAdProvider
                                            .removeItemFromAssets(index),
                                      ))
                                ],
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
          Divider(),
          Container(
              margin: EdgeInsets.symmetric(vertical: 30),
              child: CustomButton(
                  btnLbl: 'نشر الاعلان',
                  btnStyle: TextStyle(color: whiteColor),
                  onPressedFunction: () async {
                    FocusScope.of(context).requestFocus(FocusNode());
                    if (_formKey.currentState.validate()) {
                      if (_locData != null) {
                        Commons.showModelSheet(
                            msg: 'هل انت متأكد من نشر الأعلان؟',
                            imgUrl: 'assets/images/megaphone.png',
                            context: context,
                            height: _height,
                            width: _width,
                            onPressCancel: () {
                              Navigator.pop(context);
                              _navigationProvider.upadateNavigationIndex(
                                  0, context);
                            },
                            ok: () async {
                              FocusScope.of(context).requestFocus(FocusNode());
                              setState(() {
                                _isLoading = true;
                              });
                              FormData formData =
                                  await getFormData(_editingAdProvider);

                              var result = await _apiProvider.postWithDio(
                                  Urls.UPDATE_PRODUCT,
                                  body: formData,
                                  headers: {
                                    "Accept": "application/json",
                                    "Authorization":
                                        'Bearer ${_authProvider.currentUser.accessToken}'
                                  });
                              setState(() {
                                _isLoading = false;
                              });
                              if (result["status_code"] == 200) {
                                Commons.showToast(
                                    message: result['response']['msg'],
                                    context: context,
                                    color: hintColor);
                                Navigator.pop(context);
                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => MyAdsScreen(),
                                  ),
                                );
                              } else if (result["status_code"] == 401) {
                                Commons.showError(
                                    context: context,
                                    message: 'يرجى تسجيل الدخول مجدداً',
                                    onTapOk: () {
                                      Navigator.pop(
                                          _scaffoldKey.currentContext);

                                      Navigator.of(_scaffoldKey.currentContext)
                                          .pushNamedAndRemoveUntil(
                                              '/login_screen',
                                              (Route<dynamic> route) => false);
                                      SharedPreferencesHelper.remove("user");
                                    });
                              } else {
                                Commons.showMessageDialog(
                                    context: context,
                                    msg: result['response']['error']);
                              }
                            });
                      } else
                        Commons.showToast(
                            message:
                                'يرجى إتاحة إمكانية تحديد المكان والضغط على الإيقونة الخاصة بها فى الحقل الخاص بالمدينة',
                            context: context);
                    }
                  }))
        ],
      ),
    )));
  }

  Future<void> _getCurrentUserLocation() async {
    _locData = await Location().getLocation();

    if (_locData != null)
      Commons.showToast(message: 'تم تحديد موقعك', context: context);
  }

  Future<Null> _deleteImg({@required int index, @required String imgId}) async {
    setState(() {
      _isLoading = true;
    });
    final response = await _apiProvider.post(Urls.DELETE_IMG_URL, header: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${_authProvider.currentUser.accessToken}'
    }, body: {
      "product_id": _editingAdProvider.product.id,
      "image_id": imgId
    });
    setState(() {
      _isLoading = false;
    });
    if (response['status_code'] == 200) {
      _editingAdProvider.removeImgOfProduct(index);
      Commons.showToast(
          message: response['response']['data'], context: context);
    } else
      Commons.showError(
          message: response['response']['error'], context: context);
  }

  Future<FormData> getFormData(EditingAdProvider editingAdProvider) async {
    List<MultipartFile> multipartImageList = List<MultipartFile>();
    if (_editingAdProvider.listOfAssets.length > 0)
      multipartImageList = await _editingAdProvider
          .convertAssets(_editingAdProvider.listOfAssets);

    Map<String, dynamic> map = Map<String, dynamic>();
    if (_editingAdProvider.mainImg != null) {
      MultipartFile img =
          await MultipartFile.fromFile(_editingAdProvider.mainImg.path);
      map['image'] = img;
    }

    map['latitude'] = _locData.latitude;
    map['longitude'] = _locData.longitude;

    map['title'] = _editingAdProvider.product.title;
    map["category_id"] = _editingAdProvider.product.categoryId.toString();
    map["phone"] = _editingAdProvider.product.productPhone;
    map["content"] = _editingAdProvider.product.productDescription;
    map["price"] = _editingAdProvider.product.productPrice;
    map["hide_phone"] = _editingAdProvider.product.hidePhone ? '1' : '0';
    map["hide_price"] = _editingAdProvider.product.hidePrice ? '1' : '0';
    map["address_id"] = _editingAdProvider.product.addressId;
    map["product_id"] = _editingAdProvider.product.id.toString();
    for (int i = 0; i < multipartImageList.length; i++) {
      map['sub_images$i'] = multipartImageList[i];
    }

    if (_editingAdProvider.product.subCategoryId != null)
      map['sub_category_id'] =
          _editingAdProvider.product.subCategoryId.toString();
    print('sub' + _editingAdProvider.product.subCategoryId.toString());
    if (_editingAdProvider.product.childCategoryId != null)
      map['child_category_id'] =
          _editingAdProvider.product.childCategoryId.toString();
    print('child' + _editingAdProvider.product.childCategoryId.toString());
    if (_editingAdProvider.product.categoryId == 1 ||
        _editingAdProvider.product.categoryName == 'حراج السيارات') {
      map['used'] = _editingAdProvider.product.used ? '1' : '0';
      map['fuel_type'] = _editingAdProvider.product.fuelType;
      map['kilo_meter'] = _editingAdProvider.product.kiloMeter;
      map['model_year'] = _editingAdProvider.product.modelYear.toString();
    }
    return FormData.fromMap(map);
  }

  Future getImage() async {
    FocusScope.of(context).requestFocus(FocusNode());
    final pickedFile = await _picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null)
      _editingAdProvider.setMainImg(File(pickedFile.path));
  }

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;
    _navigationProvider = Provider.of<NavigationProvider>(context);
    _appBar = AppBar(
      backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        'تفاصيل الأعلان',
        style: Theme.of(context).textTheme.headline1,
      ),
      leading: InkWell(
        onTap: () {
          Navigator.pop(context);

          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => MyAdsScreen()));
        },
        child: Image.asset('assets/images/arrow_back.png'),
      ),
    );
    return NetworkIndicator(
        child: PageContainer(
            child: Scaffold(
                backgroundColor: greenColor,
                appBar: _appBar,
                key: _scaffoldKey,
                body: Stack(
                  children: [
                    _buildBodyWidget(),
                    _isLoading
                        ? Center(
                            child: SpinKitFadingCircle(color: mainAppColor),
                          )
                        : Container(),
                  ],
                ))));
  }
}
