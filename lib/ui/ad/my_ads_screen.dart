import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/custom_widgets/shared/ad.dart';
import 'package:haraj/custom_widgets/shared/shimmer_ad.dart';
import 'package:haraj/models/product.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/providers/editing_ad_provider.dart';
import 'package:haraj/providers/user_ads_provider.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';
import 'package:provider/provider.dart';
import 'package:haraj/ui/ad/editing_ad_screen.dart';
import '../../shared_preferences/shared_preferences_helper.dart';

class MyAdsScreen extends StatefulWidget {
  final bool fromAddad;

  const MyAdsScreen({Key key, this.fromAddad = false}) : super(key: key);
  @override
  _MyAdsScreenState createState() => _MyAdsScreenState();
}

class _MyAdsScreenState extends State<MyAdsScreen> {
  double _height, _width;
  AppBar appBar;
  bool _initalRun = true;
  bool _isLoading = false;
  ApiProvider _apiProvider = ApiProvider();
  AuthProvider _authProvider;
  EditingAdProvider _editingAdProvider;
  UserAdsProvider _userAdsProvider;
  Future<List<Product>> _userProductList;


@override
  void didChangeDependencies() {
  if(_initalRun){
    _userAdsProvider = Provider.of<UserAdsProvider>(context);
    _userProductList =  _userAdsProvider.getMyProductList(context);
  _initalRun =false;
  }
    super.didChangeDependencies();
  }
  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;
    _authProvider = Provider.of<AuthProvider>(context);
    _editingAdProvider = Provider.of<EditingAdProvider>(context);
    appBar = AppBar(
        backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        'أعلاناتي',
        style: Theme.of(context).textTheme.headline1,
      ),
      leading: InkWell(
        onTap: () =>
       
               Navigator.pop(context)
        ,
        child: Image.asset('assets/images/arrow_back.png', color: whiteColor,),
      ),
    );

    return NetworkIndicator(child: PageContainer(
      child: RefreshIndicator(
  onRefresh:() async {
    _userProductList =  _userAdsProvider.getMyProductList(context);
  },child:Scaffold(
        backgroundColor: greenColor,
        appBar: appBar,
        body: _isLoading
            ? Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              )
            : _buildBodyWidget(),
      ),
    )));
  }

  Widget _buildBodyWidget() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15),
      child: FutureBuilder<List<Product>>(
          future: _userProductList,
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return ListView.separated(
                  itemCount: 25,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, index) {
                    return ShimmerAd();
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                );
              case ConnectionState.active:
                return Text('');
              case ConnectionState.waiting:
                return ListView.separated(
                  itemCount: 25,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, index) {
                    return ShimmerAd();
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                );
              case ConnectionState.done:
                if (snapshot.hasError) {
                  print(snapshot.error.toString());
                  return Center(
                    child: Text('حدث خطأ ما'),
                  );
                } else if (snapshot.data.length == 0) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Text(
                            ' لا يوجد اعلانات لديك,ابدا الان بنشر اعلاناتك',
                            style: TextStyle(color: hintColor, fontSize: 17),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  );
                } else {
                  return Container(
                    margin: EdgeInsets.only(top: 10),
                    child: ListView.separated(
                      physics: BouncingScrollPhysics(),
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                                child:
                                    Ad(product: snapshot.data[index])),
                            InkWell(
                              onTap: () => _showBottomSheet(
                                  snapshot.data[index].id,
                                  snapshot.data[index]),
                              child: Container(
                                  margin: EdgeInsets.only(
                                    left: _width * 0.05,
                                  ),
                                  child: Image.asset(
                                    'assets/images/more-line.png',
                                    color: whiteColor,
                                  )),
                            )
                          ],
                        );
                      },
                      separatorBuilder: (context, index) {
                        return Divider(
                          color: whiteColor,
                        );
                      },
                    ),
                  );
                }
            }
            return ListView.separated(
              itemCount: 25,
              physics: BouncingScrollPhysics(),
              itemBuilder: (context, index) {
                return ShimmerAd();
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          }),
    );
  }

  _showBottomSheet(int adId, Product product) {
    return showModalBottomSheet(
      backgroundColor: greenColor,
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: Colors.white
          ),
            borderRadius: BorderRadius.only(
                topLeft: const Radius.circular(15.0),
                topRight: const Radius.circular(15.0))),
        context: context,
        isScrollControlled: true,
        builder: (context) {
          return Container(
              height: 150,
              margin: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () => _deleteAd(adId),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'حذف الإعلان',
                        style: TextStyle(
                          color: Colors.white,
                            fontWeight: FontWeight.bold, fontSize: 14),
                      ),
                    ),
                  ),
                  Divider(
                    color: Colors.white,
                  ),
                     InkWell(
                    onTap: () => _refreshAd(adId),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'تحديث الإعلان',
                        style: TextStyle(
                          color: Colors.white,
                            fontWeight: FontWeight.bold, fontSize: 14),
                      ),
                    ),
                  ),
                  Divider(
                    color: Colors.white,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                       _editingAdProvider.setProduct(product);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => EditingAdScreen(),
                        ),
                      );
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        'تعديل الإعلان',
                        style: TextStyle(
                           color: Colors.white,
                            fontWeight: FontWeight.bold, fontSize: 14),
                      ),
                    ),
                  )
                ],
              ));
        });
  }
void _refreshAd(int adId) async {
 setState(() {
            _isLoading = true;
          });
          var result = await _apiProvider
              .post(Urls.REFRESH_PRODUCT_URL + adId.toString(), header: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": 'Bearer ${_authProvider.currentUser.accessToken}'
          });
          setState(() {
            _isLoading = false;
          });
          if (result["status_code"] == 200) {
Navigator.pop(context);
            Commons.showMessageDialogNoImg(context: context,msg: 
             result['response']['msg']);
          }
       else if (result["status_code"] == 401) 
            Commons.showError(
                context: context,
                message: 'يرجى تسجيل الدخول مجدداً',
                onTapOk: () {
                  Navigator.pop(context);

                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/login_screen', (Route<dynamic> route) => false);
                  SharedPreferencesHelper.remove("user");
                });
           else 
            Commons.showMessageDialog(
              msg: result['response']['msg'],
              context: context,
            );
          
}
  void _deleteAd(int adId) async {
    Navigator.pop(context);
    await Commons.showModelSheet(
        context: context,
        height: _height,
        width: _width,
        msg: 'هل تريد حذف الاعلان',
        imgUrl: 'assets/images/pin.png',
        ok: () async {
          setState(() {
            _isLoading = true;
          });
          var result = await _apiProvider
              .delete(Urls.DELETE_PRODUCT + adId.toString(), header: {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": 'Bearer ${_authProvider.currentUser.accessToken}'
          });
          setState(() {
            _isLoading = false;
          });
          if (result["status_code"] == 200) {
            Commons.showMessageDialogNoImg(
              msg: "تم حذف الاعلان بنجاح",
              context: context,
            );
             _userProductList =  _userAdsProvider.getMyProductList(context);
          } else if (result["status_code"] == 401) 
            Commons.showError(
                context: context,
                message: 'يرجى تسجيل الدخول مجدداً',
                onTapOk: () {
                  Navigator.pop(context);

                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/login_screen', (Route<dynamic> route) => false);
                  SharedPreferencesHelper.remove("user");
                });
           else 
            Commons.showMessageDialog(
              msg: result['response']['error'],
              context: context,
            );
          
        });
  }
}
