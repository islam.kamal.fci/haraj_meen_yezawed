import 'package:flutter/material.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/providers/ad_details_provider.dart';
import 'package:haraj/ui/ad/widgets/comment_widget.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:provider/provider.dart';

class ShowingAllCommentsScreen extends StatefulWidget {
  @override
  _ShowingAllCommentsScreenState createState() =>
      _ShowingAllCommentsScreenState();
}

class _ShowingAllCommentsScreenState extends State<ShowingAllCommentsScreen> {
  AdDetailsProvider _adDetailsProvider;
  Widget _buildBodyItem() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: ListView.builder(
          itemCount: _adDetailsProvider.productDetails.comments.length,
          itemBuilder: (context, index) {
            return CommentWidget(
                comment: _adDetailsProvider.productDetails.comments[index]);
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    AppBar appBar = AppBar(
      centerTitle: true,
      title: Text(
        ' عرض كافة التعليقات',
        style: Theme.of(context).textTheme.headline1,
      ),
    );
    _adDetailsProvider = Provider.of<AdDetailsProvider>(context);
    return NetworkIndicator(
        child: PageContainer(
            child: Scaffold(
              backgroundColor: greenColor,

              appBar: appBar,
      body: _buildBodyItem(),
    )));
  }
}
