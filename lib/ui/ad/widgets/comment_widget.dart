import 'package:flutter/material.dart';
import 'package:haraj/models/comment.dart';
import 'package:haraj/utils/app_colors.dart';

class CommentWidget extends StatelessWidget {
  final Comment comment;

  const CommentWidget({Key key, this.comment}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Container(
      //height: 500,
      margin: EdgeInsets.symmetric(horizontal: width * 0.05, vertical: 5),
      decoration: BoxDecoration(
        border: Border.all(width: 1.0, color: blackColor),
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      child: Column(
        children: [
          Container(
            margin:
                EdgeInsets.symmetric(horizontal: width * 0.05, vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  comment.userName,
                  style: TextStyle(
                      color: whiteColor,
                      fontSize: 12,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  comment.createdAt,
                  style: TextStyle(
                    color: whiteColor,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 15,vertical: 5),
            child: Text(
              comment.body,
              style: TextStyle(color: whiteColor, fontSize: 12),
            ),
          ),
       /*   Container(
            margin: EdgeInsets.symmetric(horizontal: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  width: 100,
                ),
                Image.asset(
                  'assets/images/thumb-up-line.png',
                  color: Colors.white,
                )
              ],
            ),
          )*/
        ],
      ),
    );
  }
}
