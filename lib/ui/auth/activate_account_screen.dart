import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/providers/navigation_provider.dart';
import 'package:haraj/shared_preferences/shared_preferences_helper.dart';
import 'package:haraj/ui/auth/widgets/auth_header.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';

import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';

class ActivateAccountScreen extends StatefulWidget {
  @override
  _ActivateAccountScreenState createState() => _ActivateAccountScreenState();
}

class _ActivateAccountScreenState extends State<ActivateAccountScreen> {
  double _height, _width;
  TextEditingController _textEditingController = TextEditingController();
  StreamController<ErrorAnimationType> _errorController;
  ApiProvider _apiProvider = ApiProvider();
  AuthProvider _authProvider;
  String _verificationCode = '';
  NavigationProvider _navigationProvider;
  bool _isLoading = false, _resendLoading = false, intialRun = true;

  Widget _buildBodyWidget() {
    return ListView(
      children: <Widget>[
        Header(
          title: 'كود التفعيل',
          description:
              'تم ارسال رسالة الى بريدك مع رمز لتفعيل الحساب الخاص بك',
          setBackButton: true,
          image_path: 'assets/images/code.png',
        ),
        Padding(
          padding: EdgeInsets.symmetric(
              vertical: _height * 0.025, horizontal: _width * 0.07),
          child: Directionality(
            textDirection: TextDirection.ltr,
            child: PinCodeTextField(
                textStyle: TextStyle(color: Colors.white),
                backgroundColor: greenColor,
                appContext: context,
                length: 4,
                pinTheme: PinTheme(
                    shape: PinCodeFieldShape.box,
                    selectedFillColor: Colors.black,
                    borderRadius: BorderRadius.circular(5),
                    fieldHeight: _height * 0.075,
                    fieldWidth: _width * 0.15,
                    selectedColor: mainAppColor,
                    activeColor: Colors.white,
                    inactiveColor: Colors.red,
                    borderWidth: 1),
                animationDuration: Duration(milliseconds: 300),
                animationType: AnimationType.scale,
                controller: _textEditingController,
                errorAnimationController: _errorController,
                onChanged: (val) => _verificationCode = val,
                onCompleted: (String value) {
                  _verificationCode = value;
                  _activateUser();
                }),
          ),
        ),
        GestureDetector(
          onTap: () => _resendActivationCode(),
          child: Container(
              margin: EdgeInsets.symmetric(
                  vertical: _height * 0.005, horizontal: _width * 0.05),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('اعاده ارسال الكود؟',
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                          fontWeight: FontWeight.w600)),
                  _resendLoading
                      ? Container(
                          margin: EdgeInsets.only(right: _width * 0.05),
                          child: SpinKitDoubleBounce(
                            color: mainAppColor,
                            size: 30,
                          ),
                        )
                      : Container()
                ],
              )),
        ),
        _isLoading
            ? Container(
                margin: EdgeInsets.only(top: _height * 0.05),
                child: SpinKitDoubleBounce(color: mainAppColor),
              )
            : Container(
                margin: EdgeInsets.only(top: _height * 0.05),
                child: CustomButton(
                    btnLbl: 'تفعيل',
                    btnStyle: TextStyle(color: whiteColor),
                    onPressedFunction: () => {
                          if (_verificationCode.length == 4)
                            _activateUser()
                          else
                            _errorController.add(ErrorAnimationType.shake)
                        }),
              ),
      ],
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (intialRun) {
      _authProvider = Provider.of<AuthProvider>(context);
      _navigationProvider = Provider.of<NavigationProvider>(context);
      intialRun = false;
    }
  }

  @override
  void initState() {
    _errorController = StreamController<ErrorAnimationType>();
    super.initState();
  }

  @override
  void dispose() {
    _errorController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;
    return NetworkIndicator(
        child: PageContainer(
      child: Scaffold(
        backgroundColor: greenColor,
        body: _buildBodyWidget(),
      ),
    ));
  }

  void _resendActivationCode() async {
    setState(() {
      _resendLoading = true;
    });
    var result = await _apiProvider.post(Urls.RESEND_CODE_URL, body: {
      "phone": _authProvider.userPhone,
    });
    setState(() {
      _resendLoading = false;
    });
    if (result["status_code"] == 200) {
      _textEditingController.clear();
      _verificationCode = '';
      Commons.showToast(
          message: "تم اعاده ارسال الكود بنجاح",
          context: context,
          color: hintColor);
    } else if (result["status_code"] == 401) {
      Commons.showError(
          context: context,
          message: 'يرجى تسجيل الدخول مجدداً',
          onTapOk: () {
            Navigator.pop(context);

            Navigator.of(context).pushNamedAndRemoveUntil(
                '/login_screen', (Route<dynamic> route) => false);
            SharedPreferencesHelper.remove("user");
          });
    } else {
      Commons.showToast(
          message: result['response']['error'],
          context: context,
          color: hintColor);
    }
  }

  void _activateUser() async {
    setState(() {
      _isLoading = true;
    });
    var result = await _apiProvider.post(Urls.CONFIRM_USER_URL, body: {
      "phone": _authProvider.userPhone,
      "confirmation_code": _verificationCode.toString()
    });
    setState(() {
      _isLoading = false;
    });
    if (result["status_code"] == 200) {
      Commons.showToast(
          message: result['response']['msg'],
          context: context,
          color: hintColor);
      _authProvider.setCurrentUser(User.fromJson(result['response']['data']));
      SharedPreferencesHelper.save("user", _authProvider.currentUser);
      _navigationProvider.upadateNavigationIndex(0, context);
      Navigator.of(context).pushNamedAndRemoveUntil(
          '/navigation', (Route<dynamic> route) => false);
    } else {
      Commons.showToast(
          message: result['response']['error'],
          context: context,
          color: hintColor);
    }
  }
}
