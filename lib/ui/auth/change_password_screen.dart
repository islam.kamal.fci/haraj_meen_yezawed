import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/validation_mixin.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/ui/auth/widgets/auth_header.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';
import 'package:provider/provider.dart';

class ChangePasswordScreen extends StatefulWidget {
  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen>
    with ValidationMixin {
  double _height;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool _isLoading = false;
  String _newPassword, _confirmedPassword;
  AuthProvider _authProvider;
  ApiProvider _apiProvider = ApiProvider();
  final _formKey = GlobalKey<FormState>();
  Widget _buildBodyWidget() {
    return Form(
      key: _formKey,
      child: ListView(
        children: <Widget>[
          Header(
            title: 'اعادة تعيين كلمه السر',
            setBackButton: true,
            image_path: 'assets/images/lock.png',
          ),
          Container(
            margin: EdgeInsets.only(bottom: _height * 0.015),
            child: CustomTextFormField(
              labelText: 'كلمه السر الجديدة',
              isPassword: true,
               maxLines: 1,
              labelStyle: TextStyle(color: mainAppColor),
              hintTxt: "***************",
              hintStyle: TextStyle(color: blackColor),
              fillColor: whiteColor,
              onChangedFunc: (String text) {
                _newPassword = text.toString();
              },
              validationFunc: validatePassword,
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: _height * 0.015),
            child: CustomTextFormField(
              labelText: 'تأكيد كلمة السر الجديدة',
              isPassword: true,
              onChangedFunc: (String text) {
                _confirmedPassword = text.toString();
              },
              labelStyle: TextStyle(color: mainAppColor),

              maxLines: 1,
              hintStyle: TextStyle(color: blackColor),
               fillColor: whiteColor,
              validationFunc: validateConfirmPassword,
            ),
          ),
          _isLoading
              ? Container(
                  margin: EdgeInsets.only(top: _height * 0.05),
                  child: SpinKitDoubleBounce(color: mainAppColor),
                )
              : Container(
                  margin: EdgeInsets.only(top: _height * 0.05),
                  child: CustomButton(
                    btnLbl: 'تأكيد',
                    btnStyle: TextStyle(color: whiteColor),
                    onPressedFunction: () => _resetPassword(),
                  ),
                ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _authProvider = Provider.of<AuthProvider>(context);
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    // _width = MediaQuery.of(context).size.width;
    return NetworkIndicator(
        child: PageContainer(
            child: Scaffold(
                backgroundColor: greenColor, body: _buildBodyWidget())));
  }

  void _resendActivationCode() async {
    var result = await _apiProvider.post(Urls.RESEND_CODE_URL, body: {
      "phone": _authProvider.userPhone,
    });

    if (result["status_code"] == 200)
      Commons.showToast(
          message: result['response']['msg'],
          context: context,
          color: hintColor);
    else
      Commons.showToast(
          message: result['response']['error'],
          context: context,
          color: hintColor);
  }

  _resetPassword() async {
    if (_formKey.currentState.validate()) {
      _firebaseMessaging.getToken().then((token) async {
        setState(() {
          _isLoading = true;
        });
        print('code:' + _authProvider.confirmationCode);
        var result = await _apiProvider.post(Urls.NEW_PASSWORD_URL, body: {
          "phone": _authProvider.userPhone,
          "password": _newPassword,
          "password_confirmation": _confirmedPassword,
          "confirmation_code": _authProvider.confirmationCode,
          "firebase_clouding_msg_token": token
        });
        setState(() {
          _isLoading = false;
        });
        if (result["status_code"] == 200) {
          Commons.showToast(
              message: result['response']['msg'],
              context: context,
              color: hintColor);

          Navigator.of(context).pushNamedAndRemoveUntil(
              '/login_screen', (Route<dynamic> route) => false);
        } else if (result["status_code"] == 403) {
          Commons.showError(
              context: context,
              message: result['response']['error'],
              onTapOk: () {
                _resendActivationCode();
                Navigator.pushNamed(context, '/activate_account_screen');
              });
        } else
          Commons.showMessageDialog(
              context: context, msg: result['response']['error']);
      });
    }
  }
}
