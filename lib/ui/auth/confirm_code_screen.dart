import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/ui/auth/widgets/auth_header.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';

import '../../shared_preferences/shared_preferences_helper.dart';

class ConfirmCodeScreen extends StatefulWidget {
  @override
  _ConfirmCodeScreenState createState() => _ConfirmCodeScreenState();
}

class _ConfirmCodeScreenState extends State<ConfirmCodeScreen> {
  double _height, _width;
  TextEditingController _textEditingController = TextEditingController();
  ApiProvider _apiProvider = ApiProvider();
  String _code;
  AuthProvider _authProvider;
  bool _isLoading = false, _resendLoading = false, intialRun = true;

  StreamController<ErrorAnimationType> _errorController;

  @override
  void initState() {
    _errorController = StreamController<ErrorAnimationType>();
    super.initState();
  }

  @override
  void dispose() {
    _errorController.close();

    super.dispose();
  }

  Widget _buildBodyWidget() {
    return ListView(
      children: <Widget>[
        Header(
          title: 'كود التأكيد',
          description:
              'تم ارسال رسالة نصية الى جوالك مع رمز لاعادة تعيين كلمة المرور الخاصة بك',
          setBackButton: true,
          image_path: 'assets/images/code.png',
        ),
    Padding(
    padding: EdgeInsets.symmetric(
    vertical: _height * 0.025, horizontal: _width * 0.07),
    child: Directionality(
    textDirection: TextDirection.ltr,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Text('كود التاكيد',
            style: TextStyle(
                fontSize: 15,
                color: whiteColor,
                fontWeight: FontWeight.w600)),
      ],
    )
    )),
        Padding(
          padding: EdgeInsets.symmetric(
              vertical: _height * 0.025, horizontal: _width * 0.07),
          child: Directionality(
            textDirection: TextDirection.ltr,
            child: PinCodeTextField(
                appContext: context,
                length: 4,
                textStyle: TextStyle(color: blackColor),
                backgroundColor: whiteColor,
                pinTheme: PinTheme(
                    shape: PinCodeFieldShape.underline,
                    selectedFillColor: Colors.black,
                    borderRadius: BorderRadius.circular(5),
                    fieldHeight: _height * 0.075,
                    fieldWidth: _width * 0.15,
                    selectedColor: blackColor,
                    activeColor: mainAppColor,
                    inactiveColor: blackColor,
                    borderWidth: 1),
                animationDuration: Duration(milliseconds: 300),
                animationType: AnimationType.scale,
                controller: _textEditingController,
                errorAnimationController: _errorController,
                onChanged: (val) {
                  _code = val;
                },
                onCompleted: (String value) {
                  _code = value;
                  _confirmCode();
                }),
          ),
        ),
        GestureDetector(
          onTap: () => _resendCode(),
          child: Container(
              margin: EdgeInsets.symmetric(
                  vertical: _height * 0.005, horizontal: _width * 0.05),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text('اعاده ارسال الكود؟',
                      style: TextStyle(
                          fontSize: 15,
                          color: whiteColor,
                          fontWeight: FontWeight.w600)),
                  _resendLoading
                      ? Container(
                          margin: EdgeInsets.only(right: _width * 0.05),
                          child: SpinKitDoubleBounce(
                            color: mainAppColor,
                            size: 30,
                          ),
                        )
                      : Container()
                ],
              )),
        ),
        _isLoading
            ? Container(
                margin: EdgeInsets.only(top: _height * 0.05),
                child: SpinKitDoubleBounce(color: mainAppColor),
              )
            : Container(
                margin: EdgeInsets.only(top: _height * 0.05),

                child: CustomButton(
                    btnLbl: 'ارسال',
                    btnStyle: TextStyle(color: whiteColor),
                    onPressedFunction: () {
                      if (_code.length == 4)
                        _confirmCode();
                      else
                        _errorController.add(ErrorAnimationType.shake);
                    }),
              ),
      ],
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (intialRun) {
      _authProvider = Provider.of<AuthProvider>(context);
      _errorController = StreamController<ErrorAnimationType>();
      intialRun = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;
    return NetworkIndicator(
        child: PageContainer(
      child: Scaffold(
        backgroundColor: greenColor,
        body: _buildBodyWidget(),
      ),
    ));
  }

  void _resendCode() async {
    setState(() {
      _resendLoading = true;
    });
    var result = await _apiProvider.post(Urls.RESEND_CODE_URL, body: {
      "login": _authProvider.userPhone,
    });
    setState(() {
      _resendLoading = false;
    });
    if (result["status_code"] == 200) {
      _textEditingController.clear();
      _code = '';
      Commons.showToast(
          message: "تم اعاده ارسال الكود بنجاح",
          context: context,
          color: hintColor);
      _authProvider.setConfirmationCode(
          result['response']['confirmation_code'].toString());
    } else
      Commons.showToast(
          message: result['response']['error'],
          context: context,
          color: hintColor);
  }

  void _confirmCode() async {
    
    setState(() {
      _isLoading = true;
    });
    var result = await _apiProvider.post(Urls.CHECK_CODE_URL, body: {
      "login": _authProvider.userPhone,
      "confirmation_code" : _code
    });
    setState(() {
      _isLoading = false;
    });
    if (result["status_code"] == 200) {
  
      Commons.showToast(
          message:  result['response']['msg'],
          context: context,
          color: hintColor);
      _authProvider.setConfirmationCode(_code);

      Navigator.pushNamed(context, '/change_password_screen');
    }  else
      Commons.showToast(
          message: result['response']['msg'],
          context: context,
          color: hintColor);
  }
}
