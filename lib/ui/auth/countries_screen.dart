import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/models/city.dart';
import 'package:haraj/providers/cities_provider.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:provider/provider.dart';

class CountriesScreen extends StatefulWidget {
  @override
  _CountriesScreenState createState() => _CountriesScreenState();
}

class _CountriesScreenState extends State<CountriesScreen> {
  // double _height, _width;
  AppBar _appBar;

  @override
  Widget build(BuildContext context) {
    // _height =
    //     MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    // _width = MediaQuery.of(context).size.width;
    _appBar = AppBar(
      backgroundColor: greenColor,
      centerTitle: true,
      title: Text(
        'أختر الدولة',
        style: TextStyle(color: whiteColor),
      ),
      leading: InkWell(
        onTap: () => Navigator.pop(context),
        child: Image.asset('assets/images/arrow_back.png'),
      ),
    );
    return NetworkIndicator(
      child: PageContainer(
        child: Scaffold(
          backgroundColor: greenColor,
          appBar: _appBar,
          body: _buildBodyWidget(),
        ),
      ),
    );
  }

  Widget _buildBodyWidget() {
    return FutureBuilder<List<City>>(
        future: Provider.of<CitiesProvider>(context, listen: false)
            .getCountriesLists(context),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return SpinKitDoubleBounce(color: mainAppColor);
            case ConnectionState.active:
              return Text('');
            case ConnectionState.waiting:
              return SpinKitDoubleBounce(color: mainAppColor);
            case ConnectionState.done:
              if (snapshot.hasError) {
                print(snapshot.error.toString());
                return Center(
                  child: Text('حدث خطأ ما'),
                );
              } else if (snapshot.data.length == 0) {
                return Center(
                  child: Text(
                    'لا توجد دول',
                    style: TextStyle(color: hintColor, fontSize: 16),
                    textAlign: TextAlign.center,
                  ),
                );
              } else {
                return Container(
                  margin: EdgeInsets.only(top: 10),
                  child: ListView.separated(
                    physics: BouncingScrollPhysics(),
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                          onTap: ()=>   Navigator.pop(context, snapshot.data[index]),
                          
                          child: Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 10),
                              child: Text(snapshot.data[index].name ,
                               style: TextStyle(
                                color: Colors.white
                              ))),
                        );
                      
                    },
                    separatorBuilder: (context, index) {
                      return Divider(
                        color: Colors.white,
                      );
                    },
                  ),
                );
              }
          }
          return SpinKitDoubleBounce(color: mainAppColor);
        });
  }
}
