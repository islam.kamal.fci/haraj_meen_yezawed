import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/validation_mixin.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/ui/auth/widgets/auth_header.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';
import 'package:provider/provider.dart';

class ForgetPasswordScreen extends StatefulWidget {
  @override
  _ForgetPasswordScreenState createState() => _ForgetPasswordScreenState();
}

class _ForgetPasswordScreenState extends State<ForgetPasswordScreen>
    with ValidationMixin {
  double _height;
  String _userPhone;
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false, intialRun = true;
  AuthProvider _authProvider;
  //  var _maskFormatter = new MaskTextInputFormatter(
  //     mask: '05yxxxxxxx', filter: {"x": RegExp(r'[0-9]'),
  //     "y": RegExp(r'(0|3|4|5|6|7|8|9)'),
  //     });

  ApiProvider _apiProvider = ApiProvider();
  Widget _buildBodyWidget() {
    return Form(
      key: _formKey,
      child: ListView(
        children: <Widget>[
          Header(
            title: 'هل نسيت كلمة المرور ؟',
            description:
                'ستتلقى رمزا عشوائيا على جوالك لاعادة تعيين كلمة المرور الخاصة بك',
            setBackButton: true,
            image_path: 'assets/images/reset.png',
          ),
          Container(
            margin:
                EdgeInsets.only(top: _height * 0.05, bottom: _height * 0.07),
            child: CustomTextFormField(
                   labelText: 'رقم الجوال/الايميل',
             hintTxt: 'رقم الجوال/الايميل',
            hintStyle: TextStyle(color: blackColor),
            fillColor: whiteColor,
                inputData: TextInputType.text,
                onChangedFunc: (String text) {
                  _userPhone = text.toString();
                },
                validationFunc: validatePhoneNo),
          ),
          _isLoading
              ? Container(
                  margin: EdgeInsets.only(
                      bottom: _height * 0.02, top: _height * 0.05),
                  child: SpinKitDoubleBounce(color: mainAppColor),
                )
              : Container(
                  child: CustomButton(
                    btnLbl: 'اعاده تعيين كلمة المرور',
                    btnStyle:TextStyle(color: whiteColor),
                    onPressedFunction: () => _forgetPassword(),
                  ),
                ),
        ],
      ),
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (intialRun) {
      _authProvider = Provider.of<AuthProvider>(context);
      intialRun = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;

    return NetworkIndicator(
        child: PageContainer(
      child: Scaffold(
        backgroundColor: greenColor,
        body: _buildBodyWidget(),
      ),
    ));
  }

  _forgetPassword() async {
    if (_formKey.currentState.validate()) {
      setState(() {
        _isLoading = true;
      });
      var result = await _apiProvider
          .post(Urls.RESEND_CODE_URL, body: {"login": _userPhone});
      setState(() {
        _isLoading = false;
      });
      if (result["status_code"] == 200) {
        Commons.showToast(
            message: result['response']['msg'],
            context: context,
            color: hintColor);
        _authProvider.setUserPhone(_userPhone);
       
        Navigator.pushReplacementNamed(context, '/confirmation_code_screen');

      } else 
        Commons.showMessageDialog(
            context: context, msg: result['response']['error']);
      
    }
  }
}
