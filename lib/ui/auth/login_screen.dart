import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/validation_mixin.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/providers/navigation_provider.dart';
import 'package:haraj/shared_preferences/shared_preferences_helper.dart';
import 'package:haraj/ui/auth/widgets/auth_header.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';
import 'package:provider/provider.dart';

import '../../custom_widgets/connectivity/network_indicator.dart';
import '../../custom_widgets/safe_area/page_container.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with ValidationMixin {
  double _height, _width;
  final _formKey = GlobalKey<FormState>();
  ApiProvider _apiProvider = ApiProvider();
  String _userPhone, _userPassword;
  AuthProvider _authProvider;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  NavigationProvider _navigationProvider;
  bool _isLoading = false, intialRun = true;

  //  var _maskFormatter = new MaskTextInputFormatter(
  //     mask: '05yxxxxxxx', filter: {"x": RegExp(r'[0-9]'),
  //     "y": RegExp(r'(0|3|4|5|6|7|8|9)'),
  //     });

  @override
  Widget build(BuildContext context) {
    _authProvider = Provider.of<AuthProvider>(context);
    _navigationProvider = Provider.of<NavigationProvider>(context);
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;
    return NetworkIndicator(
      child: PageContainer(
          child: Scaffold(
              backgroundColor: greenColor, body: _buildBodyWidget())),
    );
  }

  Widget _buildBodyWidget() {
    return Form(
      key: _formKey,
      child: ListView(children: [
        Header(
          title: 'مرحبا بك من جديد',
          setBackButton: false,
          description: 'سجل دخولك للاستمرار',
          image_path: 'assets/images/login.png',
        ),
        Container(
          margin: EdgeInsets.only(bottom: _height * 0.007),
          child: CustomTextFormField(
            fillColor: whiteColor,
            inputData: TextInputType.text,
            labelStyle: TextStyle(color: mainAppColor),

            labelText: 'رقم الجوال/الايميل',
            hintTxt: 'رقم الجوال/الايميل',
            hintStyle: TextStyle(color: blackColor),


            // prefixIcon: Image.asset(
            //   'assets/images/saudi-arabia.png',
            //   width: 15,
            // ),
            validationFunc: validatePhoneNo,
            onChangedFunc: (String text) {
              _userPhone = text.toString();
            },
          ),
        ),
        CustomTextFormField(
          maxLines: 1,
          fillColor: whiteColor,
          inputData: TextInputType.text,
          isPassword: true,
          labelText: 'كلمه المرور',
          labelStyle: TextStyle(color: mainAppColor),
          validationFunc: validatePassword,
          onChangedFunc: (String text) {
            _userPassword = text.toString();
          },
        ),
        Container(
          margin: EdgeInsets.symmetric(
              vertical: _height * 0.05, horizontal: _width * 0.05),
          child: InkWell(
            onTap: () =>
                Navigator.pushNamed(context, '/forget_password_screen'),
            child: Text('هل نسيت كلمة المرور ؟',
                style: TextStyle(color: redColor, fontSize: 16)),
          ),
        ),
        _isLoading
            ? Container(
                margin: EdgeInsets.only(
                    bottom: _height * 0.02, top: _height * 0.05),
                child: SpinKitDoubleBounce(color: mainAppColor),
              )
            : CustomButton(
                btnLbl: 'تسجيل الدخول',
                btnColor: mainAppColor,
                btnStyle: TextStyle(color: whiteColor,fontSize: 16),
                onPressedFunction: () => _login(),
              ),
        InkWell(
            onTap: () => Navigator.pushNamed(context, '/signup_screen'),
            child: Container(
                margin: EdgeInsets.symmetric(vertical: _height * 0.02),
                child: Center(
                    child: RichText(
                  text: TextSpan(
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'ArefRuqaa',
                          color: blackColor,
                          fontWeight: FontWeight.bold),
                      children: [
                        TextSpan(
                            text: 'ليس لديك حساب؟ ',
                            style: TextStyle(color: whiteColor)),
                        TextSpan(
                            text: 'تسجيل حساب',
                            style: TextStyle(color: mainAppColor, fontSize: 16,)),
                      ]),
                )))),
        SizedBox(
          height: 15,
        ),
        InkWell(
          onTap: () {
            Navigator.of(context).pushNamedAndRemoveUntil(
                '/navigation', (Route<dynamic> route) => false);
            _navigationProvider.upadateNavigationIndex(0, context);
          },
          child: Center(
            child: Text(
              ' تخطي للرئيسية',
              style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'ArefRuqaa',
                  color: whiteColor,
                  fontWeight: FontWeight.bold),
            ),
          ),
        )
      ]),
    );
  }

  void _resendActivationCode() async {
    var result = await _apiProvider.post(Urls.RESEND_CODE_URL, body: {
      "phone": _userPhone,
    });

    if (result["status_code"] == 200)
      Commons.showToast(
          message: result['response']['msg'],
          context: context,
          color: hintColor);
    else
      Commons.showToast(
          message: result['response']['error'],
          context: context,
          color: hintColor);
  }

  void _login() async {
    if (_formKey.currentState.validate()) {
      _firebaseMessaging.getToken().then((token) async {
        setState(() {
          _isLoading = true;
        });
        var result = await _apiProvider.post(Urls.LOGIN_URL, body: {
          "login": _userPhone,
          "password": _userPassword,
          "firebase_clouding_msg_token": token
        });
        setState(() {
          _isLoading = false;
        });
        if (result["status_code"] == 200) {
          _authProvider
              .setCurrentUser(User.fromJson(result['response']['data']));

          SharedPreferencesHelper.save("user", _authProvider.currentUser);
          print('token' + _authProvider.currentUser.accessToken);
          _navigationProvider.upadateNavigationIndex(0, context);
          Navigator.of(context).pushNamedAndRemoveUntil(
              '/navigation', (Route<dynamic> route) => false);
        } else if (result["status_code"] == 403) {
          _authProvider.setUserPhone(_userPhone);
          Commons.showError(
              context: context,
              message: result['response']['error'],
              onTapOk: () {
                _resendActivationCode();
                Navigator.pushNamed(context, '/activate_account_screen');
              });
        } else
          Commons.showToast(
              message: result['response']['error'],
              context: context,
              color: hintColor);
      });
    }
  }
}
