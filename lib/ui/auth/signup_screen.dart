import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/validation_mixin.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/ui/auth/countries_screen.dart';
import 'package:haraj/ui/auth/widgets/auth_header.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:provider/provider.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> with ValidationMixin {
  double _height, _width;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  String _userName, _userPhone, _userEmail, _userPassword;
  bool _initialRun = true;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  ApiProvider _apiProvider = ApiProvider();
  AuthProvider _authProvider;
  bool _isLoading = false, intialRun = true;
  //  var _maskFormatter = new MaskTextInputFormatter(
  //     mask: '05yxxxxxxx', filter: {"x": RegExp(r'[0-9]'),
  //     "y": RegExp(r'(0|3|4|5|6|7|8|9)'),
  //     });

  @override
  void didChangeDependencies() {
    if (_initialRun) {
      _authProvider = Provider.of<AuthProvider>(context);
      _authProvider.setSelectedCountry(null, notifyListener: false);
      _authProvider.setSelectedCountryError(false, notifyListener: false);
      _initialRun = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;

    return NetworkIndicator(
        child: PageContainer(
            child: Scaffold(
                backgroundColor: greenColor, body: _buildBodyWidget())));
  }

  void _chooseCountry() async {
    final country = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CountriesScreen()),
    );
    if (country != null) {
      _authProvider.setSelectedCountry(country);
    }
  }

  Widget _buildBodyWidget() {
    return Form(
      key: _formKey,
      child: ListView(children: [
        Header(
          title: 'مرحبا بك في حراج',
          setBackButton: true,
          description: 'ان لم يكن لديك حساب سجل حساب جديد',
          image_path: 'assets/images/register.png',
        ),
        InkWell(
          onTap: () => _chooseCountry(),
          child: Padding(
            padding: EdgeInsets.only(bottom: 10),
            child:  Container(
                margin: EdgeInsets.symmetric(horizontal: _width * 0.07),
                decoration: BoxDecoration(
                    border: Border.all(color: whiteColor),
                  color: whiteColor,

                ),
                child: Column(
                  children: [
                    Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              _authProvider.selectedCountry == null
                                  ? 'الدولة'
                                  : _authProvider.selectedCountry.name,
                              style: TextStyle(
                                  color: _authProvider.selectedCountry == null
                                      ? mainAppColor
                                      : mainAppColor),
                            ),
                            Image.asset(
                              'assets/images/arrow_down.png',
                              color: mainAppColor,
                            )
                          ],
                        )),
                    Divider(
                      height: 3,
                    )
                  ],
                )),
          ),


        ),
        Consumer<AuthProvider>(builder: (context, authProvider, child) {
          return authProvider.selectedCountryError
              ? Container(
                  margin: EdgeInsets.only(
                    top: 10,
                    right: _width * 0.07,
                    left: _width * 0.07,
                    // bottom: 10
                  ),
                  child: Text(
                    'يرجى اختيار الدولة',
                    style: TextStyle(color: mainAppColor, fontSize: 13),
                  ))
              : Container();
        }),
        CustomTextFormField(
          inputData: TextInputType.text,
          labelText: 'الأسم',
          labelStyle: TextStyle(color: mainAppColor, fontWeight: FontWeight.bold),
          validationFunc: validateUserName,
          fillColor: whiteColor,
          onChangedFunc: (String text) {
            _userName = text.toString();
          },
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 5),
          child: CustomTextFormField(
            inputData: TextInputType.number,
            labelText: 'رقم الجوال',
            fillColor: whiteColor,
            labelStyle:
                TextStyle(color: mainAppColor, fontWeight: FontWeight.bold),

            hintTxt: '05xxxxxxxx',
            // prefixIcon: Image.asset(
            //   'assets/images/saudi-arabia.png',
            //   width: 15,
            // ),
            validationFunc: validatePhoneNo,
            onChangedFunc: (String text) {
              _userPhone = text.toString();
            },
          ),
        ),
        CustomTextFormField(
          inputData: TextInputType.text,
          labelText: 'البريد الألكتروني',
          fillColor: whiteColor,
          labelStyle: TextStyle(color: mainAppColor, fontWeight: FontWeight.bold),
          validationFunc: validateEmail,
          onChangedFunc: (String text) {
            _userEmail = text.toString();
          },
        ),
        Container(
            margin: EdgeInsets.symmetric(vertical: 5),
            child: CustomTextFormField(
              inputData: TextInputType.text,
              isPassword: true,
              maxLines: 1,
              labelText: 'كلمه المرور',
              fillColor: whiteColor,
              labelStyle:
                  TextStyle(color: mainAppColor, fontWeight: FontWeight.bold),
              validationFunc: validatePassword,
              onChangedFunc: (String text) {
                _userPassword = text.toString();
              },
            )),
        CustomTextFormField(
          inputData: TextInputType.text,
          isPassword: true,
          maxLines: 1,
          labelText: ' تأكيد كلمه المرور',
          fillColor: whiteColor,
          labelStyle: TextStyle(color: mainAppColor, fontWeight: FontWeight.bold),
          validationFunc: validateConfirmPassword,
          onChangedFunc: (String text) {
            _userPassword = text.toString();
          },
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 20),
          child: _isLoading
              ? SpinKitDoubleBounce(color: mainAppColor)
              : CustomButton(
                  btnLbl: 'انشاء حساب',
                  btnStyle: TextStyle(color: whiteColor),
                  onPressedFunction: () async {
                    if (_formKey.currentState.validate() &
                        checkValidationOfSignUp(_authProvider)) {
                      _firebaseMessaging.getToken().then((token) async {
                        setState(() {
                          _isLoading = true;
                        });
                        var result =
                            await _apiProvider.post(Urls.SIGNUP_URL, body: {
                          "country_id":
                              _authProvider.selectedCountry.id.toString(),
                          "name": _userName,
                          "phone": _userPhone,
                          "email": _userEmail,
                          "password": _userPassword,
                          "password_confirmation": _userPassword,
                          "firebase_clouding_msg_token": token
                        }, header: {
                          "Accept": "application/json",
                          "Content-Type": "application/json"
                        });
                        setState(() {
                          _isLoading = false;
                        });
                        if (result["status_code"] == 200) {
                          Commons.showToast(
                              message: result['response']['msg'],
                              context: context,
                              color: hintColor);
                          _authProvider.setUserPhone(
                              result['response']['data']['phone'].toString());

                          Navigator.pushReplacementNamed(
                              context, '/activate_account_screen');
                        } else
                          Commons.showError(
                            message: result['response']['error'],
                            context: context,
                          );
                      });
                    }
                  }),
        ),
        GestureDetector(
            onTap: () => Navigator.pushNamed(context, '/login_screen'),
            child: Center(
                child: RichText(
              text: TextSpan(
                  style: TextStyle(
                      fontSize: 15,
                      fontFamily: 'ArefRuqaa',
                      color: whiteColor,
                      fontWeight: FontWeight.bold),
                  children: [
                    TextSpan(text: 'لديك حساب؟ '),
                    TextSpan(
                        text: 'تسجيل دخول', style: TextStyle(color: mainAppColor)),
                  ]),
            ))),
        GestureDetector(
            onTap: () =>
                Navigator.pushNamed(context, '/terms_and_rules_screen'),
            child: Container(
                margin: EdgeInsets.symmetric(
                    vertical: 25, horizontal: _width * 0.04),
                child: Center(
                    child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                      style: TextStyle(
                          height: 1.6,
                          fontSize: 13,
                          fontFamily: 'ArefRuqaa',
                          color: whiteColor,
                          fontWeight: FontWeight.bold),
                      children: [
                        TextSpan(text: ' بدخولك يعد موافقه منك على '),
                        TextSpan(
                            text: 'شروط الاستخدام والشروط والأحكام',
                            style: TextStyle(color: mainAppColor)),
                        TextSpan(
                            text: ' الخاصه بحراج مين يزود',
                            style: TextStyle(color: mainAppColor)),
                      ]),
                ))))
      ]),
    );
  }
}
