import 'package:flutter/material.dart';
import 'package:haraj/utils/app_colors.dart';

class Header extends StatefulWidget {
  final String title;
  final String description;
  final bool setBackButton;
  final String image_path;
  const Header(
      {Key key,
      @required this.title,
      this.description,
        this.image_path,
      @required this.setBackButton})
      : super(key: key);
  @override
  _HeaderState createState() => _HeaderState();
}

class _HeaderState extends State<Header> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: Column(
         //   mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
           Row(
             children: [
               widget.setBackButton
                   ? GestureDetector(
                   onTap: () => Navigator.pop(context),
                   child: Image.asset(
                     'assets/images/arrow_back.png',
                     color: whiteColor,
                     height: 50,
                     width: 50,
                   ))
                   : Container(
                 width: 45,
               ),
             ],
           ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 30, bottom: 25),
                child: Image.asset('${widget.image_path}'),
              ),
              SizedBox(
                width: 45,
              )
            ],
          ),
        ),
        Center(
          child: Text(widget.title,
              style: TextStyle(
                  fontSize: 20,
                  color: whiteColor,
                  fontWeight: FontWeight.bold)),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
          child: Text(
            widget.description ?? '',
    style: TextStyle(
    fontSize: 16,
    color: whiteColor,
    fontWeight: FontWeight.normal),
            textAlign: TextAlign.center,

          ),
        ),
      ],
    );
  }
}
