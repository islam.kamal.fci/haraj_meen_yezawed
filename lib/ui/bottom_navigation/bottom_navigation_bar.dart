import 'dart:io';

import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/providers/navigation_provider.dart';
import 'package:haraj/ui/ad/adding_ad_screen.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:provider/provider.dart';

class BottomNavigation extends StatefulWidget {
  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  AuthProvider _authProvider;
  bool _initialRun = true;
  FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
  FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();

  void _iOSPermission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  void _firebaseCloudMessagingListeners() {
    var android = new AndroidInitializationSettings('mipmap/ic_launcher');
    var ios = new IOSInitializationSettings();
    var platform = new InitializationSettings(android: android, iOS: ios);
    _flutterLocalNotificationsPlugin.initialize(platform);

    if (Platform.isIOS) _iOSPermission();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');
        _showNotification(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');

        Navigator.pushNamed(context, '/notifications_screen');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');

        Navigator.pushNamed(context, '/notifications_screen');
      },
    );
  }

  _showNotification(Map<String, dynamic> message) async {
    var android = new AndroidNotificationDetails(
      'channel id',
      "CHANNLE NAME",
      "channelDescription",
    );
    var iOS = new IOSNotificationDetails();
    var platform = new NotificationDetails(android: android, iOS: iOS);
    await _flutterLocalNotificationsPlugin.show(
        0,
        message['notification']['title'],
        message['notification']['body'],
        platform);
  }

  @override
  void didChangeDependencies() {
    if (_initialRun) {
      _authProvider = Provider.of<AuthProvider>(context);
      if (_authProvider.currentUser != null) _firebaseCloudMessagingListeners();
      _initialRun = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return NetworkIndicator(child: PageContainer(child:
        Consumer<NavigationProvider>(builder: (context, navigationProvider, _) {
      return Scaffold(
          body: navigationProvider.selectedContent,
          bottomNavigationBar: BottomNavigationBar(
            backgroundColor: mainAppColor,
          //  opacity: .2,
            currentIndex: navigationProvider.navigationIndex,
            onTap: (index) {
              if (index != 0 && _authProvider.currentUser == null)
                Navigator.pushNamed(context, '/login_screen');
              else
                navigationProvider.upadateNavigationIndex(index, context);
            },
            elevation: 8,
            showUnselectedLabels: true,
             fixedColor: whiteColor,
            unselectedItemColor: small_grey_color,
            items:  <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Image.asset(
                  'assets/images/home.png',
                  color: small_grey_color,
                ),
                activeIcon: Image.asset('assets/images/home.png',
                    color: whiteColor),
                label: "الرئيسية",
                backgroundColor: mainAppColor,
              ),
              BottomNavigationBarItem(
                  backgroundColor: mainAppColor,
                  icon: Image.asset(
                    'assets/images/heart_black.png',
                    color: small_grey_color,
                  ),
                  activeIcon: Image.asset('assets/images/heart_black.png',
                      color: whiteColor),
                  label: "المفضلة"
              ),
              BottomNavigationBarItem(
                  backgroundColor: mainAppColor,
                  icon: Image.asset(
                    '',
                    color: small_grey_color,
                  ),
                  activeIcon: Image.asset('',
                      color: whiteColor),
                  label: ""
              ),
              BottomNavigationBarItem(
                  backgroundColor: mainAppColor,
                  icon: Image.asset('assets/images/message_black.png',
                      color: small_grey_color),
                  activeIcon: Image.asset(
                    'assets/images/message_black.png',
                    color: whiteColor,
                  ),
                  label: "الرسائل"
              ),
              BottomNavigationBarItem(
                  backgroundColor: mainAppColor,
                  icon: Image.asset('assets/images/menu_blck.png',
                      color: small_grey_color),
                  activeIcon: Image.asset(
                    'assets/images/menu_blck.png',
                    color: whiteColor,
                  ),
                  label: "المزيد"
              ),
            ],

            /* <BubbleBottomBarItem>[
              BubbleBottomBarItem(
                  backgroundColor: mainAppColor,
                  icon: Image.asset(
                    'assets/images/home.png',
                    color: Colors.white,
                  ),
                  activeIcon: Image.asset('assets/images/home.png',
                      color: secondary_color),
                  title: Text("الرئيسية",style: TextStyle(color: secondary_color),)),
              BubbleBottomBarItem(
                  backgroundColor: mainAppColor,
                  icon: Image.asset(
                    'assets/images/heart_black.png',
                    color: Colors.white,
                  ),
                  activeIcon: Image.asset('assets/images/heart_black.png',
                      color: secondary_color),
                  title: Text("المفضلة",style: TextStyle(color: secondary_color),)),
              BubbleBottomBarItem(
                  backgroundColor: mainAppColor,
                  icon: Image.asset(
                    'assets/images/add-fill.png',
                    color: Colors.white,
                  ),

                  activeIcon: Image.asset('assets/images/add-fill.png',
                      color: secondary_color),
                  title: Text("أضف اعلان",style: TextStyle(color: secondary_color),)),
              BubbleBottomBarItem(
                  backgroundColor: mainAppColor,
                  icon: Image.asset('assets/images/message_black.png',
                      color: Colors.white),
                  activeIcon: Image.asset(
                    'assets/images/message_black.png',
                    color: secondary_color,
                  ),
                  title: Text("الرسائل",style: TextStyle(color: secondary_color),)),
              BubbleBottomBarItem(
                  backgroundColor: mainAppColor,
                  icon: Image.asset('assets/images/menu_blck.png',
                      color: Colors.white),
                  activeIcon: Image.asset(
                    'assets/images/menu_blck.png',
                    color: secondary_color,
                  ),
                  title: Text("المزيد",style: TextStyle(color: secondary_color),))
            ],*/
          ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add),
          backgroundColor: secondary_color,
          onPressed: () {
           Navigator.push(context, MaterialPageRoute(builder: (context)=>AddingAdScreen()));
          },
        ),);
    })));
    // NetworkIndicator(child: PageContainer(child:
    //     Consumer<NavigationProvider>(builder: (context, navigationProvider, _) {
    //   return Scaffold(
    //     body: navigationProvider.selectedContent,
    //     bottomNavigationBar: CurvedNavigationBar(
    //         index: navigationProvider.navigationIndex,
    //         height: 45.0,
    //         color: mainAppColor,
    //         backgroundColor: Colors.black,
    //         buttonBackgroundColor: mainAppColor,
    //         animationDuration: Duration(milliseconds: 200),
    //         animationCurve: Curves.bounceInOut,
    //         items: <Widget>[
    //           Column(
    //             mainAxisAlignment: MainAxisAlignment.center,
    //             children: [
    //               Image.asset('assets/images/home.png',
    //                   height: 20,
    //                   color: navigationProvider.navigationIndex != 0
    //                       ? Colors.white
    //                       : blackColor),
    //               Visibility(
    //                 visible: navigationProvider.navigationIndex != 0,
    //                 child: Text(
    //                   'الرئيسية',
    //                   style: TextStyle(
    //                       fontSize: 10,
    //                       fontWeight: FontWeight.bold,
    //                       color: navigationProvider.navigationIndex != 0
    //                           ? Colors.white
    //                           : mainAppColor),
    //                 ),
    //               )
    //             ],
    //           ),
    //           Column(
    //             mainAxisAlignment: MainAxisAlignment.center,
    //             children: [
    //               Image.asset(
    //                 'assets/images/heart_black.png',
    //                 height: 20,
    //                 color: navigationProvider.navigationIndex != 1
    //                     ? Colors.white
    //                     : blackColor,
    //               ),
    //               Visibility(
    //                 visible: navigationProvider.navigationIndex != 1,
    //                 child: Text(
    //                   'المفضلة',
    //                   style: TextStyle(
    //                       fontSize: 11,
    //                       fontWeight: FontWeight.bold,
    //                       color: navigationProvider.navigationIndex != 1
    //                           ? Colors.white
    //                           : mainAppColor),
    //                 ),
    //               )
    //             ],
    //           ),
    //           Column(
    //             mainAxisAlignment: MainAxisAlignment.center,
    //             children: [
    //               Image.asset(
    //                 'assets/images/add-fill.png',
    //                 height: 20,
    //                 color: navigationProvider.navigationIndex != 2
    //                     ? Colors.white
    //                     : blackColor,
    //               ),
    //               Visibility(
    //                 visible: navigationProvider.navigationIndex != 2,
    //                 child: Text(
    //                   'أضف اعلان',
    //                   style: TextStyle(
    //                       fontSize: 11,
    //                       fontWeight: FontWeight.bold,
    //                       color: navigationProvider.navigationIndex != 2
    //                           ? Colors.white
    //                           : mainAppColor),
    //                 ),
    //               )
    //             ],
    //           ),
    //           Column(
    //             mainAxisAlignment: MainAxisAlignment.center,
    //             children: [
    //               Image.asset(
    //                 'assets/images/message_black.png',
    //                 height: 20,
    //                 color: navigationProvider.navigationIndex != 3
    //                     ? Colors.white
    //                     : blackColor,
    //               ),
    //               Visibility(
    //                 visible: navigationProvider.navigationIndex != 3,
    //                 child: Text(
    //                   'الرسائل',
    //                   style: TextStyle(
    //                       fontSize: 11,
    //                       fontWeight: FontWeight.bold,
    //                       color: navigationProvider.navigationIndex != 3
    //                           ? Colors.white
    //                           : mainAppColor),
    //                 ),
    //               )
    //             ],
    //           ),
    //           Column(
    //             mainAxisAlignment: MainAxisAlignment.center,
    //             children: [
    //               Image.asset(
    //                 'assets/images/menu_blck.png',
    //                 height: 20,
    //                 color: navigationProvider.navigationIndex != 4
    //                     ? Colors.white
    //                     : blackColor,
    //               ),
    //               Visibility(
    //                 visible: navigationProvider.navigationIndex != 4,
    //                 child: Text(
    //                   'المزيد',
    //                   style: TextStyle(
    //                       fontSize: 11,
    //                       fontWeight: FontWeight.bold,
    //                       color: navigationProvider.navigationIndex != 4
    //                           ? Colors.white
    //                           : mainAppColor),
    //                 ),
    //               )
    //             ],
    //           ),
    //         ],
    //         onTap: (index) {
    //        if (index != 0 && _authProvider.currentUser == null)
    //             Navigator.pushNamed(context, '/login_screen');
    //           else
    //             navigationProvider.upadateNavigationIndex(index);

    //         }),
    //   );
    // })));
  }
}
