import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/custom_widgets/shared/shimmer_ad.dart';
import 'package:haraj/models/message.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/providers/chat_provider.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

import '../../shared_preferences/shared_preferences_helper.dart';

class ChatDetailsScreen extends StatefulWidget {
  final String name;
  final String id;

  const ChatDetailsScreen({Key key, @required this.name, @required this.id})
      : super(key: key);

  @override
  _ChatDetailsScreenState createState() => _ChatDetailsScreenState();
}

class _ChatDetailsScreenState extends State<ChatDetailsScreen> {
  double _height, _width;
  AppBar _appBar;
  AuthProvider _authProvider;
  String _content;
  Future<List<Message>> _listOfMessages;
  bool initialRun = true;
  ApiProvider _apiProvider = ApiProvider();
  final TextEditingController controller = new TextEditingController();
  BubbleStyle styleSomebody = BubbleStyle(
    nip: BubbleNip.leftTop,
    color: whiteColor,
    elevation: 0,
    margin: BubbleEdges.only(top: 8.0, right: 50.0),
    alignment: Alignment.topLeft,
  );
  BubbleStyle styleMe = BubbleStyle(
    nip: BubbleNip.rightTop,
    color: mainAppColor,
    elevation: 0,
    margin: BubbleEdges.only(top: 8.0, left: 50.0),
    alignment: Alignment.topRight,
  );

  @override
  void initState() {
    _listOfMessages = Provider.of<ChatProvider>(context, listen: false)
        .getSingleChat(widget.id);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (initialRun) {
      _authProvider = Provider.of<AuthProvider>(context, listen: false);
      initialRun = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;
    _appBar = AppBar(
      backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        widget.name,
        style: Theme.of(context).textTheme.headline1,
      ),
      actions: [
        InkWell(
          onTap: () => Commons.showModelSheet(
              msg: 'هل تريد حذف المحادثة؟',
              imgUrl: 'assets/images/pin.png',
              context: context,
              height: _height,
              width: _width,
              ok: () => _deleteChat(int.parse(widget.id), context)),
          child: Image.asset(
            'assets/images/delete.png',
            color: whiteColor,
          ),
        )
      ],
      leading: InkWell(
        onTap: () => Navigator.pop(context),
        child: Image.asset('assets/images/arrow_back.png'),
      ),
    );
    return PageContainer(
      child: Scaffold(
        backgroundColor: greenColor,
        resizeToAvoidBottomInset: false,
        appBar: _appBar,
        body: _buildBodyStreamWidget(),
      ),
    );
  }

  List<Message> chatMessages = List();
  bool futureDone = false;

  Widget _buildBodyStreamWidget() {
    return Column(children: [
      Expanded(
          child: Container(
              height: MediaQuery.of(context).size.height - 150,
              child: FutureBuilder<List<Message>>(
                  future: _listOfMessages,
                  builder: (context, snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                        return ListView.separated(
                          itemCount: 20,
                          physics: BouncingScrollPhysics(),
                          itemBuilder: (context, index) {
                            return ShimmerAd();
                          },
                          separatorBuilder: (context, index) {
                            return Divider();
                          },
                        );
                      case ConnectionState.active:
                        return Text('');
                      case ConnectionState.waiting:
                        return ListView.separated(
                          itemCount: 20,
                          physics: BouncingScrollPhysics(),
                          itemBuilder: (context, index) {
                            return ShimmerAd();
                          },
                          separatorBuilder: (context, index) {
                            return Divider();
                          },
                        );
                      case ConnectionState.done:
                        if (snapshot.hasError) {
                          print(snapshot.error.toString());
                          return Center(
                            child: Text('حدث خطأ ما'),
                          );
                        } else {
                          if (chatMessages.isEmpty)
                            _listOfMessages.then((messages) {
                              chatMessages.clear();
                              chatMessages.addAll(messages);
                            });
                          print("future :${chatMessages.length}");
                          return StreamBuilder(
                              stream: Provider.of<ChatProvider>(context,
                                      listen: false)
                                  .messageStream,
                              builder: (context, snapshot) {
                                if (snapshot.hasData) {
                                  print(
                                      "chat messages: ${chatMessages.length}");
                                  if (chatMessages.isNotEmpty &&
                                      snapshot.data != chatMessages.last) {
                                    chatMessages.add(snapshot.data);
                                  } else if (chatMessages.isEmpty) {
                                    Message bufferedMsg = snapshot.data;
                                    chatMessages.add(bufferedMsg);
                                  }
                                }
                                print("chat messages: ${chatMessages.length}");
                                return chatMessages.length > 0
                                    ? ListView.builder(
                                        itemCount: chatMessages.length,
                                        itemBuilder: (context, index) {
                                          return Column(
                                            children: [
                                              Bubble(
                                                alignment: Alignment.center,
                                                color: Color.fromARGB(
                                                    255, 212, 234, 244),
                                                elevation: 1,
                                                margin:
                                                    BubbleEdges.only(top: 8.0),
                                                child: Text(
                                                    chatMessages[index]
                                                        .createdAt
                                                        .toString(),
                                                    style: TextStyle(
                                                        fontSize: 10)),
                                              ),
                                              widget.id ==
                                                      chatMessages[index]
                                                          .receiverId
                                                  ? Bubble(
                                                      style: styleSomebody,
                                                      child: Text(
                                                        chatMessages[index]
                                                            .content,
                                                        style: TextStyle(
                                                            color:
                                                                mainAppColor),
                                                      ),
                                                    )
                                                  : Bubble(
                                                      style: styleMe,
                                                      child: Text(
                                                        chatMessages[index]
                                                            .content,
                                                        style: TextStyle(
                                                            color: whiteColor),
                                                      ),
                                                    ),
                                            ],
                                          );
                                        },
                                      )
                                    : Center(
                                        child: Padding(
                                          padding: const EdgeInsets.all(15.0),
                                          child: Text(
                                            'لا توجد رسائل',
                                            style: TextStyle(
                                                color: hintColor, fontSize: 17),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      );
                              });
                        }
                    }
                    return ListView.separated(
                      itemCount: 20,
                      physics: BouncingScrollPhysics(),
                      itemBuilder: (context, index) {
                        return ShimmerAd();
                      },
                      separatorBuilder: (context, index) {
                        return Divider();
                      },
                    );
                  }))),
      Container(
        margin: EdgeInsets.symmetric(vertical: 15),
        padding: MediaQuery.of(context).viewInsets,
        child: CustomTextFormField(
          fillColor: whiteColor,
          hintTxt: 'اكتب رسالة..',
          enableBorder: true,
          controller: controller,
          onChangedFunc: (String val) {
            _content = val;
          },
          suffixIcon: InkWell(
            onTap: () {
              if (_content != null && _content.isNotEmpty) {
                final provider =
                    Provider.of<ChatProvider>(context, listen: false);
                provider.sendMessage(_authProvider.currentUser.id,
                    int.parse(widget.id), _content);
                controller.text = "";
              }
            },
            child: Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                    color: mainAppColor,
                    borderRadius: BorderRadius.circular(10)),
                child: Image.asset(
                  'assets/images/send.png',
                  color: blackColor,
                )),
          ),
        ),
      )
    ]);
  }

  void _deleteChat(int reciverId, BuildContext context) async {
    final response = await _apiProvider
        .delete(Urls.DELETE_CHAT_URL + reciverId.toString(), header: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${_authProvider.currentUser.accessToken}'
    });

    if (response['status_code'] == 200) {
      Provider.of<ChatProvider>(context, listen: false).messageSubject.close();
      Provider.of<ChatProvider>(context, listen: false).messageSubject =
          BehaviorSubject();
      Navigator.pushNamed(context, '/navigation');
    } else if (response["status_code"] == 401) {
      Commons.showError(
          context: context,
          message: 'يرجى تسجيل الدخول مجدداً',
          onTapOk: () {
            Navigator.pop(context);

            Navigator.of(context).pushNamedAndRemoveUntil(
                '/login_screen', (Route<dynamic> route) => false);
            SharedPreferencesHelper.remove("user");
          });
    } else {
      Commons.showMessageDialog(msg: response['response']['error']);
    }
  }
}
