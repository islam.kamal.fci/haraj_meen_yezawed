import 'package:flutter/material.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/custom_widgets/shared/simmer_chat.dart';
import 'package:haraj/models/chat_model.dart';
import 'package:haraj/providers/chat_provider.dart';
import 'package:haraj/ui/chat/chat_details_screen.dart';
import 'package:haraj/ui/chat/widgets/chat_item.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:provider/provider.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  // double _height, _width;
  AppBar _appBar;
  @override
  Widget build(BuildContext context) {
    // _height =
    //     MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    // _width = MediaQuery.of(context).size.width;
    _appBar = AppBar(
      backgroundColor: mainAppColor,
        centerTitle: true,
        title: Text(
          'الرسائل',
          style: Theme.of(context).textTheme.headline1,
        ));
    return NetworkIndicator(
        child: PageContainer(
            child: Scaffold(
              backgroundColor: greenColor,
      appBar: _appBar,
      body: _buildBodyWidget(),
    )));
  }

  Widget _buildBodyWidget() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15),
      child: FutureBuilder<List<ChatModel>>(
          future:
              Provider.of<ChatProvider>(context, listen: false).getAllChats(
                context
              ),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return ListView.separated(
                  itemCount: 20,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, index) {
                    return ShimmerChat();
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                );
              case ConnectionState.active:
                return Text('');
              case ConnectionState.waiting:
                return ListView.separated(
                  itemCount: 20,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, index) {
                    return ShimmerChat();
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                );
              case ConnectionState.done:
                if (snapshot.hasError) {
                  print(snapshot.error.toString());
                  return Center(
                    child: Text('حدث خطأ ما'),
                  );
                } else if (snapshot.data.length == 0) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Text(
                              'لا توجد رسائل لديك الان',
                              style: TextStyle(color: whiteColor, fontSize: 17),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                } else {
                  return ListView.separated(
                    physics: BouncingScrollPhysics(),
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ChatDetailsScreen(
                                        name: snapshot.data[index].name,
                                        id: snapshot.data[index].receiverId.toString(),
                                      ))),
                          child: ChatItem(chatModel: snapshot.data[index]));
                    },
                    separatorBuilder: (context, index) {
                      return Divider(
                        color: Colors.black,
                      );
                    },
                  );
                }
            }
            return ListView.separated(
              itemCount: 20,
              physics: BouncingScrollPhysics(),
              itemBuilder: (context, index) {
                return ShimmerChat();
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          }),
    );
  }
}
