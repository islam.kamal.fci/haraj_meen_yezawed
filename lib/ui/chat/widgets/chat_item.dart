import 'package:flutter/material.dart';
import 'package:haraj/models/chat_model.dart';
import 'package:haraj/utils/app_colors.dart';

class ChatItem extends StatefulWidget {
  final ChatModel chatModel;

  const ChatItem({Key key, this.chatModel}) : super(key: key);
  @override
  _ChatItemState createState() => _ChatItemState();
}

class _ChatItemState extends State<ChatItem> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constrains) {
      return Row(
        children: [
          Container(
            margin: EdgeInsets.all(10),
            child: widget.chatModel.image == null
                ? CircleAvatar(
                    radius: 30,
                    backgroundColor: hintColor,
                    child: Image.asset('assets/images/img.png'),
                  )
                : CircleAvatar(
                    radius: 30,
                    backgroundColor: whiteColor,
                    backgroundImage: NetworkImage(widget.chatModel.image),
                  ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(widget.chatModel.name,
                        style: TextStyle(
                          color: whiteColor,
                            fontSize: 14, fontWeight: FontWeight.bold)),
                    Container(
                      margin: EdgeInsets.only(left: 15),
                      child: Text(
                        widget.chatModel.createdAt,
                        style: TextStyle(fontSize: 14, color: whiteColor),
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(top: 15),
                  width: constrains.maxWidth * 0.7,
                  child: Text(
                    widget.chatModel.lastMsg,
                    style: TextStyle(fontSize: 12, color: whiteColor),
                  ),
                )
              ],
            ),
          )
        ],
      );
    });
  }
}
