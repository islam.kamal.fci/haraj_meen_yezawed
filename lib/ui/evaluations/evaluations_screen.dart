import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/validation_mixin.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/models/rate.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/rate_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/ui/evaluations/widgets/rate_widget.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';
import 'package:provider/provider.dart';

class EvaluationsScreen extends StatefulWidget {
  final String id;
  final String name;

  const EvaluationsScreen({Key key, this.id, this.name}) : super(key: key);
  @override
  _EvaluationsScreenState createState() => _EvaluationsScreenState();
}

class _EvaluationsScreenState extends State<EvaluationsScreen>
    with ValidationMixin {
      
  double _height, _width;
  AppBar _appBar;
  String _vote;
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false, intialRun = true;
  ApiProvider _apiProvider = ApiProvider();
  AuthProvider _authProvider;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (intialRun) {
      _authProvider = Provider.of<AuthProvider>(context);
      intialRun = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;
    final bottom = MediaQuery.of(context).viewInsets.bottom;
    _appBar = AppBar(
      backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        'التقييمات',
        style: Theme.of(context).textTheme.headline1,
      ),
      leading: InkWell(
        onTap: () => Navigator.pop(context),
        child: Image.asset('assets/images/arrow_back.png',color: whiteColor,),
      ),
    );

    return   NetworkIndicator(child: PageContainer(child:Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: greenColor,
      appBar: _appBar,
      body: SingleChildScrollView(

child: Container(
  height: _height,
  width: _width,
padding: EdgeInsets.only(bottom: bottom),
child:_buildBodyWidget())),
      floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () => _showBottomSheet(),
        backgroundColor: mainAppColor,
        child: Icon(Icons.add),
        elevation: 4,
      ),
    )));
  }

  Widget _buildBodyWidget() {
    return FutureBuilder<List<Rate>>(
        future: Provider.of<RateProvider>(context, listen: false)
            .getRates(widget.id,context),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.active:
              return Text('');
            case ConnectionState.waiting:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.done:
              if (snapshot.hasError) {
                debugPrint(snapshot.error.toString());
                return Center(
                  child: Text('حدث خطأ ما'),
                );
              } else if (snapshot.data.length == 0) {
                return Center(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Text(
                      'لا توجد تقييمات حتى الان',
                      style: TextStyle(color: whiteColor, fontSize: 17),
                      textAlign: TextAlign.center,
                    ),
                  ),
                );
              } else {
                return ListView.builder(
                    physics: BouncingScrollPhysics(),
                    padding: EdgeInsets.symmetric(vertical: 5),
                    itemBuilder: (context, index) {
                      return RateWidget(
                        rate: snapshot.data[index],
                      );
                    },
                    itemCount: snapshot.data.length);
              }
          }

          return Center(
            child: SpinKitDoubleBounce(
              color: mainAppColor,
            ),
          );
        });
  }

  _showBottomSheet() {
    return showModalBottomSheet(
      backgroundColor: greenColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: const Radius.circular(15.0),
                topRight: const Radius.circular(15.0))),
        context: context,
        isScrollControlled: true,
        builder: (context) {
          return SingleChildScrollView(
            child: Container(
                padding: MediaQuery.of(context).viewInsets,
                margin: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                child: Form(
                  key: _formKey,
                  child: Wrap(
                    children: [
                      Center(
                        child: Text(
                          'أضف تقييم',
                          style: TextStyle(
                            color: whiteColor,
                              fontWeight: FontWeight.bold, fontSize: 13),
                        ),
                      ),
                      Container(
                        height: 80,
                        margin: EdgeInsets.symmetric(vertical: _height * 0.03),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Consumer<RateProvider>(
                                builder: (context, termsState, child) {
                              return GestureDetector(
                                onTap: () =>
                                    termsState.setIsSwear(!termsState.isSwear),
                                child: Container(
                                  width: 20,
                                  height: 20,
                                  margin: EdgeInsets.symmetric(
                                    horizontal: _width * 0.02,
                                  ),
                                  child: Icon(
                                    Icons.check,
                                    color: whiteColor,
                                    size: 17,
                                  ),
                                  decoration: BoxDecoration(
                                    color: termsState.isSwear
                                        ? mainAppColor
                                        : whiteColor,
                                    border: Border.all(
                                      color: termsState.isSwear
                                         ? mainAppColor
                                        : whiteColor,
                                    ),
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                ),
                              );
                            }),
                            Container(
                              width: _width * 0.8,
                            
                              child: Text(
                                  'أتعهد وأقسم بالله أنني قمت بشراء سلعة من العضو ${widget.name} وأن المعلومات التي أقدمها هي معلومات صحيحة ودقيقة وأتحمل مسؤولية صحة هذه المعلومات',
                                 
                                  style: TextStyle(
                                    color: whiteColor,
                                      height: 1.5,
                                      fontSize: 13,
                                      fontWeight: FontWeight.bold)),
                            )
                          ],
                        ),
                      ),
                      Divider(),
                      Container(
                        width: _width,
                        margin: EdgeInsets.only(top: _height * 0.03, bottom: 12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'هل قمت بشراء سلعه من  ${widget.name} ؟',
                              style: TextStyle(
                                  color: whiteColor,
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: _width,
                        margin: EdgeInsets.only(bottom: _height * 0.03),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Consumer<RateProvider>(
                                    builder: (context, termsState, child) {
                                  return termsState.buyer
                                      ? GestureDetector(
                                          onTap: () => termsState.setBuyer(false),
                                          child: Container(
                                            width: 20,
                                            height: 20,
                                            margin: EdgeInsets.symmetric(
                                              horizontal: _width * 0.02,
                                            ),
                                            child: Icon(
                                              Icons.check,
                                              color: whiteColor,
                                              size: 17,
                                            ),
                                            decoration: BoxDecoration(
                                              color: mainAppColor,
                                              border: Border.all(
                                                color: mainAppColor,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                          ),
                                        )
                                      : GestureDetector(
                                          onTap: () => termsState.setBuyer(true),
                                          child: Container(
                                            width: 20,
                                            height: 20,
                                            margin: EdgeInsets.symmetric(
                                              horizontal: _width * 0.02,
                                            ),
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: hintColor,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                          ),
                                        );
                                }),
                                Container(
                                  child: Text('نعم',
                                      style: TextStyle(
                                          color: whiteColor,
                                          fontSize: 13,
                                          fontWeight: FontWeight.bold)),
                                )
                              ],
                            ),
                            SizedBox(
                              width: 50,
                            ),
                            Row(
                              children: [
                                Consumer<RateProvider>(
                                    builder: (context, termsState, child) {
                                  return !termsState.buyer
                                      ? GestureDetector(
                                          onTap: () => termsState.setBuyer(true),
                                          child: Container(
                                            width: 20,
                                            height: 20,
                                            margin: EdgeInsets.symmetric(
                                              horizontal: _width * 0.02,
                                            ),
                                            child: Icon(
                                              Icons.check,
                                              color: whiteColor,
                                              size: 17,
                                            ),
                                            decoration: BoxDecoration(
                                              color: mainAppColor,
                                              border: Border.all(
                                                color: mainAppColor,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                          ),
                                        )
                                      : GestureDetector(
                                          onTap: () => termsState.setBuyer(false),
                                          child: Container(
                                            width: 20,
                                            height: 20,
                                            margin: EdgeInsets.symmetric(
                                              horizontal: _width * 0.02,
                                            ),
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: hintColor,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                          ),
                                        );
                                }),
                                Container(
                                  child: Text('لا',
                                      style: TextStyle(
                                          color: whiteColor,
                                          fontSize: 13,
                                          fontWeight: FontWeight.bold)),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      Divider(),
                      Container(
                        width: _width,
                        margin: EdgeInsets.only(top: _height * 0.03, bottom: 12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'هل تنصح بالتعامل مع  ${widget.name} ؟',
                              style: TextStyle(
                                  color: whiteColor,
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: _width,
                        margin: EdgeInsets.only(bottom: _height * 0.03),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Consumer<RateProvider>(
                                    builder: (context, termsState, child) {
                                  return termsState.recommended
                                      ? GestureDetector(
                                          onTap: () =>
                                              termsState.setRecommended(false),
                                          child: Container(
                                            width: 20,
                                            height: 20,
                                            margin: EdgeInsets.symmetric(
                                              horizontal: _width * 0.02,
                                            ),
                                            child: Icon(
                                              Icons.check,
                                              color: whiteColor,
                                              size: 17,
                                            ),
                                            decoration: BoxDecoration(
                                              color: mainAppColor,
                                              border: Border.all(
                                                color: mainAppColor,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                          ),
                                        )
                                      : GestureDetector(
                                          onTap: () =>
                                              termsState.setRecommended(true),
                                          child: Container(
                                            width: 20,
                                            height: 20,
                                            margin: EdgeInsets.symmetric(
                                              horizontal: _width * 0.02,
                                            ),
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: hintColor,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                          ),
                                        );
                                }),
                                Container(
                                  child: Text('نعم',
                                      style: TextStyle(
                                          color: whiteColor,
                                          fontSize: 13,
                                          fontWeight: FontWeight.bold)),
                                )
                              ],
                            ),
                            SizedBox(
                              width: 50,
                            ),
                            Row(
                              children: [
                                Consumer<RateProvider>(
                                    builder: (context, termsState, child) {
                                  return !termsState.recommended
                                      ? GestureDetector(
                                          onTap: () =>
                                              termsState.setRecommended(true),
                                          child: Container(
                                            width: 20,
                                            height: 20,
                                            margin: EdgeInsets.symmetric(
                                              horizontal: _width * 0.02,
                                            ),
                                            child: Icon(
                                              Icons.check,
                                              color: whiteColor,
                                              size: 17,
                                            ),
                                            decoration: BoxDecoration(
                                              color: mainAppColor,
                                              border: Border.all(
                                                color: mainAppColor,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                          ),
                                        )
                                      : GestureDetector(
                                          onTap: () =>
                                              termsState.setRecommended(false),
                                          child: Container(
                                            width: 20,
                                            height: 20,
                                            margin: EdgeInsets.symmetric(
                                              horizontal: _width * 0.02,
                                            ),
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: hintColor,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                          ),
                                        );
                                }),
                                Container(
                                  child: Text('لا',
                                      style: TextStyle(
                                          color: whiteColor,
                                          fontSize: 13,
                                          fontWeight: FontWeight.bold)),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      CustomTextFormField(
                        enableBorder: true,
                        labelText: 'أكتب تجربتك للزوار',
                        labelStyle: TextStyle(color: mainAppColor),
                        inputData: TextInputType.text,
                        fillColor: whiteColor,
                        maxLines: 5,
                        validationFunc: validateVote,
                        onChangedFunc: (String val) {
                          _vote = val;
                        },
                      ),
                      _isLoading
                          ? Container(
                              margin: EdgeInsets.only(
                                  bottom: _height * 0.02, top: _height * 0.05),
                              child: SpinKitDoubleBounce(color: mainAppColor),
                            )
                          : Consumer<RateProvider>(
                              builder: (context, provider, _) {
                              return Container(
                                height: _height * 0.1,
                                margin: EdgeInsets.symmetric(vertical: 20),
                                child: CustomButton(
                                  btnLbl: 'ارسال',
                                  btnStyle: TextStyle(color: whiteColor),
                                  onPressedFunction: () => _addRate(
                                      recommend: provider.recommended ? 1 : 0,
                                      isSwear: provider.isSwear,
                                      buyer: provider.buyer ? 1 : 0),
                                ),
                              );
                            })
                    ],
                  ),
                )),
          );
        });
  }

  _addRate({int recommend, int buyer, bool isSwear}) async {
    if (isSwear) {
      if (_formKey.currentState.validate()) {
        setState(() {
          _isLoading = true;
        });
        var result = await _apiProvider.post(Urls.CREATE_RATE_URL, body: {
          "seller_id": int.parse(widget.id),
          "vote": recommend,
          "buying_product": buyer,
          'rating_text': _vote
        }, header: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          'Authorization': 'Bearer ${_authProvider.currentUser.accessToken}'
        });
        setState(() {
          _isLoading = false;
        });
        if (result["status_code"] == 200) {
          Navigator.pop(context);
          Provider.of<RateProvider>(context, listen: false).reset();
          Commons.showToast(
              message: 'تم الارسال', context: context, color: mainAppColor);
        } else {
          Commons.showToast(
              message: result['response']['error'],
              context: context,
              color: hintColor);
        }
      }
    } else {
      Commons.showToast(
          message: 'الرجاء القسم اولا لضمان التقييم',
          context: context,
          color: mainAppColor);
    }
  }
}
