import 'package:flutter/material.dart';
import 'package:haraj/models/rate.dart';
import 'package:haraj/utils/app_colors.dart';

class RateWidget extends StatelessWidget {
  final Rate rate;

  const RateWidget({Key key, this.rate}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: BoxDecoration(
        border: Border.all(width: 1.0, color: hintColor),
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  rate.voter ?? '',
                  style: TextStyle(
                      color: whiteColor,
                      fontSize: 12,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  rate.createdAt,
                  style: TextStyle(
                    color: whiteColor,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 15),
            child: Text(
              rate.ratingText,
              style: TextStyle(color: whiteColor, fontSize: 12),
            ),
          ),
          Container(
            margin: EdgeInsets.all(8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  width: 100,
                ),
                Row(
                  children: [
                    Text(
                      rate.buyingProduct == 1 ? ' مشتري ' : '',
                      style: TextStyle(fontSize: 11, color: whiteColor),
                    ),
               /*     Image.asset(
                      'assets/images/thumb-up-line.png',
                      color: mainAppColor,
                    )*/
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
