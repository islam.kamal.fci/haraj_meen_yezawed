import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/shared/ad.dart';
import 'package:haraj/custom_widgets/shared/shimmer_ad.dart';
import 'package:haraj/models/product.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/providers/favourite_provider.dart';
import 'package:haraj/shared_preferences/shared_preferences_helper.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';

class FavoritesScreen extends StatefulWidget {
  @override
  _FavoritesScreenState createState() => _FavoritesScreenState();
}

class _FavoritesScreenState extends State<FavoritesScreen> {
  // double _height, _width;
  ApiProvider _apiProvider =ApiProvider();
  AuthProvider _authProvider;
  bool _isLoading = false;
   bool _initialRun = true;

   Future<List<Product>> _productList;
  FavouriteProvider _favouriteProvider;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    if (_initialRun) {
      _favouriteProvider = Provider.of<FavouriteProvider>(context);
      _productList = _favouriteProvider.getFavouriteList(context);
      _initialRun = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    _authProvider = Provider.of<AuthProvider>(context);
    // _height =
    //     MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    // _width = MediaQuery.of(context).size.width;
   AppBar appBar = AppBar(
     backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        'المفضلة',
        style: Theme.of(context).textTheme.headline1,
      ),
    );

    return Scaffold(
      backgroundColor: greenColor,
      appBar: appBar,
      body: Stack(
        children: [
          _buildBodyWidget(),
              _isLoading
                ? Center(
                    child: SpinKitFadingCircle(color: mainAppColor),
                  )
                : Container(),

        ],
      )
    );
  }

  Widget _buildBodyWidget() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15),
      child: FutureBuilder<List<Product>>(
          future: _productList,
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return ListView.separated(
                  itemCount: 20,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, index) {
                    return ShimmerAd();
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                );
              case ConnectionState.active:
                return Text('');
              case ConnectionState.waiting:
                return ListView.separated(
                  itemCount: 20,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, index) {
                    return ShimmerAd();
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                );
              case ConnectionState.done:
                if (snapshot.hasError) {
                  print(snapshot.error.toString());
                  return Center(
                    child: Text('حدث خطأ ما'),
                  );
                } else if (snapshot.data.length == 0) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: Shimmer.fromColors(
                            baseColor: hintColor,
                            highlightColor: Colors.blue[50],
                            child: Image.asset(
                              'assets/images/heart_black.png',
                              fit: BoxFit.cover,
                              color: hintColor,
                              height: 85,
                              width: 85,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Text(
                            'لا توجد منتجات مفضله لديك ,ابدا بفحص المنتجات وسجل المفضل لديك الان',
                            style: TextStyle(color: whiteColor, fontSize: 17),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  );
                } else {
                 
                      return ListView.separated(
                        itemCount: snapshot.data.length,
                        physics: BouncingScrollPhysics(),
                        itemBuilder: (context, index) {
                          return Ad(
                                
                            onPressDelete: () => _deleteFromFav(snapshot.data[index].id),
                            isFavourite: true,
                            product: snapshot.data[index],
                          );
                        },
                        separatorBuilder: (context, index) {
                          return Divider();
                        },
                      );
                   
                }
            }
            return ListView.separated(
              itemCount: 20,
              physics: BouncingScrollPhysics(),
              itemBuilder: (context, index) {
                return ShimmerAd();
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          }),
    );
  }


 _deleteFromFav(int id) async {
  
  
      setState(() {
        _isLoading = true;
      });
      var result = await _apiProvider.put(Urls.TOGGLE_FAV_URL+id.toString(),
     header: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${_authProvider.currentUser.accessToken}'
    });
      setState(() {
        _isLoading = false;
      });
      if (result["status_code"] == 200) {
      
      Commons.showToast(message:result['response']['data'],
       context: context);
       _productList = _favouriteProvider.getFavouriteList(context);
       _favouriteProvider.removeFavItem(id);
    
      } 
      else if (result["status_code"] == 401) 
      Commons.showError(
          context: context,
          message: 'يرجى تسجيل الدخول مجدداً',
          onTapOk: () {
            Navigator.pop(context);

            Navigator.of(context).pushNamedAndRemoveUntil(
                '/login_screen', (Route<dynamic> route) => false);
            SharedPreferencesHelper.remove("user");
          });
    else 
        Commons.showToast(
            message: result['response']['error'],
            context: context,
            color: hintColor);
      
      
    }
    
  }


