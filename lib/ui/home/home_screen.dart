import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
import 'package:haraj/custom_widgets/no_data/no_data.dart';
import 'package:haraj/custom_widgets/shared/ad.dart';
import 'package:haraj/custom_widgets/shared/shimmer_ad.dart';
import 'package:haraj/custom_widgets/shared/shimmer_chip.dart';
import 'package:haraj/models/product.dart';
import 'package:haraj/models/city.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/providers/cities_provider.dart';
import 'package:haraj/providers/home_provider.dart';
import 'package:haraj/ui/home/widgets/category_item.dart';
import 'package:haraj/ui/home/widgets/grid_ad.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:location/location.dart';
import 'package:haraj/models/category.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:provider/provider.dart';
import 'package:haraj/custom_widgets/drop_down_list_selector/drop_down_list_selector.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class HomeScreen extends StatefulWidget {
  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  double _height, _width;
  HomeProvider _homeProvider;
  bool initialRun = true;
  String _keySearch = '';
  AuthProvider _authProvider;
  Future<List<Product>> _searchResultProductList;
  Future<List<Product>> _productList;
  Future<List<Category>> _categoryList;
  Future<List<City>> _cityList;
  CitiesProvider _citiesProvider;
  bool _isScrollingDown = false;
  ScrollController _scrollListViewController = new ScrollController();
  TextEditingController _seacrhTextEditingController = TextEditingController();

  List<Product> photos;
  List<int> maxPhotos = [];
  @override
  void initState() {
    super.initState();
    _handleScrolling();
    maxPhotos.addAll(List.generate(50000, (x) => x));
    photos = [];
    Commons.homeScreenState = this;
  }

  Widget buildListView(
    BuildContext context,
    AsyncSnapshot<List<Product>> snapshot,
  ) {
    if (!snapshot.hasData) {
      return Center(
          child: CircularProgressIndicator(
        valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
      ));
    }

    print(snapshot.data);

    photos.addAll(snapshot.data);

    return ListView.builder(
        itemCount: (maxPhotos.length > photos.length)
            ? photos.length + 1
            : photos.length,
        itemBuilder: (context, index) {
          print("index $index");
          print("index photos.length ${photos.length}");
          if (index == photos.length) {
            return Center(
              child: CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(Colors.pink),
              ),
            );
          } else
            return _homeProvider.grid
                ? Container(
                    height: 250,
                    child: GridAd(
                      product: photos[index],
                    ),
                  )
                : Ad(
                    product: photos[index],
                  );
        });
  }

  void _handleScrolling() async {
    _scrollListViewController.addListener(() {
      if (_scrollListViewController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        if (!_isScrollingDown) {
          _isScrollingDown = true;

          // _homeProvider.setHideSelection(true);
        }
      }
      if (_scrollListViewController.position.userScrollDirection ==
          ScrollDirection.forward) {
        if (_isScrollingDown) {
          _isScrollingDown = false;

          // _homeProvider.setHideSelection(false);
        }
      }
    });
  }

  @override
  void dispose() {
    _scrollListViewController.removeListener(() {});
    super.dispose();
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    if (initialRun) {
      _homeProvider = Provider.of<HomeProvider>(context);
      // _homeProvider.addStream(context);
      _citiesProvider = Provider.of<CitiesProvider>(context);
      _homeProvider.updateEnableSearch(
        false,
      );
      _homeProvider.updateUserLatitude(null);
      _homeProvider.updateUserLongitude(null);
      _homeProvider.detectLocation(false);
      _homeProvider.setMainCategoryIndex(0);
      _homeProvider.setHideSelection(false, isNotifyListener: false);
      _homeProvider.clearProductList();
      _homeProvider.clearSearchResultList();
      _homeProvider.setCurrentPageSearch(1, notifyListener: false);
      _homeProvider.setCurrentPageHome(1, notifyListener: false);
      _categoryList = _homeProvider.getCategoriesList(context);
      _productList = _homeProvider.getProductList(
        context,
      );
      _homeProvider.setSelectedCarModel(null, notifyListener: false);
      _homeProvider.setSelectedCity(null);
      _homeProvider.setKeySearch('');
      _cityList = _citiesProvider.getCityList(context);
      initialRun = false;
    }
  }

  void _chooseCarModel() {
    FocusScope.of(context).requestFocus(FocusNode());
    showDialog<int>(
        context: context,
        builder: (BuildContext context) {
          return NumberPickerDialog.integer(
            minValue: DateTime.now().year - 40,
            maxValue: DateTime.now().year,
            title: new Text(
              'قم باختيار موديل السيارة',
              style: TextStyle(fontSize: 16),
            ),
            selectedTextStyle: TextStyle(color: mainAppColor, fontSize: 24),
            initialIntegerValue: _homeProvider.selectedCarModel != null
                ? _homeProvider.selectedCarModel
                : DateTime.now().year,
            confirmWidget: Text(
              'موافق',
              style: TextStyle(color: mainAppColor, fontSize: 14),
            ),
            cancelWidget: Text(
              'إلغاء',
              style: TextStyle(color: mainAppColor, fontSize: 14),
            ),
          );
        }).then((int value) {
      if (value != null) {
        _homeProvider.setSelectedCarModel(value);
        if (!_homeProvider.enableSearch)
          _homeProvider.updateEnableSearch(true, isNotifyListener: true);
        _homeProvider.clearProductList();
        _homeProvider.clearSearchResultList();
        _searchResultProductList = _homeProvider.getSearchResultList(context);
      }
    });
  }

  bool onNotification(ScrollNotification scrollInfo, HomeProvider bloc) {
    // print(scrollInfo);
    if (scrollInfo is OverscrollNotification) {
      bloc.sink.add(scrollInfo);
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;
    _authProvider = Provider.of<AuthProvider>(context);
    return Scaffold(
        backgroundColor: greenColor,
        body: RefreshIndicator(
            onRefresh: () async {

            },
            child: _buildBodyWidget()));
  }

  Future<void> _getCurrentUserLocation() async {
    LocationData locData = await Location().getLocation();

    if (locData != null) {
      Commons.showToast(message: 'تم تحديد موقعك', context: context);
      _homeProvider.updateUserLatitude(locData.latitude,
          isNotifyListener: true);
      _homeProvider.updateUserLongitude(locData.longitude,
          isNotifyListener: true);
      if (!_homeProvider.enableSearch)
        _homeProvider.updateEnableSearch(true, isNotifyListener: true);
      _homeProvider.clearProductList();
      _homeProvider.clearSearchResultList();
      _searchResultProductList = _homeProvider.getSearchResultList(context);
    }
  }

  Widget _buildBodyWidget() {
    final bloc = Provider.of<HomeProvider>(context);
    return Column(
      children: [
        Container(
          height: 130,
          child: Stack(
            children: [
              Container(
                  width: _width,
                  height: 100,
                  decoration: BoxDecoration(
                    color: greenColor,
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(),
                          Image.asset(
                            'assets/images/tlogo.png',
                            height: 55,
                          ),
                          Container(
                            margin: EdgeInsets.only(left: _width * 0.05),
                            child: InkWell(
                                onTap: () => (_authProvider.currentUser == null)
                                    ? Navigator.pushNamed(
                                        context, '/login_screen')
                                    : Navigator.pushNamed(
                                        context, '/notifications_screen'),
                                child: Image.asset(
                                  'assets/images/notification.png',
                                  color: secondary_color,
                                )),
                          )
                        ],
                      ),
                    ],
                  )),
              Positioned(
                top: 80,
                child: Container(
                  // alignment: Alignment.center,
                  // margin:
                  //     EdgeInsets.symmetric(horizontal: _width * 0.02),

                  width: _width,
                  height: 50,
                  child: CustomTextFormField(
                      fillColor: greenColor,
                      controller: _seacrhTextEditingController,
                      enableBorder: true,
                      onChangedFunc: (String text) {
                        if (text.trim().length > 0) {
                          _keySearch = text.trim();
                        }
                      },

                      hasHorizontalMargin: true,
                      hintTxt: 'عن ماذا تبحث؟',

                      hintStyle: TextStyle(
                          color: whiteColor,
                          fontSize: 12,
                          fontWeight: FontWeight.w500),
                   /*   prefixIcon: IconButton(
                        icon: Icon(
                          Icons.close,
                          color: mainAppColor,
                        ),
                        onPressed: () {
                          if (_keySearch.trim().length > 0) {
                            FocusScope.of(context).requestFocus(FocusNode());
                            _seacrhTextEditingController.clear();
                            _homeProvider.setKeySearch('',
                                isNotifyListener: true);
                            _keySearch = '';
                            if (!_homeProvider.enableSearch)
                              _homeProvider.updateEnableSearch(true,
                                  isNotifyListener: true);
                            _homeProvider.clearProductList();
                            _homeProvider.clearSearchResultList();
                            _searchResultProductList =
                                _homeProvider.getSearchResultList(context);
                          }
                        },
                      ),*/
                      suffixIcon: IconButton(
                        icon: Icon(
                          Icons.search,
                          color: whiteColor,
                        ),
                        onPressed: () {
                          if (_keySearch.length > 0) {
                            FocusScope.of(context).requestFocus(FocusNode());

                            _homeProvider.setKeySearch(_keySearch,
                                isNotifyListener: true);
                            if (!_homeProvider.enableSearch)
                              _homeProvider.updateEnableSearch(true,
                                  isNotifyListener: true);
                            _homeProvider.clearProductList();
                            _homeProvider.clearSearchResultList();
                            _searchResultProductList =
                                _homeProvider.getSearchResultList(context);
                          } else {
                            FocusScope.of(context).unfocus();
                            Commons.showToast(
                                message: 'يرجى إدخال كلمة للبحث',
                                context: context);
                          }
                        },
                      )),
                ),
              ),
            ],
          ),
        ),
        Consumer<HomeProvider>(builder: (context, homeProvider, child) {
          return !homeProvider.hideSelection
              ? Container(
                  height: 60,
                  margin: EdgeInsets.symmetric(vertical: 5),
                  child: FutureBuilder<List<Category>>(
                      future: _categoryList,
                      builder: (context, snapshot) {
                        switch (snapshot.connectionState) {
                          case ConnectionState.none:
                            return ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: 20,
                              itemBuilder: (builder, index) {
                                return ShimmerChip();
                              },
                            );
                          case ConnectionState.active:
                            return Text('');
                          case ConnectionState.waiting:
                            return ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: 20,
                              itemBuilder: (builder, index) {
                                return ShimmerChip();
                              },
                            );
                          case ConnectionState.done:
                            if (snapshot.hasError) {
                              print(snapshot.error.toString());
                              return Center(
                                child: Text('حدث خطأ ما'),
                              );
                            } else if (snapshot.data.length == 0)
                              return Center(
                                child: Text(
                                  'لا توجد اقسام',
                                  style:
                                      TextStyle(color: hintColor, fontSize: 16),
                                  textAlign: TextAlign.center,
                                ),
                              );
                            else
                              return ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: snapshot.data.length,
                                itemBuilder: (builder, index) {
                                  print("image : ${snapshot.data[index].categoryImage}");
                                  return Consumer<HomeProvider>(
                                    builder: (context, homeProvider, _) {
                                      return InkWell(
                                          onTap: () {
                                            homeProvider
                                                .updateChangesOnMainCategoriesList(
                                                    index);
                                            if (homeProvider.enableSearch)
                                              _searchResultProductList =
                                                  _homeProvider
                                                      .getSearchResultList(
                                                context,
                                              );
                                            else
                                              _productList = _homeProvider
                                                  .getProductList(context);
                                          },
                                          child: CategoryItem(
                                            isSelected:
                                                snapshot.data[index].isSelected,
                                            text: snapshot.data[index].name,
                                            cat_image: snapshot.data[index].categoryImage,
                                          )

                                          );
                                    },
                                  );
                                },
                              );
                        }
                        return ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: 20,
                          itemBuilder: (builder, index) {
                            return ShimmerChip();
                          },
                        );
                      }),
                )
              : Container();
        }),
        Consumer<HomeProvider>(builder: (context, homeProvider, child) {
          return !homeProvider.hideSelection
              ? homeProvider.mainCategoryIndex != 0 &&
                      homeProvider.categoryList[homeProvider.mainCategoryIndex]
                              .subCategories.length >
                          0
                  ? Container(
                      height: 60,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: homeProvider
                            .categoryList[homeProvider.mainCategoryIndex]
                            .subCategories
                            .length,
                        itemBuilder: (builder, index) {
                          return InkWell(
                              onTap: () {
                                homeProvider
                                    .updateChangesOnSubCategoriesList(index);
                                if (homeProvider.enableSearch)
                                  _searchResultProductList =
                                      _homeProvider.getSearchResultList(
                                    context,
                                  );
                              },
                              child: CategoryItem(
                                isSelected: homeProvider
                                    .categoryList[
                                        homeProvider.mainCategoryIndex]
                                    .subCategories[index]
                                    .isSelected,
                                text: homeProvider
                                    .categoryList[
                                        homeProvider.mainCategoryIndex]
                                    .subCategories[index]
                                    .name,
                              )
                              // ChipDesign(
                              //   color: homeProvider
                              //           .categoryList[
                              //               homeProvider.mainCategoryIndex]
                              //           .subCategories[index]
                              //           .isSelected
                              //        ? mainAppColor
                              //               :  Color(0xff1E1C1C),
                              //   label: homeProvider
                              //       .categoryList[
                              //           homeProvider.mainCategoryIndex]
                              //       .subCategories[index]
                              //       .name,
                              //   txtColor: homeProvider
                              //           .categoryList[
                              //               homeProvider.mainCategoryIndex]
                              //           .subCategories[index]
                              //           .isSelected
                              //       ?  Color(0xff1E1C1C)
                              //                   : mainAppColor,
                              // ),
                              );
                        },
                      ))
                  : Container()
              : Container();
        }),
        Consumer<HomeProvider>(builder: (context, homeProvider, child) {
          return !homeProvider.hideSelection
              ? homeProvider.mainCategoryIndex != 0 &&
                      homeProvider.categoryList[homeProvider.mainCategoryIndex]
                              .subCategories.length >
                          0 &&
                      homeProvider.subCategoryIndex != null
                  ? homeProvider
                              .categoryList[homeProvider.mainCategoryIndex]
                              .subCategories[homeProvider.subCategoryIndex]
                              .childCategory
                              .length >
                          0
                      ? Container(
                          margin: EdgeInsets.symmetric(vertical: 5),
                          height: 60,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: homeProvider
                                .categoryList[homeProvider.mainCategoryIndex]
                                .subCategories[homeProvider.subCategoryIndex]
                                .childCategory
                                .length,
                            itemBuilder: (builder, index) {
                              return InkWell(
                                  onTap: () {
                                    homeProvider
                                        .updateChangesOnChildCategoriesList(
                                            index);
                                    if (homeProvider.enableSearch)
                                      _searchResultProductList =
                                          _homeProvider.getSearchResultList(
                                        context,
                                      );
                                  },
                                  child: CategoryItem(
                                    isSelected: homeProvider
                                        .categoryList[
                                            homeProvider.mainCategoryIndex]
                                        .subCategories[
                                            homeProvider.subCategoryIndex]
                                        .childCategory[index]
                                        .isSelected,
                                    text: homeProvider
                                        .categoryList[
                                            homeProvider.mainCategoryIndex]
                                        .subCategories[
                                            homeProvider.subCategoryIndex]
                                        .childCategory[index]
                                        .name,
                                  )
                                  //  ChipDesign(
                                  //   color: homeProvider
                                  //           .categoryList[homeProvider
                                  //               .mainCategoryIndex]
                                  //           .subCategories[
                                  //               homeProvider.subCategoryIndex]
                                  //           .childCategory[index]
                                  //           .isSelected    ? mainAppColor
                                  //           :  Color(0xff1E1C1C),

                                  //   label: homeProvider
                                  //       .categoryList[
                                  //           homeProvider.mainCategoryIndex]
                                  //       .subCategories[
                                  //           homeProvider.subCategoryIndex]
                                  //       .childCategory[index]
                                  //       .name,
                                  //   txtColor: homeProvider
                                  //           .categoryList[homeProvider
                                  //               .mainCategoryIndex]
                                  //           .subCategories[
                                  //               homeProvider.subCategoryIndex]
                                  //           .childCategory[index]
                                  //           .isSelected
                                  //       ?   Color(0xff1E1C1C): mainAppColor,
                                  // ),
                                  );
                            },
                          ))
                      : Container()
                  : Container()
              : Container();
        }),
        Consumer<HomeProvider>(builder: (context, homeProvider, child) {
          return Container(
              height: 60,
              margin: EdgeInsets.symmetric(
                  horizontal: 6, vertical: _height * 0.015),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: _width * 0.70,

                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            width: _width * 0.3,
                            height: 60,

                            child: FutureBuilder<List<City>>(
                              future: _cityList,
                              builder: (context, snapshot) {
                                if (snapshot.hasData) {
                                  var cityList = snapshot.data.map((item) {
                                    return new DropdownMenuItem<City>(
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            'assets/images/map-pin-2-line.png',
                                            color: whiteColor,
                                          ),
                                          SizedBox(width: 2,),
                                          new Text(item.name,
                                            style: TextStyle(color: mainAppColor,fontSize: 14),),
                                        ],
                                      ),
                                      value: item,

                                    );
                                  }).toList();

                                  return DropDownListSelector(
                                    elementHasDefaultMargin: false,
                                    dropDownList: cityList,
                                    hint: 'المدينة',
                                    onChangeFunc: (newValue) {
                                      _homeProvider.setSelectedCity(newValue,
                                          isNotifyListener: true);

                                      if (!_homeProvider.enableSearch)
                                        _homeProvider.updateEnableSearch(true,
                                            isNotifyListener: true);
                                      _homeProvider.clearProductList();
                                      _homeProvider.clearSearchResultList();
                                      _searchResultProductList = _homeProvider
                                          .getSearchResultList(context);
                                    },
                                    value: _homeProvider.selectedCity,
                                  );
                                } else if (snapshot.hasError) {
                                  return Text("${snapshot.error}");
                                }

                                return Center(
                                    child: CircularProgressIndicator(
                                  valueColor: new AlwaysStoppedAnimation<Color>(
                                      Colors.cyan),
                                ));
                              },
                            )),
                       Padding(padding: EdgeInsets.symmetric(horizontal: 3),
                       child:  InkWell(
                         onTap: () {
                           if (!homeProvider.enableLocation) {
                             homeProvider.detectLocation(true,
                                 isNotifyListener: true);
                             _getCurrentUserLocation();
                           }
                           else {
                             _homeProvider.updateUserLatitude(null,
                                 isNotifyListener: true);
                             _homeProvider.updateUserLongitude(null,
                                 isNotifyListener: true);
                             homeProvider.detectLocation(false,
                                 isNotifyListener: true);
                             _homeProvider.clearProductList();
                             _homeProvider.clearSearchResultList();
                             _searchResultProductList =
                                 _homeProvider.getSearchResultList(context);
                           }
                         },
                         child: Container(
                           width:60,
                           height: 60,

                           child: Image.asset(
                             'assets/images/map-pin-2-line.png',
                             color: homeProvider.enableLocation
                                 ? whiteColor
                                 : mainAppColor,
                           ),
                           decoration: BoxDecoration(
                               border: Border.all(color: mainAppColor),
                               color: homeProvider.enableLocation
                                   ? mainAppColor
                                   : greenColor,
                               borderRadius: BorderRadius.circular(
                                 10,
                               )),
                         ),
                       ),
                       ),
                        Padding(padding: EdgeInsets.symmetric(horizontal: 3),
                            child:  InkWell(
                          onTap: () {
                            homeProvider.setGrid(!homeProvider.grid);
                            print('grid id : ${homeProvider.grid}');
                          },
                          child: Container(
                            width: 60,
                            child: Image.asset(
                              'assets/images/function-line.png',
                              color:
                                  homeProvider.grid ? hintColor : mainAppColor,
                            ),
                            decoration: BoxDecoration(
                                border: Border.all(color: mainAppColor),
                                color: homeProvider.grid
                                    ? mainAppColor
                                    : greenColor,
                                borderRadius: BorderRadius.circular(10)),
                          ),
                        ))
                      ],
                    ),
                  ),
                  _homeProvider.mainCategoryIndex != 0 ? _homeProvider.categoryList[homeProvider.mainCategoryIndex].id == 1 ||
                              _homeProvider.categoryList[homeProvider.mainCategoryIndex].name == 'حراج السيارات'
                          ? InkWell(
                              onTap: () => _chooseCarModel(),
                              child: Container(
                                width: 60,
                                child: Center(
                                    child: Text(
                                  _homeProvider.selectedCarModel != null
                                      ? _homeProvider.selectedCarModel
                                          .toString()
                                      : 'الموديل',
                                  style: TextStyle(
                                      color: whiteColor,
                                      fontSize: 10,
                                      fontWeight: FontWeight.bold),
                                )),
                                decoration: BoxDecoration(
                                    border: Border.all(color: mainAppColor),
                                    color: greenColor,
                                    borderRadius: BorderRadius.circular(10)),
                              ),
                            )
                          : Container()
                      : Container(),

                  InkWell(
                    onTap: () {
                      if (!_homeProvider.enableSearch)
                        _homeProvider.updateEnableSearch(true,
                            isNotifyListener: true);
                      homeProvider.enableTheLatest(!homeProvider.isLatest);
                      _homeProvider.clearProductList();
                      _homeProvider.clearSearchResultList();
                      _searchResultProductList =
                          _homeProvider.getSearchResultList(context);
                    },
                    child: Container(
                      width: 60,
                      child: Center(
                          child: Text(
                        'حسب الأحدث',
                        style: TextStyle(
                            color: homeProvider.isLatest
                                ? whiteColor
                                : mainAppColor,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                      )),
                      decoration: BoxDecoration(
                          border: Border.all(color: mainAppColor),
                          color: homeProvider.isLatest
                              ? mainAppColor
                              : greenColor,
                          borderRadius: BorderRadius.circular(10)),
                    ),
                  ),
                ],
              ));
        }),
        Expanded(
            child: _homeProvider.enableSearch
                ? FutureBuilder<List<Product>>(
                    future: _searchResultProductList,
                    builder: (context, snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                          return ListView.separated(
                            itemCount: 20,
                            physics: BouncingScrollPhysics(),
                            itemBuilder: (context, index) {
                              return ShimmerAd();
                            },
                            separatorBuilder: (context, index) {
                              return Divider();
                            },
                          );
                        case ConnectionState.active:
                          return Text('');
                        case ConnectionState.waiting:
                          return ListView.separated(
                            itemCount: 20,
                            physics: BouncingScrollPhysics(),
                            itemBuilder: (context, index) {
                              return ShimmerAd();
                            },
                            separatorBuilder: (context, index) {
                              return Divider();
                            },
                          );
                        case ConnectionState.done:
                          if (snapshot.hasError) {
                            print(snapshot.error.toString());
                            return Center(
                              child: Text(snapshot.error.toString()),
                            );
                          } else if (snapshot.data.length > 0) {
                            print(
                                "snapshot.data.length search ${snapshot.data.length} ");
                            return NotificationListener<ScrollNotification>(
                              onNotification: (ScrollNotification scrollInfo) {
                                if (scrollInfo.metrics.pixels ==
                                        scrollInfo.metrics.maxScrollExtent &&
                                    _homeProvider.currentPageSearch <
                                        _homeProvider.noOfPagesSearch) {
                                  _homeProvider.setCurrentPageSearch(
                                      _homeProvider.currentPageSearch + 1);
                                  _productList =
                                      _homeProvider.getSearchResultList(
                                    context,
                                  );
                                }
                              },
                              child: ListView.separated(
                                controller: _scrollListViewController,
                                itemCount:
                                    _homeProvider.searchResultList.length,
                                itemBuilder: (context, index) {
                                  return Consumer<HomeProvider>(
                                      builder: (context, homeProvider, child) {
                                    List<Product> myList;
                                    myList = homeProvider.searchResultList;
                                    // snapshot.data;
                                    return homeProvider.grid
                                        ? Container(
                                            height: 250,
                                            child: GridAd(
                                              product: myList[index],
                                            ),
                                          )
                                        : Ad(
                                            product: myList[index],
                                          );
                                  });
                                },
                                separatorBuilder: (context, index) {
                                  return Divider();
                                },
                              ),
                            );
                          } else
                            return Center(
                              child: NoData(
                                message: 'لاتوجد منتجات',
                              ),
                            );
                      }
                      return ListView.separated(
                        itemCount: 20,
                        physics: BouncingScrollPhysics(),
                        itemBuilder: (context, index) {
                          return ShimmerAd();
                        },
                        separatorBuilder: (context, index) {
                          return Divider();
                        },
                      );
                    })
                :
                FutureBuilder<List<Product>>(
                    future: _productList,
                    builder: (context, snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                          return ListView.separated(
                            itemCount: 20,
                            physics: BouncingScrollPhysics(),
                            itemBuilder: (context, index) {
                              return ShimmerAd();
                            },
                            separatorBuilder: (context, index) {
                              return Divider();
                            },
                          );
                        case ConnectionState.active:
                          return Text('');
                        case ConnectionState.waiting:
                          return ListView.separated(
                            itemCount: 20,
                            physics: BouncingScrollPhysics(),
                            itemBuilder: (context, index) {
                              return ShimmerAd();
                            },
                            separatorBuilder: (context, index) {
                              return Divider();
                            },
                          );
                        case ConnectionState.done:
                          if (snapshot.hasError) {
                            print(snapshot.error.toString());
                            return Center(
                              child: Text(snapshot.error.toString()),
                            );
                          }
                          else if (snapshot.data.length > 0) {
                            print(
                                "_homeProvider.productList.length ${_homeProvider.productList.length}");
                            return NotificationListener<ScrollNotification>(
                                onNotification:
                                    (ScrollNotification scrollInfo) {
                                  if (scrollInfo.metrics.pixels ==
                                          scrollInfo.metrics.maxScrollExtent &&
                                      _homeProvider.currentPageHome <
                                          _homeProvider.noOfPagesHome) {
                                    _homeProvider.setCurrentPageHome(
                                        _homeProvider.currentPageHome + 1);
                                    _homeProvider.getProductList(
                                      context,
                                    );
                                  }
                                },
                                child: ListView.separated(
                                  controller: _scrollListViewController,
                                  itemCount: _homeProvider.productList.length,
                                  itemBuilder: (context, index) {
                                    return Consumer<HomeProvider>(builder:
                                        (context, homeProvider, child) {
                                      print(
                                          "snapshot.data.length ${snapshot.data.length}");
                                      List<Product> myList;
                                      myList = _homeProvider.productList;
                                      //  myList.removeWhere((element) => element.id == )
                                      return homeProvider.grid
                                          ? Container(
                                              height: 250,
                                              child: GridAd(
                                                product: myList[index],
                                              ),
                                            )
                                          : Ad(
                                              product: myList[index],
                                            );
                                    });
                                  },
                                  separatorBuilder: (context, index) {
                                    return Divider();
                                  },
                                ));
                          }
                          else
                            return Center(
                              child: NoData(
                                message: 'لاتوجد منتجات',
                              ),
                            );
                      }
                      return ListView.separated(
                        itemCount: 20,
                        physics: BouncingScrollPhysics(),
                        itemBuilder: (context, index) {
                          return ShimmerAd();
                        },
                        separatorBuilder: (context, index) {
                          return Divider();
                        },
                      );
                    })),
      ],
    );
  }
}
