import 'package:flutter/material.dart';
import 'package:haraj/utils/app_colors.dart';

class CategoryItem extends StatelessWidget {
  final String text;
  final bool isSelected;
  final String cat_image;
  const CategoryItem(
      {Key key, this.isSelected: false, this.text, this.cat_image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: 60,
      decoration: isSelected
          ? BoxDecoration(
              borderRadius: BorderRadius.circular(11.0),
              gradient: LinearGradient(
                colors: [mainAppColor, mainAppColor],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
              ),
              border: Border.all(color: mainAppColor))
          : BoxDecoration(
              borderRadius: BorderRadius.circular(11.0),
              border: Border.all(color: mainAppColor)),
      child: Text(
        text,
        style: TextStyle(
            color: isSelected ? Colors.black : whiteColor,
            fontFamily: 'ArefRuqaa',
            fontSize: 14),
        textAlign: TextAlign.center,
      ),
   //   padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(left: 8, right: 8, top: 2, bottom: 2),
    );
  }
}
