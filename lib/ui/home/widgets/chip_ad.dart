import 'package:flutter/material.dart';


class ChipAd extends StatefulWidget {
  final String label;
  final Color color;
  final txtColor;
  final String imgUrl;

  const ChipAd({Key key, this.label, this.color, this.txtColor, this.imgUrl})
      : super(key: key);
  @override
  _ChipAdState createState() => _ChipAdState();
}

class _ChipAdState extends State<ChipAd> {
  @override
  Widget build(BuildContext context) {
  final width = MediaQuery.of(context).size.width;
    return Container(
      width:  width *0.26,
      child: Chip(
      
        avatar: Image.asset(widget.imgUrl, color:  Colors.black,),
        label: 
        Text(
          widget.label,
          overflow:TextOverflow.ellipsis ,
          style: TextStyle(

              color: widget.txtColor,
              fontFamily: 'Almarai',
              fontWeight: FontWeight.normal,
              fontSize: 10),
        ),
        backgroundColor: widget.color,
        elevation: 0,
        shadowColor: Colors.grey[50],
        // padding: EdgeInsets.all(5),
      ),
      margin: EdgeInsets.only(top: 2, bottom: 2),
    );
  }
}
