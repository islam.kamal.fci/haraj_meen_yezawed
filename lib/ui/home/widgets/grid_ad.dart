import 'package:flutter/material.dart';
import 'package:haraj/models/product.dart';
import 'package:haraj/providers/ad_details_provider.dart';
import 'package:haraj/ui/home/widgets/chip_ad.dart';
import 'package:haraj/ui/profile/announced_profile_screen.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:provider/provider.dart';
import 'package:haraj/providers/view_provider.dart';

class GridAd extends StatelessWidget {
  final Product product;

  const GridAd({Key key, this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    AdDetailsProvider adDetailsProvider =
        Provider.of<AdDetailsProvider>(context);
    ViewProvider viewProvider = Provider.of<ViewProvider>(context);
    return InkWell(
      onTap: () {
        adDetailsProvider.setProductId(product.id);
        Navigator.pushNamed(context, '/ad_details_screen');
      },
      child: Container(
        color: viewProvider.viewedProducts.contains(product.id)
            ? Color(0xff1E1C1C)
            : greenColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: width * 0.04),
              width: width,
              height: width * 0.3,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10.00)),
                child: Image.network(
                  product.image,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              child: Align(
                alignment: Alignment.centerRight,
                child: Text(
                  product.title,
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    product.addressName,
                    style: TextStyle(color: whiteColor, fontSize: 14),
                  ),
                  Row(children: [
                    ChipAd(
                      color: secondary_color,
                      imgUrl: 'assets/images/time-line.png',
                      label: product.createdAt,
                      txtColor: whiteColor,
                    ),
                    InkWell(
                        onTap: () => authProvider.currentUser == null
                            ? Navigator.pushNamed(context, '/login_screen')
                            : Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        AnnouncedProfileScreen(
                                          id: product.userId.toString(),
                                          name: product.userName,
                                        )),
                              ),
                        child: Container(
                          margin: EdgeInsets.only(right: 5),
                          child: ChipAd(
                            color: secondary_color,
                            imgUrl: 'assets/images/user-line.png',
                            label: product.userName,
                            txtColor: whiteColor,
                          ),
                        ))
                  ])
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
