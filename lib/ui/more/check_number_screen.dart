import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/validation_mixin.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';
import 'package:provider/provider.dart';

import '../../shared_preferences/shared_preferences_helper.dart';

class CheckNumberScreen extends StatefulWidget {
  @override
  _CheckNumberScreenState createState() => _CheckNumberScreenState();
}

class _CheckNumberScreenState extends State<CheckNumberScreen>
    with ValidationMixin {
  double _height, _width;
  AppBar _appBar;
  String _userName = '';
  ApiProvider _apiProvider = ApiProvider();
  bool _isLoading = false, initRun = true;
  AuthProvider _authProvider;
  final _formKey = GlobalKey<FormState>();
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (initRun) {
      _authProvider = Provider.of<AuthProvider>(context);
      initRun = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;
    _appBar = AppBar(
         backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        'القائمة السوداء',
        style: Theme.of(context).textTheme.headline1,
      ),
      leading: InkWell(
        onTap: () => Navigator.pop(context),
        child: Image.asset('assets/images/arrow_back.png',color: whiteColor,),
      ),
    );

    return NetworkIndicator(
        child:PageContainer(child:Scaffold(
         backgroundColor: greenColor,
      appBar: _appBar,
      body: _buildBodyWidget())),
    );
  }

  Widget _buildBodyWidget() {
    return ListView(
      children: [
        Form(
          key: _formKey,
          child: Container(
            margin: EdgeInsets.symmetric(vertical: _height * 0.03),
            child: CustomTextFormField(
              labelText: 'الاسم',
              enableBorder: true,
              validationFunc: validateUserName,
              inputData: TextInputType.text,
              onChangedFunc: (String val) {
                _userName = val;
              },
            ),
          ),
        ),
        _isLoading
            ? Container(
                margin: EdgeInsets.only(
                    bottom: _height * 0.02, top: _height * 0.05),
                child: SpinKitDoubleBounce(color: mainAppColor),
              )
            : Container(
                margin: EdgeInsets.symmetric(vertical: _height * 0.02),
                child: CustomButton(
                  btnLbl: 'تحقق',
                  onPressedFunction: () async {
                    if (_formKey.currentState.validate()) {
                      setState(() {
                        _isLoading = true;
                      });
                      var result = await _apiProvider
                          .post(Urls.BLOCKED_USERS_URL, body: {
                        'search': _userName
                      }, header: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization':
                            'Bearer ${_authProvider.currentUser.accessToken}'
                      });
                      setState(() {
                        _isLoading = false;
                      });
                      if (result['status_code'] == 200) {
                        if (result['response']['msg'] ==
                            "هذا الرقم غير موجود في القائمه السوداء") {
                          Commons.showMessageDialog(
                            context: context,
                            msg: result['response']['msg'],
                          );
                        }else if(result["status_code" ] == 401){
      Commons.showError(context: context,message: 'يرجى تسجيل الدخول مجدداً',
      onTapOk: (){
 Navigator.pop(context);

                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login_screen', (Route<dynamic> route) => false);
                        SharedPreferencesHelper.remove("user");
      });
   
      } else {
                          Commons.showMessageDialogNoImg(
                            context: context,
                            msg: result['response']['msg'],
                          );
                        }
                      } else {
                        Commons.showToast(
                            message: result['response']['error'],
                            context: context);
                      }
                    }
                  },
                )),
      ],
    );
  }
}
