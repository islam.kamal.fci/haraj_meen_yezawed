import 'dart:convert';
import 'dart:io' show Platform;
import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/models/page_content.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/providers/static_pages_provider.dart';
import 'package:haraj/ui/payment/gateway_payment_screen.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:provider/provider.dart';

class CommissionCalculate extends StatefulWidget {
  @override
  _CommissionCalculateState createState() => _CommissionCalculateState();
}

class _CommissionCalculateState extends State<CommissionCalculate> {
  double _height, _width;
  AppBar _appBar;

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;
    _appBar = AppBar(
      backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        'حساب عمولة الموقع',
        style: Theme.of(context).textTheme.headline1,
      ),
      leading: InkWell(
        onTap: () => Navigator.pop(context),
        child: Image.asset(
          'assets/images/arrow_back.png',
          color: whiteColor,
        ),
      ),
    );

    return NetworkIndicator(
        child: PageContainer(
      child: Scaffold(
        backgroundColor: greenColor,
        appBar: _appBar,
        body: _buildBodyWidget(),
      ),
    ));
  }

  Widget _buildBodyWidget() {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);

    return FutureBuilder<List<PageContent>>(
        future: Provider.of<StaticPagesProvider>(context, listen: false)
            .getStaticPage(context),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.active:
              return Text('');
            case ConnectionState.waiting:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.done:
              if (snapshot.hasError) {
                print(snapshot.error.toString());
                return Center(
                  child: Text('حدث خطأ ما'),
                );
              } else {
                return ListView(
                  physics: BouncingScrollPhysics(),
                  children: [

                    Container(
                      margin: EdgeInsets.symmetric(vertical: _height * 0.02),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: _width * 0.05),
                              child: Text('دفع العمولة',
                                  style: TextStyle(
                                      color: whiteColor,
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold))),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Column(
                                children: [
                                  Container(
                                    height: 55,
                                    width: _width * 0.3,
                                    child: CustomButton(
                                      enableHorizontalMargin: false,
                                      btnLbl: 'الدفع الإلكتروني',
                                      btnStyle: TextStyle(color: whiteColor),
                                      onPressedFunction: () {
                                        var bytes1 = utf8.encode("haraj" +
                                            authProvider.currentUser.id
                                                .toString() +
                                            "almotamiz"); // data being hashed
                                        var hashValue = sha256
                                            .convert(bytes1); // Hashing Process

                                        print("hashValue: $hashValue");
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    GatewayPaymentScreen(
                                                        checkoutUrl:
                                                            'https://harajalmotamiz.com.sa/payment/first-step/' +
                                                                authProvider
                                                                    .currentUser
                                                                    .id
                                                                    .toString() +
                                                                "/" +
                                                                hashValue
                                                                    .toString())));
                                      },
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Image.asset(
                                        'assets/images/mada.png',
                                        height: 40,
                                        width: 25,
                                        color: mainAppColor,
                                      ),
                                      
                                      SizedBox(width: 5,),
                                      Icon(FontAwesomeIcons.ccMastercard,color: mainAppColor,
                                      size: 25,),
                                      
                                      Platform.isIOS
                                          ? Row(
                                            children: [
                                              SizedBox(width: 5,),
                                              Icon(FontAwesomeIcons.applePay,color: mainAppColor,
                                      size: 25),
                                            ],
                                          )
                                          : Container(),
                                          SizedBox(width: 5,),
                                      Icon(FontAwesomeIcons.ccVisa,color: mainAppColor,
                                      size: 25)
                                    ],
                                  )
                                ],
                              ),
                              Column(
                                children: [
                                  Container(
                                    height: 55,
                                    width: _width * 0.3,
                                    child: CustomButton(
                                      enableHorizontalMargin: false,
                                      btnLbl: 'التحويل البنكي',
                                      btnStyle: TextStyle(color: whiteColor),
                                      onPressedFunction: () =>
                                          Navigator.pushNamed(
                                              context, '/bank_transfer_screen'),
                                    ),
                                  ),
                                  Container(
                                    height: 50,
                                  )
                                ],
                              ),
                              Platform.isIOS
                                  ? Column(
                                    children: [
                                      Container(
                                          height: 55,
                                          width: _width * 0.3,
                                          child: CustomButton(
                                              enableHorizontalMargin: false,
                                              btnLbl: 'Apple Pay',
                                              onPressedFunction: () {
                                                var bytes1 = utf8.encode("haraj" +
                                                    authProvider.currentUser.id
                                                        .toString() +
                                                    "almotamiz"); // data being hashed
                                                var hashValue = sha256.convert(
                                                    bytes1); // Hashing Process

                                                print("hashValue: $hashValue");
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            GatewayPaymentScreen(
                                                                checkoutUrl: 'https://harajalmotamiz.com.sa/payment/apple-pay/first-step/' +
                                                                    authProvider
                                                                        .currentUser
                                                                        .id
                                                                        .toString() +
                                                                    "/" +
                                                                    hashValue
                                                                        .toString())));
                                              }),
                                        ),
                                          Container(
                                    height: 50,
                                  )
                                        
                                    ],
                                  )
                                  : Container(),
                                  
                            ],
                          )
                        ],
                      ),
                    ),
                    Divider(),
                    Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: _width * 0.04, vertical: _height * 0.02),
                      child: Text(
                        snapshot.data[3].content[0].body,
                        style: TextStyle(
                            color: whiteColor, height: 1.6, fontSize: 14),
                      ),
                    ),
                    InkWell(
                      onTap: () => Navigator.pushNamed(
                          context, '/payment_policy_screen'),
                      child: Center(
                        child: Container(
                          margin: EdgeInsets.symmetric(vertical: _height * 0.02),
                          child: Text(
                            'سياسة دفع العمولة',
                            style: TextStyle(
                              decoration: TextDecoration.underline,
                              fontWeight: FontWeight.bold,
                              fontSize: 13,
                              color: mainAppColor,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                );
              }
          }
          return Center(
            child: SpinKitDoubleBounce(
              color: mainAppColor,
            ),
          );
        });
  }
}
