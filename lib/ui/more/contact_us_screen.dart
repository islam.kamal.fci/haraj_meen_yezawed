import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';

import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/validation_mixin.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/shared_preferences/shared_preferences_helper.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';
import 'package:provider/provider.dart';

class ContactUsScreen extends StatefulWidget {
  @override
  _ContactUsScreenState createState() => _ContactUsScreenState();
}

class _ContactUsScreenState extends State<ContactUsScreen>
    with ValidationMixin {
  ApiProvider _apiProvider = ApiProvider();
  bool _isLoading = false;
  AuthProvider _authProvider;
  String _email = '', _reasonOfContact = '', _message = '';
  final _formKey = GlobalKey<FormState>();

  Widget _buildBodyItem() {
    _authProvider = Provider.of<AuthProvider>(context);
    return Form(
      key: _formKey,
      child: Padding(
        padding: EdgeInsets.only(top: 20),
        child: Column(
          children: [
            CustomTextFormField(
              inputData: TextInputType.emailAddress,
              labelText: 'البريد الإلكتروني',
              fillColor: whiteColor,

              onChangedFunc: (String text) {
                _email = text;
              },
              validationFunc: validateEmail,
            ),
            SizedBox(
              height: 20,
            ),
            CustomTextFormField(
              inputData: TextInputType.text,
              labelText: ' سبب الاتصال',
              fillColor: whiteColor,

              onChangedFunc: (String text) {
                _reasonOfContact = text;
              },
              validationFunc: validateContactReason,
            ),
            SizedBox(
              height: 20,
            ),
            CustomTextFormField(
              maxLines: 3,
              inputData: TextInputType.text,
              fillColor: whiteColor,
              labelText: ' نص الرسالة',
              onChangedFunc: (String text) {
                _message = text;
              },
              validationFunc: validateMsg,
            ),
            Spacer(),
            _isLoading
                ? Center(
              child: SpinKitDoubleBounce(color: mainAppColor),
            )
                : CustomButton(
              btnLbl: 'إرسال',
              btnStyle: TextStyle(color: whiteColor),
              onPressedFunction: () => _sendMessage(),
            ),
            Spacer(),
          ],
        ),
      )
    );
  }

  void _sendMessage() async {
    if (_formKey.currentState.validate()) {
      setState(() {
        _isLoading = true;
      });
      var result = await _apiProvider.post(Urls.CONTACT_URL, body: {
        "email": _email,
        "subject": _reasonOfContact,
        "content": _message
      }, header: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Accept-Language': 'ar',
        'Authorization': 'Bearer ${_authProvider.currentUser.accessToken}'
      });
      print("result : ${result}");
      setState(() {
        _isLoading = false;
      });
      if (result["status_code"] == 200) {
        Commons.showToast(
            message: result['response']['msg'],
            context: context,
            color: hintColor);
        Navigator.pop(context);
      } else if (result["status_code"] == 401)
        Commons.showError(
            context: context,
            message: 'يرجى تسجيل الدخول مجدداً',
            onTapOk: () {
              Navigator.pop(context);

              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/login_screen', (Route<dynamic> route) => false);
              SharedPreferencesHelper.remove("user");
            });
      else if(result["status_code"] == 422)
        Commons.showToast(
            message: result['response']['email'].toString().substring(1,result['response']['email'].toString().length-1),
            context: context,
            color: hintColor);
      else
        Commons.showToast(
            message: result['response']['error'],
            context: context,
            color: hintColor);
    }
  }

  @override
  Widget build(BuildContext context) {
    AppBar appBar = AppBar(
      backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        'اتصل بنا',
        style: Theme.of(context).textTheme.headline1,
      ),
      leading: InkWell(
        onTap: () => Navigator.pop(context),
        child: Image.asset(
          'assets/images/arrow_back.png',
          color: whiteColor,
        ),
      ),
    );

    return NetworkIndicator(
        child: PageContainer(
            child: Scaffold(
              resizeToAvoidBottomInset: false,
      backgroundColor: greenColor,
      appBar: appBar,
      body: _buildBodyItem(),
    )));
  }
}
