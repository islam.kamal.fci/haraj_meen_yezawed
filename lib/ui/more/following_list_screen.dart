import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/models/product.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/providers/following_provider.dart';
import 'package:haraj/ui/more/widgets/following_item.dart';
import 'package:haraj/ui/more/widgets/ad_item.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:provider/provider.dart';

class FollowingListScreen extends StatefulWidget {
  @override
  _FollowingListScreenState createState() => _FollowingListScreenState();
}

class _FollowingListScreenState extends State<FollowingListScreen> {

  @override
  Widget build(BuildContext context) {
   
    final  appBar = AppBar(
         backgroundColor: mainAppColor,
        centerTitle: true,
        title: Text(
          'قائمه المتابعة',
          style: Theme.of(context).textTheme.headline1,
        ),
        leading: InkWell(
          onTap: () => Navigator.pop(context),
          child: Image.asset('assets/images/arrow_back.png',color: mainAppColor,),
        ),
        bottom: TabBar(
          indicatorColor: whiteColor,
          tabs: [
            Tab(
                child: Text('اعلانات',
                    style: TextStyle(color: whiteColor, fontSize: 14))),
            Tab(
                child: Text('أشخاص',
                    style: TextStyle(color: whiteColor, fontSize: 14))),
          ],
        ));

    return NetworkIndicator(
      child:
      PageContainer(
      child: DefaultTabController(
      length: 2,
      child: Scaffold(
           backgroundColor: greenColor,
        appBar: appBar,
        body: TabBarView(
          children: [
            _listOfAds(),
            _listOfFollwing(),
          ],
        ),
      ),
    ) ));
  }

  Widget _listOfAds() {
    return FutureBuilder<List<Product>>(
        future: Provider.of<FollowingProvider>(context, listen: false)
            .getFollowingAds(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.active:
              return Text('');
            case ConnectionState.waiting:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.done:
              if (snapshot.hasError) {
                print(snapshot.error.toString());
                return Center(
                  child: Text('حدث خطأ ما',style: TextStyle(color: whiteColor),),
                );
              } else {
                return snapshot.data.length != 0
                    ? ListView.separated(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        itemBuilder: (context, index) {
                          return AdItem(
                            product: snapshot.data[index],
                          );
                        },
                        separatorBuilder: (context, _) {
                          return Divider();
                        },
                        itemCount: snapshot.data.length)
                    : Center(
                        child: Text(
                          'لا توجد اعلانات متابعة',
                          style: TextStyle(color: whiteColor, fontSize: 14),
                        ),
                      );
              }
          }
          return Center(
            child: SpinKitDoubleBounce(
              color: mainAppColor,
            ),
          );
        });
  }

  Widget _listOfFollwing() {
    return FutureBuilder<List<User>>(
        future: Provider.of<FollowingProvider>(context, listen: true)
            .getFollowingUsers(context),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.active:
              return Text('');
            case ConnectionState.waiting:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.done:
              if (snapshot.hasError) {
                print(snapshot.error.toString());
                return Center(
                  child: Text('حدث خطأ ما'),
                );
              } else {
                return snapshot.data.length != 0
                    ? ListView.separated(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        itemBuilder: (context, index) {
                          return FollowingItem(
                            user: snapshot.data[index],
                          );
                        },
                        separatorBuilder: (context, _) {
                          return Divider();
                        },
                        itemCount: snapshot.data.length)
                    : Center(
                        child: Text(
                          'لا يوجد أشخاص تتابعهم',
                          style: TextStyle(color: whiteColor, fontSize: 14),
                        ),
                      );
              }
          }
          return Center(
            child: SpinKitDoubleBounce(
              color: mainAppColor,
            ),
          );
        });
  }
}
