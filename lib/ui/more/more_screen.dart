import 'package:flutter/material.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/ui/more/widgets/more_item.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:provider/provider.dart';

class MoreScreen extends StatefulWidget {
  @override
  _MoreScreenState createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  double _height, _width;
  AuthProvider _authProvider;
  @override
  void didChangeDependencies() {
    _authProvider = Provider.of<AuthProvider>(context, listen: false);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: greenColor,
      body:_buildBodyWidget() ,
    ) ;
  }

  Widget _buildBodyWidget() {
     return Stack(
       children: [
         Container(
           padding: EdgeInsets.symmetric(horizontal: _width * 0.05,
           vertical: 20),
           width: _width,
           height: 120,
           color: greenColor,
           child: Text(
                    'أهلاً ... ${_authProvider.currentUser.name}',
                    style: TextStyle(color: whiteColor),
                  ) ,
         ),
        
       Column(
         children: [
           Expanded(
             child: Container(
                      margin: EdgeInsets.only(left: _width * 0.05,
                      right: _width * 0.05,top: 60,bottom: 20),
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.shade600,
                          spreadRadius: 1,
                          blurRadius: 6,
                        ),
                      ],
                      color: greenColor,
                      borderRadius: BorderRadius.all(
                        Radius.circular(12.0),
                      )),
                 
                  width: _width,
                  child: ListView(
                    children: [
                         MoreItem(
                                    imgUrl: 'assets/images/add-fill.png',
                                    title: 'أضف إعلانك',
                                    route: '/navigation',
                                  ),
                                  Divider(
                                    color: Colors.white,
                                  ),
                                  MoreItem(
                                    imgUrl: 'assets/images/advertisement-line.png',
                                    title: 'إعلاناتي',
                                    route: '/myAds_screen',
                                  ),
                                 Divider(
                                    color: Colors.white,
                                  ),
                                  MoreItem(
                                    imgUrl: 'assets/images/user-follow-line.png',
                                    title: 'الصفحة الشخصية',
                                    route: '/user_profile_screen',
                                  ),
                                   Divider(
                                    color: Colors.white,
                                  ),
                                  MoreItem(
                                    imgUrl:
                                        'assets/images/money-dollar-circle-line.png',
                                    title: 'حساب عمولة الموقع',
                                    route: '/commission_screen',
                                  ),
                                  Divider(
                                    color: Colors.white,
                                  ),
                                  MoreItem(
                                    imgUrl: 'assets/images/user-follow-line.png',
                                    title: 'قائمة المتابعة',
                                    route: '/following_list_screen',
                                  ),
                                Divider(
                                    color: Colors.white,
                                  ),
                                   MoreItem(
                                    imgUrl: 'assets/images/sub.png',
                                    title: 'الإشتراكات',
                                    route:   '/subscriptions_screen'
                                  ),
                                Divider(
                                    color: Colors.white,
                                  ),
                                  MoreItem(
                                    imgUrl: 'assets/images/coupon-4-line.png',
                                    title: 'العروض',
                                    route: '/discount_system_screen',
                                  ),
                                 Divider(
                                    color: Colors.white,
                                  ),
                                  MoreItem(
                                    imgUrl: 'assets/images/forbid-line.png',
                                    title: 'قائمة السلع والاعلانات الممنوعة',
                                    route: '/prohibted_goods_screen',
                                  ),
                                Divider(
                                    color: Colors.white,
                                  ),
                                  MoreItem(
                                    imgUrl: 'assets/images/spam-3-line.png',
                                    title: 'القائمة السوداء',
                                    route: '/black_list_screen',
                                  ),
                                 Divider(
                                    color: Colors.white,
                                  ),
                                  MoreItem(
                                    imgUrl: 'assets/images/file-list-2-line.png',
                                    title: 'معاهدة استخدام الخدمة',
                                    route: '/usage_treaty_screen',
                                  ),
                               Divider(
                                    color: Colors.white,
                                  ),
                                  MoreItem(
                                    imgUrl: 'assets/images/question-line.png',
                                    title: 'اتصل بنا',
                                    route: 'contact_us_screen',
                                  ),
                                    Divider(
                                    color: Colors.white,
                                  ),
                                  MoreItem(
                                    imgUrl: 'assets/images/logout.png',
                                    title: 'تسجيل الخروج',
                                    setColor: true,
                                  ),
                                     SizedBox(
                                    height: 15,
                                  ),
                    ],
                  ),
               ),
           ),
         ],
       )
         
       ],
     );
    
  }
}
