import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/models/page_content.dart';
import 'package:haraj/providers/static_pages_provider.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:provider/provider.dart';

class ProhibitedGoodsScreen extends StatefulWidget {
  @override
  _ProhibitedGoodsScreenState createState() => _ProhibitedGoodsScreenState();
}

class _ProhibitedGoodsScreenState extends State<ProhibitedGoodsScreen> {
  double _height, _width;
  AppBar _appBar;

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;
    _appBar = AppBar(
         backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        'قائمة السلع والأعلانات الممنوعة',
        style: Theme.of(context).textTheme.headline1,
      ),
      leading: InkWell(
        onTap: () => Navigator.pop(context),
        child: Image.asset('assets/images/arrow_back.png',color: whiteColor,),
      ),
    );

    return NetworkIndicator(
        child:PageContainer(child: Scaffold(
             backgroundColor: greenColor,
      appBar: _appBar,
      body: _buildBodyWidget(),
    )));
  }

  Widget _buildBodyWidget() {
    return FutureBuilder<List<PageContent>>(
        future: Provider.of<StaticPagesProvider>(context, listen: false)
            .getStaticPage(context),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.active:
              return Text('');
            case ConnectionState.waiting:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.done:
              if (snapshot.hasError) {
                print(snapshot.error.toString());
                return Center(
                  child: Text('حدث خطأ ما'),
                );
              } else {
                return ListView(
                  padding: EdgeInsets.all(10),
                  physics: BouncingScrollPhysics(),
                  children: [
                    Image.asset(
                      'assets/images/tlogo.png',
                      height: _height * 0.13,
                    ),
                    Container(
                      margin: EdgeInsets.only(top: _height * 0.05),
                      child: Text(snapshot.data[0].content[0].title,
                          style: TextStyle(
                              color: mainAppColor,
                              fontSize: 14,
                              fontWeight: FontWeight.bold)),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          top: _height * 0.015,
                          right: _width * 0.02,
                          left: _width * 0.02),
                      child: Text(snapshot.data[0].content[0].body,
                            textAlign: TextAlign.justify,
                          style: TextStyle(
                              color: whiteColor,
                              height: 1.6,
                              fontSize: 14,
                              fontWeight: FontWeight.bold)),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: _height * 0.05),
                      child: Text(snapshot.data[0].content[1].title,
                          style: TextStyle(
                              color: whiteColor,
                              fontSize: 14,
                              fontWeight: FontWeight.bold)),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: _height * 0.015),
                      child: Text(snapshot.data[0].content[1].body,
                            textAlign: TextAlign.justify,
                          style: TextStyle(
                              color: whiteColor,
                              height: 1.6,
                              fontSize: 14,
                              fontWeight: FontWeight.bold)),
                    ),
                  ],
                );
              }
          }
          return Center(
            child: SpinKitDoubleBounce(
              color: mainAppColor,
            ),
          );
        });
  }
}
