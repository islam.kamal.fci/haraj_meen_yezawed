import 'package:flutter/material.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/models/product.dart';
import 'package:haraj/providers/ad_details_provider.dart';
import 'package:haraj/providers/following_provider.dart';
import 'package:haraj/ui/home/widgets/chip_ad.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:provider/provider.dart';

class AdItem extends StatefulWidget {
  final Product product;

  const AdItem({Key key, this.product}) : super(key: key);
  @override
  _AdItemState createState() => _AdItemState();
}

class _AdItemState extends State<AdItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Provider.of<AdDetailsProvider>(context, listen: false)
            .setProductId(widget.product.id);
        Navigator.pushNamed(context, '/ad_details_screen');
      },
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            margin: EdgeInsets.only(right: 10, left: 12),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              child: Image.network(
                widget.product.image,
                fit: BoxFit.fitWidth,
                height: 95,
                width: 95,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 7),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width:MediaQuery.of(context).size.width*0.33 ,
                  child: Text(
                    widget.product.title,
                    style: TextStyle(
                        color: whiteColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 14),
                        overflow: TextOverflow.ellipsis,
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    widget.product.addressName,
                    style: TextStyle(color: whiteColor, fontSize: 13),
                  ),
                ),
             
                  ChipAd(
                    color: mainAppColor,
                    imgUrl: 'assets/images/time-line.png',
                    label: widget.product.createdAt,
                    txtColor: blackColor,
                  )
            
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width *0.33,
            height: 50,
            child: CustomButton(
              onPressedFunction: () {
                final provider =
                    Provider.of<FollowingProvider>(context, listen: false);
                //1
                provider.toggleAd(widget.product.id.toString());
                if (provider.listIdsOfAds.contains(widget.product.id)) {
                  //2
                  provider.removeFromListAds(widget.product.id);
                  Commons.showToast(
                      message: 'تم حذف الاعلان من قائمه المتابعه',
                      context: context);
                } else {
                  provider.addToListAds(widget.product.id);
                  Commons.showToast(
                      message: 'تم أضافه الاعلان لقائمه المتابعه',
                      context: context);
                }
              },
              btnLbl: 'الغاء المتابعة',
              btnStyle: TextStyle(fontSize: 12, color: whiteColor),
            ),
          )
        ],
      ),
    );
  }
}
