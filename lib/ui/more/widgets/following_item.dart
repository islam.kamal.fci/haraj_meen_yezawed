import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/providers/following_provider.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:provider/provider.dart';

class FollowingItem extends StatefulWidget {
  final User user;

  const FollowingItem({Key key, this.user}) : super(key: key);

  @override
  _FollowingItemState createState() => _FollowingItemState();
}

class _FollowingItemState extends State<FollowingItem> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return ListTile(
        leading: CircleAvatar(
          radius: 30,
          backgroundColor: Color(0xff383838),
           child: Icon(Icons.person,color: mainAppColor,)

        ),
        title: Text(widget.user.name,
        style: TextStyle(
          color: Colors.white
        ),
        ),
        trailing: Container(
          width: constraints.maxWidth * 0.4,
          child: CustomButton(
            onPressedFunction: () {
              final provider =
                  Provider.of<FollowingProvider>(context, listen: false);
              provider.toggleFollowing(widget.user.id.toString());
              if (provider.listIdsOfFollowing.contains(widget.user.id)) {
                //2
                provider.removeFromListFollowing(widget.user.id);
                Commons.showToast(
                    message: 'تم الحذف من قائمه المتابعه', context: context);
              } else {
                provider.addToListFollowing(widget.user.id);
                Commons.showToast(
                    message: 'تم الأضافه لقائمه المتابعه', context: context);
              }
            },
            btnLbl: 'الغاء المتابعة',
            btnStyle: TextStyle(fontSize: 12, color: whiteColor),
          ),
        ),
      );
    });
  }
}
