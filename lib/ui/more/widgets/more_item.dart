import 'package:flutter/material.dart';
import 'package:haraj/custom_widgets/dialogs/log_out_dialog.dart';
import 'package:haraj/providers/navigation_provider.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:provider/provider.dart';

class MoreItem extends StatefulWidget {
  final String imgUrl;
  final String title;
  final bool setColor;
  final String route;

  const MoreItem(
      {Key key, this.imgUrl, this.title, this.setColor = false, this.route})
      : super(key: key);
  @override
  _MoreItemState createState() => _MoreItemState();
}

class _MoreItemState extends State<MoreItem> {
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Consumer<NavigationProvider>(
      builder: (context, provider, _) {
        return Container(
          height: 40,
          child: InkWell(
            onTap: () {
              if (widget.route == '/navigation')
                provider.upadateNavigationIndex(2, context);
              else if (widget.title == 'تسجيل الخروج')
                showDialog(
                    barrierDismissible: true,
                    context: context,
                    builder: (_) {
                      return LogoutDialog();
                    });
              else
                Navigator.pushNamed(context, widget.route);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: width * 0.07, vertical: 8),
                      child: Image.asset(
                        widget.imgUrl,
                        color: secondary_color,
                      ),
                    ),
                    Text(
                      widget.title,
                      style: TextStyle(color: Colors.white, fontSize: 14),
                    )
                  ],
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: width * 0.07),
                  child: Image.asset(
                    'assets/images/arrow_left.png',
                    color: secondary_color,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
