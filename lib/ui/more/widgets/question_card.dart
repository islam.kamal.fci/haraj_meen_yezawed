import 'package:flutter/material.dart';
import 'package:expandable/expandable.dart';

class QuestionCard extends StatelessWidget {
  final String question;
  final String answer;

  const QuestionCard({Key key, @required this.question, @required this.answer})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ExpandableNotifier(
        child: Card(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      elevation: 2,
      child: Column(
        children: <Widget>[
          ScrollOnExpand(
            scrollOnExpand: true,
            scrollOnCollapse: false,
            child: ExpandablePanel(
              theme: const ExpandableThemeData(
                headerAlignment: ExpandablePanelHeaderAlignment.center,
                tapBodyToCollapse: true,
              ),
              header: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
                  child: Text(
                    question,
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  )),
              collapsed: Padding(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                child: Text(
                  answer,
                  style: TextStyle(fontSize: 14),
                  softWrap: true,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              builder: (_, collapsed, expanded) {
                return Padding(
                  padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                  child: Expandable(
                    collapsed: collapsed,
                    expanded: expanded,
                    theme: const ExpandableThemeData(
                   
                      crossFadePoint: 0),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    ));
  }
}
