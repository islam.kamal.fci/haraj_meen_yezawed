import 'package:flutter/material.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/no_data/no_data.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/custom_widgets/shared/shimmer_ad.dart';
import 'package:haraj/custom_widgets/shared/shimmer_notification.dart';
import 'package:haraj/models/notification.dart';
import 'package:haraj/providers/notification_provider.dart';
import 'package:haraj/ui/notifications/widgets/notification_item.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:provider/provider.dart';


class NotificationsScreen extends StatefulWidget {
  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> with
  TickerProviderStateMixin{
  // double _height, _width;
  
  AnimationController _animationController;

  bool _initialRun = true;
   Future<List<NotificationModel>> _notificationList;
  NotificationProvider _notificationProvider;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    if (_initialRun) {
      _notificationProvider = Provider.of<NotificationProvider>(context);
      _notificationList = _notificationProvider.getNotificationList(context);
      _initialRun = false;
    }
  }
  @override
  void initState() {
    _animationController = AnimationController(
        duration: Duration(milliseconds: 2000), vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // _height =
    //     MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    // _width = MediaQuery.of(context).size.width;
      AppBar appBar = AppBar(
        backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        'الإشعارات',
        style: Theme.of(context).textTheme.headline1,
      ),
      actions: [Image.asset('assets/images/notification.png')],
      leading: InkWell(
        onTap: () => Navigator.pop(context),
        child: Image.asset('assets/images/arrow_back.png'),
      ),
    );

    return NetworkIndicator(
        child: PageContainer(
            child: Scaffold(
              backgroundColor: greenColor,
      appBar: appBar,
      body: _buildBodyWidget(),
    )));
  }

  Widget _buildBodyWidget() {
    return  Container(
      margin: EdgeInsets.symmetric(vertical: 15),
      child: FutureBuilder<List<NotificationModel>>(
          future: _notificationList,
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return ListView.separated(
                  itemCount: 20,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, index) {
                    return ShimmerAd();
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                );
              case ConnectionState.active:
                return Text('');
              case ConnectionState.waiting:
                return ListView.separated(
                  itemCount: 20,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, index) {
                    return ShimmerNotification();
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                );
              case ConnectionState.done:
                if (snapshot.hasError) {
                  print(snapshot.error.toString());
                  return Center(
                    child: Text('حدث خطأ ما'),
                  );
              
                } else if (snapshot.data.length == 0) {
                  return Center(
                    child: NoData(message: 'لاتوجد إشعارات',)
                  );
                } else {
                 
                      return   Container(
      margin: EdgeInsets.only(top: 10),
      child: ListView.separated(
        physics: BouncingScrollPhysics(),
        itemCount: snapshot.data.length,
        itemBuilder: (context, index) {
            var count =snapshot.data.length;
          
                            var animation = Tween(begin: 0.0, end: 1.0).animate(
                              CurvedAnimation(
                                parent: _animationController,
                                curve: Interval((1 / count) * index, 1.0,
                                    curve: Curves.fastOutSlowIn),
                              ),
                            );
                            _animationController.forward();
          return NotificationItem(
            animation: animation,
            animationController: _animationController,
            notificationModel: snapshot.data[index],
          );
        },
        separatorBuilder: (context, index) {
          return Divider(
            color: Colors.white,
          );
        },
      ),
    );
                   
                }
            }
            return ListView.separated(
              itemCount: 20,
              physics: BouncingScrollPhysics(),
              itemBuilder: (context, index) {
                return ShimmerNotification();
              },
              separatorBuilder: (context, index) {
                return Divider();
              },
            );
          }),
    );
   
  }
}
