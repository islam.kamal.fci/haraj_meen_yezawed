import 'package:flutter/material.dart';
import 'package:haraj/models/notification.dart';
import 'package:haraj/utils/app_colors.dart';

class NotificationItem extends StatelessWidget {
    final AnimationController animationController;
  final Animation animation;
  final NotificationModel notificationModel;

  const NotificationItem({Key key,
  this.notificationModel, this.animationController, this.animation}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
   return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: animation,
          child: new Transform(
            transform: new Matrix4.translationValues(
                0.0, 50 * (1.0 - animation.value), 0.0),
            child: 
       Row(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 15),
            child: Image.asset(
              'assets/images/tlogo.png',
              height: 75,
              width: 75,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: width * 0.7,
                margin: EdgeInsets.only(bottom: 12),
                child: Text(
               notificationModel.body,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(fontSize: 14, 
                  color :  Colors.white,fontWeight: FontWeight.bold),
                ),
              ),
              Text(
               notificationModel.date,
                style: TextStyle(fontSize: 14, color: whiteColor),
              )
            ],
          )
        ],
      )
   
            
    ));
    }
    );
      
  }
}

