import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/validation_mixin.dart';
import 'package:haraj/custom_widgets/drop_down_list_selector/drop_down_list_selector.dart';
import 'package:haraj/custom_widgets/no_data/no_data.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/locale/app_localizations.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/shared_preferences/shared_preferences_helper.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';
import 'package:intl/intl.dart';

import 'package:provider/provider.dart';
import 'package:haraj/models/bank.dart';
import 'package:haraj/utils/error.dart';
import 'package:haraj/providers/bank_transfer_provider.dart';

class BankTransferScreen extends StatefulWidget {
  @override
  _BankTransferScreenState createState() => _BankTransferScreenState();
}

class _BankTransferScreenState extends State<BankTransferScreen>
    with TickerProviderStateMixin,ValidationMixin {
  double _height, _width;
  bool _initialRun = true;
  ApiProvider _apiProvider = ApiProvider();
  String _commissionAmount = '',
      _bankName = '',
      _commissionTime = '',
      _converterName = '', 
      _adNo = '',
      _notes = '';
  bool _isLoading = false;    
  bool _initalSelectedBank = true;
  Future<List<Bank>> _bankList;
  var _bankListForDropDown;
  final _formKey = GlobalKey<FormState>();
  Bank _selectedBank;
  AuthProvider _authProvider;
  BankTransferProvider _bankTransferProvider;

  @override
  void initState() {

    super.initState();
  }
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_initialRun) {
      _bankTransferProvider = Provider.of<BankTransferProvider>(context);
      _authProvider = Provider.of<AuthProvider>(context);
  
      _bankList = _bankTransferProvider.getBankList(context);
      _initialRun = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;

    AppBar appBar = AppBar(
         backgroundColor: Colors.black,
      centerTitle: true,
      title: Text(
        'التحويل البنكي',
        style: Theme.of(context).textTheme.headline1,
      ),
      leading: InkWell(
        onTap: () => Navigator.pop(context),
        child: Image.asset('assets/images/arrow_back.png'),
      ),
    );

    return PageContainer(
        child: Scaffold(
             backgroundColor: Colors.black,
      appBar: appBar,
      body: _buildBodyWidget(),
    ));
  }

  Widget _buildBodyWidget() {
    
    return SingleChildScrollView(
        child: Form(
            key: _formKey,
            child: Column(
              children: [
                FutureBuilder<List<Bank>>(
                    future: _bankList,
                    builder: (context, snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                          return Center(
                            child: SpinKitPulse(color: mainAppColor),
                          );
                        case ConnectionState.active:
                          return Text('');
                        case ConnectionState.waiting:
                          return Center(
                            child: SpinKitFadingCircle(color: mainAppColor),
                          );
                        case ConnectionState.done:
                          if (snapshot.hasError) {
                            return Error(
                                errorMessage: snapshot.error.toString());
                            // errorMessage: AppLocalizations.of(context)
                            //     .translate('error'));
                          } else {
                            if (snapshot.data.length > 0) {
                              if (_initalSelectedBank) {
                                _bankListForDropDown =
                                    snapshot.data.map((item) {
                                  return new DropdownMenuItem<Bank>(
                                    child: new Text(item.bankName),
                                    value: item,
                                  );
                                }).toList();
                                _selectedBank = snapshot.data[0];
                                _initalSelectedBank = false;
                              }

                              return Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.symmetric(vertical: 10),
                                    child: DropDownListSelector(
                                      dropDownList: _bankListForDropDown,
                                      hint: 'اختر البنك',
                                      onChangeFunc: (newValue) {
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        setState(() {
                                          _selectedBank = newValue;
                                        });
                                      },
                                      value: _selectedBank,
                                    ),
                                  ),
                                  Container(
                                      height: _height * 0.15,
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 10),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              RichText(
                                                text: TextSpan(
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      fontFamily: 'ArefRuqaa',
                                                      color: hintColor,
                                                    ),
                                                    children: [
                                                      TextSpan(
                                                          text: 'اسم البنك : '),
                                                      TextSpan(
                                                          text: _selectedBank
                                                                  .bankName ??
                                                              '',
                                                          style: TextStyle(
                                                              color: whiteColor,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)),
                                                    ]),
                                              ),
                                              SizedBox(height: 8),
                                              RichText(
                                                text: TextSpan(
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      fontFamily: 'ArefRuqaa',
                                                      color: hintColor,
                                                    ),
                                                    children: [
                                                      TextSpan(
                                                          text:
                                                              'رقم الحساب : '),
                                                      TextSpan(
                                                          text: _selectedBank
                                                              .accountNumber,
                                                          style: TextStyle(
                                                              color: whiteColor,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)),
                                                    ]),
                                              ),
                                              SizedBox(height: 8),
                                              RichText(
                                                text: TextSpan(
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      fontFamily: 'ArefRuqaa',
                                                      color: hintColor,
                                                    ),
                                                    children: [
                                                      TextSpan(
                                                          text:
                                                              'رقم الايبان : '),
                                                      TextSpan(
                                                          text: _selectedBank
                                                              .internationalAccount,
                                                          style: TextStyle(
                                                              color: whiteColor,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)),
                                                    ]),
                                              ),
                                            ],
                                          ),
                                          Image.network(
                                            _selectedBank.bankImage,
                                            width: _width * 0.3,
                                          )
                                        ],
                                      )),
                                ],
                              );
                            } else
                              return NoData(
                                  message: AppLocalizations.of(context)
                                      .translate('no_results'));
                          }
                          return Center(
                            child: SpinKitFadingCircle(color: mainAppColor),
                          );
                      }
                      return Center(
                        child: SpinKitFadingCircle(color: mainAppColor),
                      );
                    }),
                Divider(),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: CustomTextFormField(
                    enabled: false,
                    initialValue: _authProvider.currentUser.name,
                    labelText: 'اسم المستخدم',
                    enableBorder: false,
                    inputData: TextInputType.text,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: CustomTextFormField(
                    onChangedFunc: (String value){
                      _commissionAmount = value;
                    },
                    labelText: 'مبلغ العمولة',
                    enableBorder: false,
                    validationFunc: validateCommissionAmount,
                    suffixIcon:Column(
                      children: [
                        SizedBox(
                          height: 20,
                        ),
                        Text('ريال',style: TextStyle(
                          color: Colors.black,
                          fontSize: 13
                        ),)
                      ],
                    ),
                    inputData: TextInputType.number,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: CustomTextFormField(
                    labelText: 'البنك الذى تم التحويل اليه',
                    enableBorder: false,
                    validationFunc: validateBankName,
                    onChangedFunc: (String value){
                      _bankName = value;
                    },
                    inputData: TextInputType.text,
                  ),
                ),
               Container(
                  margin: EdgeInsets.only(top: 10),
                  child: CustomTextFormField(
                    labelText: 'متى تم التحويل',
                    validationFunc: validateConverterTime,
                    enableBorder: false,
                    onChangedFunc: (String value){
                      _commissionTime = value;
                    },
                    inputData: TextInputType.datetime,
                  ),
                ),

                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: CustomTextFormField(
                    labelText: 'أسم المحول',
                    enableBorder: false,
                     validationFunc: validateConverterName,
                    
                     onChangedFunc: (String value){
                      _converterName = value;
                    },
                    inputData: TextInputType.text,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: CustomTextFormField(
                    initialValue: _authProvider.currentUser.phone,
                    labelText: 'رقم الجوال المرتبط بعضويتك',
                       hintTxt: '05xxxxxxxx',
                   validationFunc: validatePhoneNo,
                    enableBorder: false,
                     enabled: false,
                    inputData: TextInputType.number,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 7),
                  child: CustomTextFormField(
                    labelText: 'رقم الأعلان',
                  validationFunc: validateAdNumber,
                    enableBorder: false,
                     onChangedFunc: (String value){
                      _adNo = value;
                    },
                    inputData: TextInputType.number,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: CustomTextFormField(
                      labelText: 'ملاحظات',
                      enableBorder: false,
                    
                     onChangedFunc: (String value){
                      _notes = value;
                    },
                      inputData: TextInputType.text),
                ),
             _isLoading
            ?  SpinKitDoubleBounce(color: mainAppColor)
              
            :     Container(
                  margin: EdgeInsets.symmetric(vertical: 40),
                  child: CustomButton(
                    btnLbl: 'ارسال',
                      onPressedFunction: () => _sendBankTransfer(),
                      
                  
                  ),
                )
              ],
            )));
  }

void _sendBankTransfer() async {
    if (_formKey.currentState.validate()) {
        FocusScope.of(context).requestFocus(FocusNode());
  
      setState(() {
        _isLoading = true;
      });
      var result = await _apiProvider.post(Urls.CREATE_BANK_TRANSFER, body: {
        "name": _authProvider.currentUser.name,
        "amount": _commissionAmount,
    "bank_name":_bankName,
    "datetransfer":_commissionTime,
    "moneysender":_converterName,
    "phone": _authProvider.currentUser.phone,
    "prod_id":_adNo,
    "notes":_notes
      },
      header:{
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${_authProvider.currentUser.accessToken}'
    });
      setState(() {
        _isLoading = false;
      });
      if (result["status_code"] == 200) {
      
      Commons.showToast(message:result['response']['data'],
       context: context);
       Navigator.pop(context);
    
      } 
      else if (result["status_code"] == 401) 
      Commons.showError(
          context: context,
          message: 'يرجى تسجيل الدخول مجدداً',
          onTapOk: () {
            Navigator.pop(context);

            Navigator.of(context).pushNamedAndRemoveUntil(
                '/login_screen', (Route<dynamic> route) => false);
            SharedPreferencesHelper.remove("user");
          });
    else 
        Commons.showToast(
            message: result['response']['error'],
            context: context,
            color: hintColor);
      
      
    }
    
  }
}
