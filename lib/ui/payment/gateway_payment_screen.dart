import 'dart:async';
import 'dart:core';
import 'package:flutter/material.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:webview_flutter/webview_flutter.dart';


class GatewayPaymentScreen extends StatefulWidget {
  final Function onTapBackFunction;
  final String checkoutUrl;

  const GatewayPaymentScreen({Key key, this.checkoutUrl, this.onTapBackFunction}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return GatewayPaymentScreenState();
  }
}

class GatewayPaymentScreenState extends State<GatewayPaymentScreen> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {
 

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: greenColor,
      appBar: 
      AppBar(
        backgroundColor: mainAppColor,
        leading: InkWell(
          child: Icon(Icons.arrow_back_ios,color: whiteColor,),
           onTap: () => widget.onTapBackFunction != null ? widget.onTapBackFunction() :
                    Navigator.of(context).pop()
        ),
      ),
      body: WebView(
        initialUrl: widget.checkoutUrl,
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController webViewController) {
          _controller.complete(webViewController);
        },
        navigationDelegate: (NavigationRequest request) {
          // if (request.url.contains('Result=Successful') &&
          //     request.url.contains('ResponseCode=000')) {
          //   Future.delayed(const Duration(seconds: 5), () {
          //     navigationState.upadateNavigationIndex(1);
          //     Navigator.of(context).pushNamedAndRemoveUntil(
          //         '/navigation', (Route<dynamic> route) => false);
          //   });
          // }

          return NavigationDecision.navigate;
        },
      ),
    );
  }
}
