// import 'package:flutter/material.dart';
// import 'package:haraj/custom_widgets/buttons/custom_button.dart';
// import 'package:haraj/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
// import 'package:haraj/custom_widgets/safe_area/page_container.dart';

// class MadaPaymentScreen extends StatefulWidget {
//   @override
//   _MadaPaymentScreenState createState() => _MadaPaymentScreenState();
// }

// class _MadaPaymentScreenState extends State<MadaPaymentScreen> {
//   double _height, _width;
//   AppBar _appBar;
//   @override
//   Widget build(BuildContext context) {
//     _height =
//         MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
//     _width = MediaQuery.of(context).size.width;
//     _appBar = AppBar(
//          backgroundColor: Colors.black,
//       centerTitle: true,
//       title: Text(
//         'الدفع بمدى',
//         style: Theme.of(context).textTheme.headline1,
//       ),
//       leading: InkWell(
//         onTap: () => Navigator.pop(context),
//         child: Image.asset('assets/images/arrow_back.png'),
//       ),
//     );

//     return PageContainer(
//       child: Scaffold(
//            backgroundColor: Colors.black,
//         appBar: _appBar,
//         body: _buildBodyWidget(),
//       ),
//     );
//   }

//   Widget _buildBodyWidget() {
//     return ListView(
//       padding: EdgeInsets.all(10),
//       physics: BouncingScrollPhysics(),
//       children: [
//         Image.asset(
//           'assets/images/mada.png',
//           height: _height * 0.13,
//         ),
//         Container(
//           margin: EdgeInsets.only(top: 7),
//           child: CustomTextFormField(
//             labelText: 'المبلغ المدفوع',
//             enableBorder: false,
//             suffixIcon: Text('ريال'),
//             inputData: TextInputType.number,
//           ),
//         ),
//         Container(
//           margin: EdgeInsets.only(top: 7),
//           child: CustomTextFormField(
//             labelText: 'رقم الأعلان',
//             enableBorder: false,
//             inputData: TextInputType.number,
//           ),
//         ),
//         Container(
//           margin: EdgeInsets.only(top: 7),
//           child: CustomTextFormField(
//             labelText: 'الأسم كما يظهر فى البطاقة',
//             enableBorder: false,
//             inputData: TextInputType.text,
//           ),
//         ),
//         Container(
//           margin: EdgeInsets.only(top: 7),
//           child: CustomTextFormField(
//             labelText: 'رقم البطاقة',
//             enableBorder: false,
//             inputData: TextInputType.text,
//           ),
//         ),
//         Container(
//           margin: EdgeInsets.only(top: 7),
//           child: CustomTextFormField(
//             labelText: 'تاريخ الانتهاء',
//             enableBorder: false,
//             inputData: TextInputType.text,
//           ),
//         ),
//         Container(
//           margin: EdgeInsets.only(top: 7),
//           child: CustomTextFormField(
//             labelText: 'رمز التحقق',
//             enableBorder: false,
//             inputData: TextInputType.text,
//           ),
//         ),
//         Container(
//           margin: EdgeInsets.only(top: _height * 0.1),
//           child: CustomButton(
//             btnLbl: 'دفع',
//           ),
//         )
//       ],
//     );
//   }
// }
