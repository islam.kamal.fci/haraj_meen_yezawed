import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/custom_widgets/shared/ad.dart';
import 'package:haraj/models/user_ads.dart';
import 'package:haraj/providers/following_provider.dart';
import 'package:haraj/providers/user_ads_provider.dart';
import 'package:haraj/ui/chat/chat_details_screen.dart';
import 'package:haraj/ui/evaluations/evaluations_screen.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:provider/provider.dart';

class AnnouncedProfileScreen extends StatefulWidget {
  final String id;
  final bool fromMyAds;
  final String name;

  const AnnouncedProfileScreen({Key key, 
  this.fromMyAds : true,
  this.id, this.name}) : super(key: key);
  @override
  _AnnouncedProfileScreenState createState() => _AnnouncedProfileScreenState();
}

class _AnnouncedProfileScreenState extends State<AnnouncedProfileScreen> {
  double _height, _width;
  AppBar _appBar;

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;
    _appBar = AppBar(
         backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        widget.name,
        style: Theme.of(context).textTheme.headline1,
      ),
      leading: InkWell(
        onTap: () => Navigator.pop(context),
        child: Image.asset('assets/images/arrow_back.png'),
      ),
    );

    return  NetworkIndicator(child:
    PageContainer(child:
     Scaffold(
         backgroundColor: greenColor,
      appBar: _appBar,
      body: _buildBodyWidget(),
    )));
  }

  Widget _buildBodyWidget() {
    return FutureBuilder<UserAds>(
        future: Provider.of<UserAdsProvider>(context, listen: false)
            .getAnnouncedProfile(widget.id,context),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.active:
              return Text('');
            case ConnectionState.waiting:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.done:
              if (snapshot.hasError) {
                debugPrint(snapshot.error.toString());
                return Center(
                  child: Text('حدث خطأ ما'),
                );
              } else {
                return ListView(
                  padding: EdgeInsets.symmetric(vertical: _height * 0.04),
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/checkbox.png',
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 5),
                          child: Text(
                            snapshot.data.name,
                            style: TextStyle(
                              color: Colors.white,
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: _height * 0.03),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(snapshot.data.createdAt,
                              style: TextStyle(fontSize: 13, color: whiteColor)),
                          Container(
                            color: whiteColor,
                            height: 20,
                            width: 1,
                            margin:
                                EdgeInsets.symmetric(horizontal: _width * 0.03),
                          ),
                          Text(
                            snapshot.data.createdAt,
                            style: TextStyle(fontSize: 13, color: whiteColor),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        InkWell(onTap: () {
                          final provider = Provider.of<FollowingProvider>(
                              context,
                              listen: false);
                          provider.toggleFollowing(widget.id.toString());
                          if (provider.listIdsOfFollowing
                              .contains(int.parse(widget.id))) {
                            //2
                            provider
                                .removeFromListFollowing(int.parse(widget.id));
                            Commons.showToast(
                                message: 'تم الحذف من قائمه المتابعه',
                                context: context);
                          } else {
                            provider.addToListFollowing(int.parse(widget.id));
                            Commons.showToast(
                                message: 'تم الأضافه لقائمه المتابعه',
                                context: context);
                          }
                        }, child: Consumer<FollowingProvider>(
                          builder: (context, provider, _) {
                            return Container(
                              height: 40,
                              width: _width * 0.3,
                              decoration: BoxDecoration(
                                color: provider.listIdsOfFollowing
                                        .contains(int.parse(widget.id))
                                    ? mainAppColor
                                    : mainAppColor,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                              child: Center(
                                  child: Text(
                                !provider.listIdsOfFollowing
                                        .contains(int.parse(widget.id))
                                    ? 'متابعة'
                                    : 'الغاء المتابعة',
                                style: TextStyle(
                                    color: provider.listIdsOfFollowing
                                            .contains(int.parse(widget.id))
                                        ? greenColor
                                        : whiteColor,
                                    fontWeight: FontWeight.bold),
                              )),
                            );
                          },
                        )),
                        InkWell(
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => EvaluationsScreen(
                                        id: widget.id,
                                        name: widget.name,
                                      ))),
                          child: Container(
                            height: 40,
                            width: _width * 0.3,
                            margin: EdgeInsets.only(left: 15),
                            decoration: BoxDecoration(
                              color:mainAppColor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            child: Container(
                              margin: EdgeInsets.only(right: 5),
                              child: Row(
                                children: [
                                Image.asset('assets/images/thumb-up-line.png',
                                color: whiteColor,),
                                SizedBox(width: 5,),
                                   Text(
                                  'أضف تقييم',
                                  style: TextStyle(
                                    fontSize: 14,
                                      color: whiteColor,
                                      fontWeight: FontWeight.bold),
                                ),
                                ],
                              )
                              
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ChatDetailsScreen(
                                      name: widget.name,
                                      id: widget.id,
                                    )),
                          ),
                          child: Container(
                            height: 40,
                            width: _width * 0.15,
                            decoration: BoxDecoration(
                              color: mainAppColor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            child: Image.asset('assets/images/mail_line.png',
                            color: whiteColor,),
                          ),
                        )
                      ],
                    ),
                    Divider(
                      height: _height * 0.05,
                    ),
                    Container(
                      height: _height * 0.7,
                      child: ListView.separated(
                        itemCount: snapshot.data.products.length,
                        physics: BouncingScrollPhysics(),
                        itemBuilder: (context, index) {
                          return Ad(
                            fromMyAds: widget.fromMyAds,
                            product: snapshot.data.products[index],
                          );
                        },
                        separatorBuilder: (context, index) {
                          return Divider();
                        },
                      ),
                    ),
                  ],
                );
              }
          }

          return Center(
            child: SpinKitDoubleBounce(
              color: mainAppColor,
            ),
          );
        });
  }
}
