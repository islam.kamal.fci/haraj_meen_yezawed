import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/validation_mixin.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';
import 'package:provider/provider.dart';

import '../../shared_preferences/shared_preferences_helper.dart';

class EditPasswordScreen extends StatefulWidget {
  @override
  _EditPasswordScreenState createState() => _EditPasswordScreenState();
}

class _EditPasswordScreenState extends State<EditPasswordScreen>
    with ValidationMixin {
  double _height, _width;
  AppBar _appBar;
  String _oldPassword, _newPassword;
  final _formKey = GlobalKey<FormState>();
  ApiProvider _apiProvider = ApiProvider();
  AuthProvider _authProvider;
  bool _isLoading = false, intialRun = true;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (intialRun) {
      _authProvider = Provider.of<AuthProvider>(context);
      intialRun = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;
    _appBar = AppBar(
         backgroundColor:mainAppColor,
      centerTitle: true,
      title: Text(
        'تغيير كلمة المرور',
        style: Theme.of(context).textTheme.headline1,
      ),
      leading: InkWell(
        onTap: () => Navigator.pop(context),
        child: Image.asset('assets/images/arrow_back.png'),
      ),
    );

    return  NetworkIndicator(child:PageContainer(child: Scaffold(
         backgroundColor: greenColor,
      appBar: _appBar,
      body: _buildBodyWidget(),
    )));
  }

  Widget _buildBodyWidget() {
    return SingleChildScrollView(
      child: Container(
        height: _height,
        width: _width,
        padding: EdgeInsets.only(top: 20),
        child: Form(
          key: _formKey,
          child: Column(
        
          children: [
            Column(
              children: [
                CustomTextFormField(
                  hintTxt: 'كلمه المرور القديمة',
                  validationFunc: validateOldPassword,
                  isPassword: true,
                  fillColor: whiteColor,

                  maxLines: 1,
                  onChangedFunc: (String value) => _oldPassword = value,
                ),
                SizedBox(
                  height: 10,
                ),
                CustomTextFormField(
                  hintTxt: 'كلمه المرور الجديدة',
                  validationFunc: validatePassword,
                  isPassword: true,
                   maxLines: 1,
                  fillColor: whiteColor,
                  onChangedFunc: (String value) => _newPassword = value,
                ),
                SizedBox(
                  height: 10,
                ),
                CustomTextFormField(
                  maxLines: 1,
                  fillColor: whiteColor,

                  hintTxt: 'تأكيد كلمه المرور الجديدة',
                  validationFunc: validateConfirmPassword,
                  isPassword: true,
               
                ),
              ],
            ),
            Spacer(
              flex: 1,
            ),
             _isLoading
        ? Center(
          child: SpinKitDoubleBounce(color: mainAppColor),
          )
        : 
            
                 CustomButton(
                     btnStyle: TextStyle(color: whiteColor),
                     btnLbl: 'حفظ',
                    onPressedFunction: () async {
                      if (_formKey.currentState.validate()) {
                        setState(() {
                          _isLoading = true;
                        });
                        var result = await _apiProvider
                            .post(Urls.UPDATE_PASSWORD_URL, body: {
                          "old_password": _oldPassword,
                          "password": _newPassword,
                          "password_confirmation": _newPassword
                        }, header: {
                          'Authorization':
                              'Bearer ${_authProvider.currentUser.accessToken}',
                          "Accept": "application/json",
                          "Content-Type": "application/json"
                        });
                        setState(() {
                          _isLoading = false;
                        });
                        if (result["status_code"] == 200) {
                          Commons.showToast(
                              message: result['response']['data'],
                              context: context,
                              color: hintColor);
                              Navigator.pop(context);

                      
                        }else if(result["status_code" ] == 401){
      Commons.showError(context: context,message: 'يرجى تسجيل الدخول مجدداً',
      onTapOk: (){
 Navigator.pop(context);

                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login_screen', (Route<dynamic> route) => false);
                        SharedPreferencesHelper.remove("user");
      });
                        }
       else {
                          Commons.showToast(
                              message: result['response']['error'],
                              context: context,
                              color: hintColor);
                        }
                      }
                    }),
       
       
        SizedBox(
            height: 80,
            ), 
          ],
        ),
      ),
    ));
  }
}
