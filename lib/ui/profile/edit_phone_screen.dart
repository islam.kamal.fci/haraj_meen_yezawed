import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/validation_mixin.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';
import 'package:provider/provider.dart';

import '../../shared_preferences/shared_preferences_helper.dart';

class EditPhoneScreen extends StatefulWidget {
  @override
  _EditPhoneScreenState createState() => _EditPhoneScreenState();
}

class _EditPhoneScreenState extends State<EditPhoneScreen>
    with ValidationMixin {
  double _height, _width;
  AppBar _appBar;
  String _oldPhone, _newPhone;
  final _formKey = GlobalKey<FormState>();
  ApiProvider _apiProvider = ApiProvider();
  AuthProvider _authProvider;
  bool _isLoading = false, intialRun = true;


  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (intialRun) {
      _authProvider = Provider.of<AuthProvider>(context);
      _oldPhone = _authProvider.currentUser.phone;
      intialRun = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _width = MediaQuery.of(context).size.width;
    _appBar = AppBar(
         backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        'تغيير رقم الجوال',
        style: Theme.of(context).textTheme.headline1,
      ),
      leading: InkWell(
        onTap: () => Navigator.pop(context),
        child: Image.asset('assets/images/arrow_back.png'),
      ),
    );

    return  NetworkIndicator(
        child:PageContainer(child:Scaffold(
         backgroundColor: greenColor,
      appBar: _appBar,
      body: _buildBodyWidget(),
    )));
  }

  Widget _buildBodyWidget() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                CustomTextFormField(
   hintTxt: '05xxxxxxxx',
                   labelText: 'رقم الجوال القديم',
                  fillColor: whiteColor,
                  validationFunc: validatePhoneNo,
                  initialValue: _oldPhone,
                  inputData: TextInputType.number,
                  onChangedFunc: (String val) => _oldPhone = val,
                ),
                SizedBox(
                  height: _height * 0.02,
                ),
                CustomTextFormField(
                     hintTxt: '05xxxxxxxx',
                   labelText: 'رقم الجوال الجديد',
                  fillColor: whiteColor,

                  validationFunc: validatePhoneNo,
                  inputData: TextInputType.number,
                  onChangedFunc: (String val) => _newPhone = val,
                ),
              ],
            ),
            _isLoading
                ? Container(
                    margin: EdgeInsets.only(
                        bottom: _height * 0.02, top: _height * 0.05),
                    child: SpinKitDoubleBounce(color: mainAppColor),
                  )
                : CustomButton(
                    btnLbl: 'حفظ',

                btnStyle: TextStyle(color: whiteColor),

                onPressedFunction: () async {
                      if (_formKey.currentState.validate()) {
                        setState(() {
                          _isLoading = true;
                        });
                        var result = await _apiProvider
                            .post(Urls.UPDATE_PHONE_URL, body: {
                          "old_phone": _oldPhone,
                          "phone": _newPhone,
                        }, header: {
                          'Authorization':
                              'Bearer ${_authProvider.currentUser.accessToken}',
                          "Accept": "application/json",
                          "Content-Type": "application/json"
                        });
                        setState(() {
                          _isLoading = false;
                        });
                        if (result["status_code"] == 200) {
                             FocusScope.of(context)
                                        .requestFocus(FocusNode());
                          Commons.showToast(
                              message: result['response']['data'],
                              context: context,
                              color: hintColor);

                          _authProvider.updateUserPhone(_newPhone);
                          Navigator.pop(context);
                        }
                        else if(result["status_code" ] == 401){
      Commons.showError(context: context,message: 'يرجى تسجيل الدخول مجدداً',
      onTapOk: (){
 Navigator.pop(context);

                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/login_screen', (Route<dynamic> route) => false);
                        SharedPreferencesHelper.remove("user");
      });
   
      } else {
                          Commons.showToast(
                              message: result['response']['error'],
                              context: context,
                              color: hintColor);
                        }
                      }
                    }),
          ],
        ),
      ),
    );
  }
}
