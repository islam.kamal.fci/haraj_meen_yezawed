import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/models/user_ads.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/providers/user_ads_provider.dart';
import 'package:haraj/ui/profile/widgets/edit_profile_item.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:provider/provider.dart';

class UserProfileScreen extends StatefulWidget {
  @override
  _UserProfileScreenState createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends State<UserProfileScreen> {
  double _height = 0;
  AppBar _appBar;
  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    _appBar = AppBar(
      backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        Provider.of<AuthProvider>(context, listen: false).currentUser.name,
        style: Theme.of(context).textTheme.headline1,
      ),
      leading: InkWell(
        onTap: () => Navigator.pop(context),
        child:  Image.asset('assets/images/arrow_back.png',color: whiteColor,),
      ),
    );

    return  NetworkIndicator(
        child:PageContainer(child:Scaffold(
      backgroundColor: greenColor,
      appBar: _appBar,
      body: _buildBodyWidget(),
    )));
  }

  Widget _buildBodyWidget() {
    return FutureBuilder<UserAds>(
        future:
            Provider.of<UserAdsProvider>(context, listen: false).getUserAds(context),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.active:
              return Text('');
            case ConnectionState.waiting:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.done:
              if (snapshot.hasError) {
                debugPrint(snapshot.error.toString());
                return Center(
                  child: Text('حدث خطأ ما'),
                );
              } else {
                return ListView(
                  padding: EdgeInsets.all(10),
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/checkbox.png',
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 5),
                          child: Text(
                            snapshot.data.name,
                            style: TextStyle(
                              color: Colors.white,
                                fontSize: 15, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                    Center(
                      child: Container(
                        margin: EdgeInsets.symmetric(vertical: _height * 0.03),
                        child: Text(snapshot.data.createdAt,
                            style: TextStyle(fontSize: 14, color: whiteColor)),
                      ),
                    ),
                    Container(
                      height: _height * 0.7,
                      child: ListView(
                        children: [
                          EditProfileItem(
                            title: 'تغيير مسمى العضوية',
                            route: '/edit_username_screen',
                          ),
                          Divider(),
                          EditProfileItem(
                            title: 'تغيير كلمة المرور',
                            route: '/edit_password_screen',
                          ),
                          Divider(),
                          EditProfileItem(
                            title: 'تغيير رقم الجوال',
                            route: '/edit_phone_screen',
                          ),
                          Divider(),
                          EditProfileItem(
                            title: 'تغيير البريد الألكتروني',
                            route: '/edit_email_screen',
                          ),
                          Divider(),
                          EditProfileItem(
                            title: 'دفع عموله الموقع',
                            route: '/commission_screen',
                          ),
                          Divider(),
                        ],
                      ),
                    )
                  ],
                );
              }
          }

          return Center(
            child: SpinKitDoubleBounce(
              color: mainAppColor,
            ),
          );
        });
  }
}
