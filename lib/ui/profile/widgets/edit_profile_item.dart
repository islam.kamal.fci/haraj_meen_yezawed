import 'package:flutter/material.dart';
import 'package:haraj/utils/app_colors.dart';

class EditProfileItem extends StatefulWidget {
  final String title;

  final String route;

  const EditProfileItem({Key key, this.title, this.route}) : super(key: key);
  @override
  _EditProfileItemState createState() => _EditProfileItemState();
}

class _EditProfileItemState extends State<EditProfileItem> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constrains) {
      return InkWell(
        onTap: () {
          Navigator.pushNamed(context, widget.route);
        },
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 10),
          height: 30,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                widget.title,
                style: TextStyle(color: Colors.white, fontSize: 14),
              ),
              Container(
                child: Image.asset(
                  'assets/images/arrow_left.png',
                  color: whiteColor,
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}
