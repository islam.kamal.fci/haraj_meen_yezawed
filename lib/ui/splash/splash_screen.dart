import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/models/user.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/shared_preferences/shared_preferences_helper.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool _initalRun = true;
  AuthProvider _authProvider;
  VideoPlayerController _controller;
  bool _visible = false;
  Future initData() async {
    await Future.delayed(Duration(seconds: 4));
  }

  Future<Null> _checkIsLogin() async {
    var _userData = await SharedPreferencesHelper.read("user");
    if (_userData != null) {
      _authProvider.setCurrentUser(User.fromJson(_userData));
      Navigator.pushReplacementNamed(context, '/navigation');
      //your home page is loaded
    } else {
      //replace it with the login page
      Navigator.pushReplacementNamed(context, '/login_screen');
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_initalRun) {
      _authProvider = Provider.of<AuthProvider>(context);
      _initalRun = false;
    }
  }

  @override
  void initState() {
    super.initState();
    print("1");
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    print("2");

    _controller = VideoPlayerController.asset("assets/video/splash_video.mp4");
    print("3");

    _controller.initialize().then((_) {
      _controller.setLooping(true);
      Timer(Duration(milliseconds: 100), () {
        setState(() {
          _controller.play();
          _visible = true;
        });
      });
    });
    print("4");
    initData().then((value) {
      _checkIsLogin();
    });
  }

  @override
  void dispose() {
    super.dispose();
    print("6");

    if (_controller != null) {
      _controller.dispose();
      _controller = null;
    }
  }

  _getVideoBackground() {
    print("7");

    return AnimatedOpacity(
      opacity: _visible ? 1.0 : 0.0,
      duration: Duration(milliseconds: 1000),
      child: VideoPlayer(_controller),
    );
  }

  _getBackgroundColor() {
    return Container(color: Colors.transparent //.withAlpha(120),
    );
  }

  _getContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
    );
  }

  @override
  Widget build(BuildContext context) {

    return PageContainer(child:Scaffold(
      backgroundColor:greenColor,
      body: Center(
        child: Stack(
          children: <Widget>[
            _getVideoBackground(),
          ],
        ),
      ),
    ));
  }
}
