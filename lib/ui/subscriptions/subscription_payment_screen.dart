import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/custom_text_form_field.dart';
import 'package:haraj/custom_widgets/custom_text_form_field/validation_mixin.dart';
import 'package:haraj/custom_widgets/dialogs/subcribtion_confirmation_dialog.dart';
import 'package:haraj/custom_widgets/drop_down_list_selector/drop_down_list_selector.dart';
import 'package:haraj/custom_widgets/no_data/no_data.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/locale/app_localizations.dart';
import 'package:haraj/models/bank.dart';
import 'package:haraj/networking/api_provider.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/providers/bank_transfer_provider.dart';
import 'package:haraj/providers/subscription_provider.dart';
import 'package:haraj/shared_preferences/shared_preferences_helper.dart';
import 'package:haraj/ui/payment/gateway_payment_screen.dart';
import 'package:haraj/ui/subscriptions/subscriptions_screen.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:haraj/utils/commons.dart';
import 'package:haraj/utils/urls.dart';
import 'package:provider/provider.dart';
import 'package:haraj/utils/error.dart';
import 'dart:convert';

import 'package:crypto/crypto.dart';

class SubscriptionPaymentScreen extends StatefulWidget {
  final int subPlanId;

  const SubscriptionPaymentScreen({Key key, this.subPlanId}) : super(key: key);
  @override
  _SubscriptionPaymentScreenState createState() =>
      _SubscriptionPaymentScreenState();
}

class _SubscriptionPaymentScreenState extends State<SubscriptionPaymentScreen>
    with ValidationMixin {
  double _width;
  ApiProvider _apiProvider = ApiProvider();
  String _amount = '', _dateTransfer = '', _moneySender = '';
  bool _initialRun = true;
  Bank _selectedBank;
  bool _isLoading = false;
  bool _initalSelectedBank = true;
  Future<List<Bank>> _bankList;
  var _bankListForDropDown;
  final _formKey = GlobalKey<FormState>();
  AuthProvider _authProvider;
  BankTransferProvider _bankTransferProvider;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_initialRun) {
      _bankTransferProvider = Provider.of<BankTransferProvider>(context);
      _authProvider = Provider.of<AuthProvider>(context);

      _bankList = _bankTransferProvider.getBankList(context);
      _initialRun = false;
    }
  }

  Widget _buildBodyItem() {
    return SingleChildScrollView(
        child: Form(
            key: _formKey,
            child: Column(
              children: [
                Consumer<SubscriptionProvider>(
                    builder: (context, subscriptionProvider, child) {
                  return Row(
                    children: [
                      InkWell(
                        onTap: () =>
                            subscriptionProvider.setBankTransferIsEnabled(true),
                        child: Container(
                          height: 50,
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: 20, left: 10),
                                width: 20,
                                height: 20,
                                child:
                                    subscriptionProvider.bankTransferIsEnabled
                                        ? Icon(
                                            Icons.check,
                                            color: Colors.white,
                                            size: 17,
                                          )
                                        : Container(),
                                decoration: BoxDecoration(
                                  color:
                                      subscriptionProvider.bankTransferIsEnabled
                                          ? mainAppColor
                                          : Colors.white,
                                  border: Border.all(
                                    color: subscriptionProvider
                                            .bankTransferIsEnabled
                                        ? mainAppColor
                                        : hintColor,
                                  ),
                                  borderRadius: BorderRadius.circular(25.0),
                                ),
                              ),
                              Text(
                                'الدفع بالحوالة البنكية',
                                style: TextStyle(
                                    color: subscriptionProvider
                                            .bankTransferIsEnabled
                                        ? mainAppColor
                                        : Colors.white.withOpacity(0.6),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700),
                              )
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          subscriptionProvider
                            .setBankTransferIsEnabled(false);
                                                                 var bytes1 = utf8.encode("haraj" +
                                            _authProvider.currentUser.id
                                                .toString() +
                                            "almotamiz"); // data being hashed
                                        var hashValue = sha256
                                            .convert(bytes1); // Hashing Process

                                        print("hashValue: $hashValue");
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    GatewayPaymentScreen(
                                                      onTapBackFunction: (){
                                                        subscriptionProvider.setBankTransferIsEnabled(true);
                                                        Navigator.pop(context);
                                                      },
                                                        checkoutUrl:
                                                            'https://harajalmotamiz.com.sa/payment/first-step/' +
                                                                _authProvider
                                                                    .currentUser
                                                                    .id
                                                                    .toString() +
                                                                "/" +
                                                                hashValue
                                                                    .toString()+"/"+
                                                                    widget.subPlanId.toString()
                                                                    )));
                        },
                        child: Container(
                          height: 50,
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: 20, left: 10),
                                width: 20,
                                height: 20,
                                child:
                                    !subscriptionProvider.bankTransferIsEnabled
                                        ? Icon(
                                            Icons.check,
                                            color: Colors.white,
                                            size: 17,
                                          )
                                        : Container(),
                                decoration: BoxDecoration(
                                  color: !subscriptionProvider
                                          .bankTransferIsEnabled
                                      ? mainAppColor
                                      : Colors.white,
                                  border: Border.all(
                                    color: !subscriptionProvider
                                            .bankTransferIsEnabled
                                        ? mainAppColor
                                        : hintColor,
                                  ),
                                  borderRadius: BorderRadius.circular(25.0),
                                ),
                              ),
                              Text(
                                'الدفع ببطاقة بمدى',
                                style: TextStyle(
                                    color: !subscriptionProvider
                                            .bankTransferIsEnabled
                                        ? mainAppColor
                                        : Colors.white.withOpacity(0.6),
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  );
                }),
                FutureBuilder<List<Bank>>(
                    future: _bankList,
                    builder: (context, snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                          return Center(
                            child: SpinKitPulse(color: mainAppColor),
                          );
                        case ConnectionState.active:
                          return Text('');
                        case ConnectionState.waiting:
                          return Center(
                            child: SpinKitFadingCircle(color: mainAppColor),
                          );
                        case ConnectionState.done:
                          if (snapshot.hasError) {
                            return Error(
                                errorMessage: snapshot.error.toString());
                            // errorMessage: AppLocalizations.of(context)
                            //     .translate('error'));
                          } else {
                            if (snapshot.data.length > 0) {
                              if (_initalSelectedBank) {
                                _bankListForDropDown =
                                    snapshot.data.map((item) {
                                  return new DropdownMenuItem<Bank>(
                                    child: new Text(item.bankName),
                                    value: item,
                                  );
                                }).toList();
                                _selectedBank = snapshot.data[0];
                                _initalSelectedBank = false;
                              }

                              return Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.symmetric(vertical: 10),
                                    child: DropDownListSelector(
                                      dropDownList: _bankListForDropDown,
                                      hint: 'اختر البنك',
                                      onChangeFunc: (newValue) {
                                        FocusScope.of(context)
                                            .requestFocus(FocusNode());
                                        setState(() {
                                          _selectedBank = newValue;
                                        });
                                      },
                                      value: _selectedBank,
                                    ),
                                  ),
                                  Container(
                                      height: 150,
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 10),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              RichText(
                                                text: TextSpan(
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      fontFamily: 'ArefRuqaa',
                                                      color: hintColor,
                                                    ),
                                                    children: [
                                                      TextSpan(
                                                          text: 'اسم البنك : '),
                                                      TextSpan(
                                                          text: _selectedBank
                                                                  .bankName ??
                                                              '',
                                                          style: TextStyle(
                                                              color: whiteColor,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)),
                                                    ]),
                                              ),
                                              SizedBox(height: 8),
                                              RichText(
                                                text: TextSpan(
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      fontFamily: 'ArefRuqaa',
                                                      color: hintColor,
                                                    ),
                                                    children: [
                                                      TextSpan(
                                                          text:
                                                              'رقم الحساب : '),
                                                      TextSpan(
                                                          text: _selectedBank
                                                              .accountNumber,
                                                          style: TextStyle(
                                                              color: whiteColor,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)),
                                                    ]),
                                              ),
                                              SizedBox(height: 8),
                                              RichText(
                                                text: TextSpan(
                                                    style: TextStyle(
                                                      fontSize: 13,
                                                      fontFamily: 'ArefRuqaa',
                                                      color: hintColor,
                                                    ),
                                                    children: [
                                                      TextSpan(
                                                          text:
                                                              'رقم الايبان : '),
                                                      TextSpan(
                                                          text: _selectedBank
                                                              .internationalAccount,
                                                          style: TextStyle(
                                                              color: whiteColor,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)),
                                                    ]),
                                              ),
                                            ],
                                          ),
                                          Image.network(
                                            _selectedBank.bankImage,
                                            width: _width * 0.25,
                                          )
                                        ],
                                      )),
                                ],
                              );
                            } else
                              return NoData(
                                  message: AppLocalizations.of(context)
                                      .translate('no_results'));
                          }
                          return Center(
                            child: SpinKitFadingCircle(color: mainAppColor),
                          );
                      }
                      return Center(
                        child: SpinKitFadingCircle(color: mainAppColor),
                      );
                    }),
                CustomTextFormField(
                  enabled: false,
                  fillColor: whiteColor,
                  labelStyle: TextStyle(color: mainAppColor),
                  initialValue: _authProvider.currentUser.name,
                  labelText: 'اسم المحول',
                  enableBorder: false,
                  inputData: TextInputType.text,
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: CustomTextFormField(
                    onChangedFunc: (String value) {
                      _amount = value;
                    },
                    fillColor: whiteColor,
                    labelStyle: TextStyle(color: mainAppColor),
                    labelText: 'قيمة المبلغ الذي تم تحويله',
                    enableBorder: false,
                    validationFunc: validateCommissionAmount,
                    suffixIcon: Column(
                      children: [
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          'ريال',
                          style: TextStyle(color: Colors.black, fontSize: 13),
                        )
                      ],
                    ),
                    inputData: TextInputType.number,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: CustomTextFormField(
                    labelText: 'متى تم التحويل',
                    fillColor: whiteColor,
                    labelStyle: TextStyle(color: mainAppColor),
                    validationFunc: validateConverterTime,
                    enableBorder: false,
                    onChangedFunc: (String value) {
                      _dateTransfer = value;
                    },
                    inputData: TextInputType.datetime,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: CustomTextFormField(
                    labelText: 'أسم المحول',
                    fillColor: whiteColor,
                    labelStyle: TextStyle(color: mainAppColor),
                    enableBorder: false,
                    validationFunc: validateConverterName,
                    onChangedFunc: (String value) {
                      _moneySender = value;
                    },
                    inputData: TextInputType.text,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: CustomTextFormField(
                    initialValue: _authProvider.currentUser.phone,
                    labelText: 'رقم الجوال المرتبط بعضويتك',
                    fillColor: whiteColor,
                    labelStyle: TextStyle(color: mainAppColor),
                    hintTxt: '05xxxxxxxx',
                    validationFunc: validatePhoneNo,
                    enableBorder: false,
                    enabled: false,
                    inputData: TextInputType.number,
                  ),
                ),
                _isLoading
                    ? SpinKitDoubleBounce(color: mainAppColor)
                    : Container(
                        margin: EdgeInsets.symmetric(vertical: 40),
                        child: CustomButton(
                            btnLbl: 'دفع',
                            btnStyle: TextStyle(color: whiteColor),
                            onPressedFunction: () {
                              if (_formKey.currentState.validate())
                                _sendBankTransfer();
                            }),
                      )
              ],
            )));
  }

  void _sendBankTransfer() async {
    if (_formKey.currentState.validate()) {
      FocusScope.of(context).requestFocus(FocusNode());

      setState(() {
        _isLoading = true;
      });
      var result = await _apiProvider.post(
          Urls.PLANS_URL + '/' + widget.subPlanId.toString() + '/subscribe',
          body: {
            "name": _authProvider.currentUser.name,
            "amount": _amount,
            "bank_name": _selectedBank.bankName,
            "datetransfer": _dateTransfer,
            "moneysender": _moneySender,
            "phone": _authProvider.currentUser.phone,
          },
          header: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer ${_authProvider.currentUser.accessToken}'
          });
      print("result : ${result}");
      setState(() {
        _isLoading = false;
      });
      if (result["status_code"] == 200) {
        showDialog(
            context: context,
            barrierDismissible: true,
            builder: (_) => SubcribtionConfirmationDialog());

        Future.delayed(const Duration(seconds: 2), () {
          Navigator.pop(context);
          Navigator.pop(context);
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => SubscriptionsScreen()));
        });
      }
      else if (result["status_code"] == 401)
        Commons.showError(
            context: context,
            message: 'يرجى تسجيل الدخول مجدداً',
            onTapOk: () {
              Navigator.pop(context);

              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/login_screen', (Route<dynamic> route) => false);
              SharedPreferencesHelper.remove("user");
            });
      else if(result["status_code"] == 422)
        Commons.showToast(
            message: result['response']['amount'].toString().substring(1,result['response']['amount'].toString().length-1),
            context: context,
            color: hintColor);
      else
        Commons.showToast(
            message: result['response']['error'],
            context: context,
            color: hintColor);
    }
  }

  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        'دفع الإشتراك',
        style: Theme.of(context).textTheme.headline1,
      ),
      leading: InkWell(
        onTap: () => Navigator.pop(context),
        child: Image.asset(
          'assets/images/arrow_back.png',
          color: whiteColor,
        ),
      ),
    );
    _width = MediaQuery.of(context).size.width;
    return NetworkIndicator(
        child: PageContainer(
            child: Scaffold(
      backgroundColor: greenColor,
      appBar: appBar,
      body: _buildBodyItem(),
    )));
  }
}
