import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';
import 'package:haraj/custom_widgets/no_data/no_data.dart';
import 'package:haraj/custom_widgets/safe_area/page_container.dart';
import 'package:haraj/locale/app_localizations.dart';
import 'package:haraj/models/sub_plan.dart';
import 'package:haraj/models/subscription_plan.dart';
import 'package:haraj/providers/subscription_provider.dart';
import 'package:haraj/ui/subscriptions/widgets/subscription_item.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:provider/provider.dart';
import 'package:haraj/utils/error.dart';


class SubscriptionsScreen extends StatefulWidget {
  SubscriptionsScreen({Key key}) : super(key: key);

  @override
  _SubscriptionsScreenState createState() => _SubscriptionsScreenState();
}

class _SubscriptionsScreenState extends State<SubscriptionsScreen>
    with TickerProviderStateMixin {
  double _width;
  AnimationController _animationController;
  bool _initialRun = true;
  Future<SubscriptionPlan> _currentSubscriptionPlan;
  SubscriptionProvider _subscriptionProvider;
    Future<List<SubPlan>> _subscriptionPlanList;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    if (_initialRun) {
      _subscriptionProvider = Provider.of<SubscriptionProvider>(context);
      _currentSubscriptionPlan =
          _subscriptionProvider.getCurrentSubscriptionPlan(context);
        _subscriptionPlanList = _subscriptionProvider.getSubScriptionPlanList(context) ; 
      _initialRun = false;
    }
  }

  @override
  void initState() {
    _animationController = AnimationController(
        duration: Duration(milliseconds: 2000), vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  Widget _buildBodyItem() {
    return Container(
      width: _width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        children: [
          FutureBuilder<SubscriptionPlan>(
              future: _currentSubscriptionPlan,
              builder: (context, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                    return Center(
                      child: SpinKitFadingCircle(color: mainAppColor),
                    );
                  case ConnectionState.active:
                    return Text('');
                  case ConnectionState.waiting:
                    return Center(
                      child: SpinKitFadingCircle(color: mainAppColor),
                    );
                  case ConnectionState.done:
                    if (snapshot.hasError)
                      return Error(
                          errorMessage:snapshot.error.toString(),
                          
                              // AppLocalizations.of(context).translate('error')
                              );
                    else {
                      return   Container(
                              padding: EdgeInsets.all(15),
                              margin: EdgeInsets.symmetric(
                                  vertical: 15, horizontal: _width * 0.05),
                              decoration: BoxDecoration(
                                color: Color(0xff1E1C1C),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10.00),
                                ),
                              ),
                              child: Row(
                                children: [
                                  Image.asset('assets/images/dim.png'),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'إشتراكك الحالي',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 13,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                   snapshot.data != null ?
                                        snapshot.data.name : 'لايوجد اشتراك حالي',
                                        style: TextStyle(
                                            color: mainAppColor,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w700),
                                      )
                                    ],
                                  ),
                                  Spacer(),
                                 snapshot.data != null ?   Text(
                                      ' ينتهي في ' +
                                          _subscriptionProvider.endDate,
                                      style: TextStyle(
                                          color: Colors.white.withOpacity(0.6),
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500))
                                          : Container()
                                ],
                              ),
                            );
                         
                    }
                }
                return Center(
                  child: SpinKitFadingCircle(color: mainAppColor),
                );
              }),
           Expanded(
             child: FutureBuilder<List<SubPlan>>(
                  future: _subscriptionPlanList,
                  builder: (context, snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                        return Center(
                          child: SpinKitFadingCircle(color: mainAppColor),
                        );
                      case ConnectionState.active:
                        return Text('');
                      case ConnectionState.waiting:
                        return Center(
                          child: SpinKitFadingCircle(color: mainAppColor),
                        );
                      case ConnectionState.done:
                        if (snapshot.hasError) 
                          return Error(
                               errorMessage: snapshot.error.toString(),
                              // errorMessage: AppLocalizations.of(context)
                              //     .translate('error')
                                  );
                        else {
                          if (snapshot.data.length > 0) 
                            return ListView.separated(
                              itemCount: snapshot.data.length,
                              itemBuilder: (BuildContext context, int index) {
                                var count = snapshot.data.length;
                                var animation =
                                    Tween(begin: 0.0, end: 1.0).animate(
                                  CurvedAnimation(
                                    parent: _animationController,
                                    curve: Interval((1 / count) * index, 1.0,
                                        curve: Curves.fastOutSlowIn),
                                  ),
                                );
                                _animationController.forward();

                                return SubScriptionItem(
                                  animation: animation,
                                  animationController: _animationController,
                                  subPlan:snapshot.data[index] ,
                                );
                              },
                              separatorBuilder: (context, index) {
                                return Divider(
                                  color: Colors.white,
                                );
                              },
                            );
                           else 
                            return Center(
                              child: NoData(
                                  message: AppLocalizations.of(context)
                                      .translate('no_results')),
                            );
                          
                        }
                    }
                    return Center(
                      child: SpinKitFadingCircle(color: mainAppColor),
                    );
                  }),
           )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        'الإشتراكات',
        style: Theme.of(context).textTheme.headline1,
      ),
      leading: InkWell(
        onTap: () => Navigator.pop(context),
        child: Image.asset(
          'assets/images/arrow_back.png',
          color: mainAppColor,
        ),
      ),
    );
    _width = MediaQuery.of(context).size.width;
    return NetworkIndicator(
        child: PageContainer(
            child: Scaffold(
      backgroundColor: greenColor,
      appBar: appBar,
      body: _buildBodyItem(),
    )));
  }
}
