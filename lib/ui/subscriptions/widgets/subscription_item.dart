import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/models/sub_plan.dart';
import 'package:haraj/ui/subscriptions/subscription_payment_screen.dart';

import 'package:haraj/utils/app_colors.dart';

class SubScriptionItem extends StatelessWidget {
  final SubPlan subPlan;
  final AnimationController animationController;
  final Animation animation;
  const SubScriptionItem(
      {Key key, this.subPlan, this.animationController, this.animation})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return AnimatedBuilder(
        animation: animationController,
        builder: (BuildContext context, Widget child) {
          return FadeTransition(
              opacity: animation,
              child: new Transform(
                  transform: new Matrix4.translationValues(
                      0.0, 50 * (1.0 - animation.value), 0.0),
                  child: Container(
                    width: width,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            margin:
                                EdgeInsets.symmetric(horizontal: width * 0.04),
                            child: Image.asset('assets/images/digold.png')),
                        Container(
                            width: width * 0.82,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      subPlan.name,
                                      style: TextStyle(
                                          color: mainAppColor,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    Spacer(),
                                    subPlan.priceAfterDiscount.trim().length > 0
                                        ? Text(
                                            subPlan.priceAfterDiscount + ' SR',
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: mainAppColor,
                                                fontWeight: FontWeight.w600),
                                          )
                                        : Container(),
                                    SizedBox(
                                      width: width * 0.03,
                                    ),
                                    Text(
                                      subPlan.price + ' SR',
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: subPlan.priceAfterDiscount
                                                      .trim()
                                                      .length >
                                                  0
                                              ? Colors.white.withOpacity(0.6)
                                              : mainAppColor,
                                          decoration: subPlan.priceAfterDiscount
                                                      .trim()
                                                      .length >
                                                  0
                                              ? TextDecoration.lineThrough
                                              : TextDecoration.none,
                                          fontWeight: FontWeight.w600),
                                    ),
                                    SizedBox(
                                      width: width * 0.03,
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Icon(
                                      FontAwesomeIcons.clock,
                                      size: 20,
                                      color: Colors.white.withOpacity(0.6),
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      ' لمدة ' +
                                          subPlan.durationInDays +
                                          ' يوم ',
                                      style: TextStyle(
                                          color: Colors.white.withOpacity(0.6),
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500),
                                    )
                                  ],
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(vertical: 10),
                                  child: Text(
                                    subPlan.description ?? '',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 11,
                                        height: 1.6),
                                  ),
                                ),
                                Container(
                                  height: 60,
                                  child: CustomButton(
                                      enableHorizontalMargin: false,
                                      btnLbl: 'إشترك الأن',
                                      onPressedFunction: () => Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SubscriptionPaymentScreen(
                                                    subPlanId: subPlan.id,
                                                  )))),
                                )
                              ],
                            )),
                      ],
                    ),
                  )));
        });
  }
}
