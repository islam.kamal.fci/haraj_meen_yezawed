import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:haraj/models/page_content.dart';
import 'package:haraj/providers/static_pages_provider.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:provider/provider.dart';
import 'package:haraj/custom_widgets/connectivity/network_indicator.dart';


class TermsAndRulesScreen extends StatefulWidget {
  @override
  _TermsAndRulesScreenState createState() => _TermsAndRulesScreenState();
}

class _TermsAndRulesScreenState extends State<TermsAndRulesScreen> {
  double _height;
  AppBar _appBar;
  @override
  Widget build(BuildContext context) {
    _height =
        MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    // _width = MediaQuery.of(context).size.width;
    _appBar = AppBar(
         backgroundColor: mainAppColor,
      centerTitle: true,
      title: Text(
        'الشروط و الأحكام',
        style: Theme.of(context).textTheme.headline1,
      ),
      leading: InkWell(
        onTap: () => Navigator.pop(context),
        child: Image.asset('assets/images/arrow_back.png'),
      ),
    );

    return  NetworkIndicator(child: Scaffold(
         backgroundColor: greenColor,
      appBar: _appBar,
      body: _buildBodyWidget(),
    ));
  }

  Widget _buildBodyWidget() {
    return FutureBuilder<PageContent>(
        future:
            Provider.of<StaticPagesProvider>(context, listen: false)
            .getTerms(context),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.active:
              return Text('');
            case ConnectionState.waiting:
              return Center(
                child: SpinKitDoubleBounce(
                  color: mainAppColor,
                ),
              );
            case ConnectionState.done:
              if (snapshot.hasError) {
                print(snapshot.error.toString());
                return Center(
                  child: Text('حدث خطأ ما'),
                );
              } else {
                return ListView(
                  padding: EdgeInsets.all(10),
                  physics: BouncingScrollPhysics(),
                  children: [
                    Image.asset(
                      'assets/images/tlogo.png',
                      height: _height * 0.15,
                    ),
                    Container(
                        margin: EdgeInsets.symmetric(vertical: _height * 0.02),
                        child: Text(
                          snapshot.data.content[0].body,
                          style: TextStyle(fontSize: 14,
                          height: 1.4,
                          color: Colors.white
                          ) ,
                        ))
                  ],
                );
              }
          }
          return Center(
            child: SpinKitDoubleBounce(
              color: mainAppColor,
            ),
          );
        });
  }
}
