import 'package:flutter/material.dart';

import 'hex_color.dart';

final mainAppColor = HexColor('88C4EC');
final secondary_color = HexColor('816854');
final hintColor = HexColor('B2B6BF');
final blackColor = HexColor('000000');
final whiteColor = HexColor('FFFFFF');
final redColor = HexColor('BB0000');
final whiteGreyColor = HexColor('F6F6F6');
const greyColor = Color(0xffA0AEC0);
const greenColor = Color(0xFF74C384);
const small_grey_color= Color(0xFFA2A2A2);
// final chipColor = HexColor('F8F7F9');
