import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:haraj/custom_widgets/buttons/custom_button.dart';
import 'package:haraj/providers/auth_provider.dart';
import 'package:haraj/ui/home/home_screen.dart';
import 'package:haraj/utils/app_colors.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';

class Commons {
  static HomeScreenState homeScreenState;
  static void showError(
      {@required BuildContext context,
      @required String message,
      Function onTapOk}) {
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              title: Text(
                message,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w700,
                    height: 1.7),
                textAlign: TextAlign.center,
              ),
              backgroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(15)),
              actions: <Widget>[
                FlatButton(
                    child: Text('موافق'),
                    textColor: Colors.black,
                    onPressed: () => onTapOk != null
                        ? onTapOk()
                        : Navigator.of(context).pop()),
              ],
            ));
  }

  static void showLogoutDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              title: Text(
                "تأكيد تسجيل الخروج؟",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                FlatButton(
                  onPressed: () => Navigator.pop(context),
                  child: Text(
                    'لا',
                    style: TextStyle(color: mainAppColor),
                  ),
                ),
                Consumer<AuthProvider>(
                  builder: (context, provider, _) {
                    return FlatButton(
                      onPressed: () => provider.signOut(context),
                      child: Text(
                        'نعم',
                        style: TextStyle(color: mainAppColor),
                      ),
                    );
                  },
                )
              ],
            ));
  }

  static void showMessageDialog({BuildContext context, String msg}) {
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(backgroundColor: greenColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              title: Image.asset(
                'assets/images/cancel.png',
                height: 25,
              ),
              content: Text(
                msg,
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                FlatButton(
                  onPressed: () => Navigator.pop(context),
                  child: Text('اغلاق'),
                )
              ],
            ));
  }

  static void showMessageDialogNoImg({BuildContext context, String msg}) {
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          backgroundColor: greenColor,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              title: CircleAvatar(
                maxRadius: 40,
                minRadius: 40,
                backgroundColor: hintColor,
                child: Center(
                  child: Icon(
                    Icons.check,
                    color: Colors.greenAccent,
                    size: 45,
                  ),
                ),
              ),
              content: Text(
                msg,
                style: TextStyle(fontSize: 14,color: whiteColor),
              ),
              actions: [
                FlatButton(
                  onPressed: () => Navigator.pop(context),
                  child: Text('اغلاق'),
                )
              ],
            ));
  }

  static showModelSheet(
      {String msg,
      String imgUrl,
      Function ok,
      Function onPressCancel,
      BuildContext context,
      double height,
      double width}) {
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: const Radius.circular(15.0),
                topRight: const Radius.circular(15.0))),
        context: context,
        builder: (context) {
          return Container(
              height: height * 0.35,
              decoration: BoxDecoration(
                color: greenColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10.0),
                  topRight: Radius.circular(10.0),
                ),
              ),
              child: Column(
                children: [
                  Image.asset(
                    imgUrl,
                    height: 90,
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: height * 0.03),
                    child: Text(
                      msg,
                      style: TextStyle(
                          fontSize: 14,
                          color: whiteColor,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    height: height * 0.08,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: width * 0.5,
                          child: CustomButton(
                            onPressedFunction: () async {
                              Navigator.pop(context);
                              await ok();
                            },
                            btnLbl: 'نعم',
                            btnStyle: TextStyle(
                                color: whiteColor,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                        ),

                        //   InkWell(
                        //   onTap: () async {
                        //           Navigator.pop(context);
                        //     await ok();
                        //   },
                        //   child: Container(
                        //     decoration: BoxDecoration(
                        //       color: mainAppColor,
                        //       // borderRadius: BorderRadius.all(15.0)
                        //     ),
                        //     width: width *0.2,
                        //     // margin: EdgeInsets.symmetric(
                        //     //     horizontal: width * 0.07,
                        //     //     vertical: height * 0.01),
                        //     child: Text(
                        //       'نعم',
                        //       style: TextStyle(
                        //           color: blackColor,
                        //           fontSize: 14,
                        //           fontWeight: FontWeight.bold),
                        //     ),
                        //   ),
                        // )

                        InkWell(
                          onTap: () => onPressCancel != null
                              ? onPressCancel()
                              : Navigator.of(context).pop(),
                          child: Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: width * 0.07,
                                vertical: height * 0.01),
                            child: Text(
                              'لا, شكرا',
                              style: TextStyle(
                                  color: whiteColor,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ));
        });
  }

// handleUnauthenticated(BuildContext context) {
//   showDialog(
//       barrierDismissible: false, // user must tap button!
//       context: context,
//       builder: (_) {
//         return ResponseAlertDialog(
//           alertTitle: 'عفواً',
//           alertMessage: 'يرجي تسجيل الدخول مجدداً',
//           alertBtn: 'موافق',
//           onPressedAlertBtn: () {
//             Navigator.pop(context);
//             SharedPreferencesHelper.remove("user");
//             Navigator.of(context).pushNamedAndRemoveUntil(
//                 '/login_screen', (Route<dynamic> route) => false);
//           },
//         );
//       });
// }

  // showErrorDialog(var message, BuildContext context) {
  //   showDialog(
  //       barrierDismissible: false, // user must tap button!
  //       context: context,
  //       builder: (_) {
  //         return ResponseAlertDialog(
  //           alertTitle: 'عفواً',
  //           alertMessage: message,
  //           alertBtn: 'موافق',
  //           onPressedAlertBtn: () {},
  //         );
  //       });
  // }

  static void showToast(
      {@required String message, @required BuildContext context, Color color}) {
    return Toast.show(
      message,
      context,
      backgroundColor: color == null ? mainAppColor : color,
      duration: Toast.LENGTH_LONG,
      gravity: Toast.BOTTOM,
    );
  }
}
