import 'package:haraj/ui/ad/ad_details_screen.dart';
import 'package:haraj/ui/ad/my_ads_screen.dart';
import 'package:haraj/ui/auth/activate_account_screen.dart';
import 'package:haraj/ui/auth/change_password_screen.dart';
import 'package:haraj/ui/auth/cities_screen.dart';
import 'package:haraj/ui/auth/confirm_code_screen.dart';
import 'package:haraj/ui/auth/countries_screen.dart';
import 'package:haraj/ui/auth/forget_password_screen.dart';
import 'package:haraj/ui/auth/login_screen.dart';
import 'package:haraj/ui/auth/signup_screen.dart';
import 'package:haraj/ui/bottom_navigation/bottom_navigation_bar.dart';
import 'package:haraj/ui/evaluations/evaluations_screen.dart';
import 'package:haraj/ui/more/black_list_screen.dart';
import 'package:haraj/ui/more/check_number_screen.dart';
import 'package:haraj/ui/more/commission_calculate_screen.dart';
import 'package:haraj/ui/more/contact_us_screen.dart';
import 'package:haraj/ui/more/discount_system_screen.dart';
import 'package:haraj/ui/more/following_list_screen.dart';
import 'package:haraj/ui/more/payment_policy_screen.dart';
import 'package:haraj/ui/more/prohibited_goods_screen.dart';
import 'package:haraj/ui/more/usage_treaty_screen.dart';
import 'package:haraj/ui/notifications/notifications_screen.dart';
import 'package:haraj/ui/payment/bank_transfer_screen.dart';
import 'package:haraj/ui/profile/announced_profile_screen.dart';
import 'package:haraj/ui/profile/edit_email_screen.dart';
import 'package:haraj/ui/profile/edit_password_screen.dart';
import 'package:haraj/ui/profile/edit_phone_screen.dart';
import 'package:haraj/ui/profile/edit_username_screen.dart';
import 'package:haraj/ui/profile/user_profile_screen.dart';
import 'package:haraj/ui/splash/splash_screen.dart';
import 'package:haraj/ui/subscriptions/subscriptions_screen.dart';
import 'package:haraj/ui/terms/terms_and_rules_screen.dart';

final routes = {
  '/': (context) => SplashScreen(),
  '/navigation': (context) => BottomNavigation(),
  '/login_screen': (context) => LoginScreen(),
  '/signup_screen': (context) => SignupScreen(),
  '/change_password_screen': (context) => ChangePasswordScreen(),
  '/forget_password_screen': (context) => ForgetPasswordScreen(),
  '/confirmation_code_screen': (context) => ConfirmCodeScreen(),
  '/activate_account_screen': (context) => ActivateAccountScreen(),
  '/notifications_screen': (context) => NotificationsScreen(),
  '/myAds_screen': (context) => MyAdsScreen(),
  '/commission_screen': (context) => CommissionCalculate(),
  '/ad_details_screen': (context) => AdDetailsScreen(),
  '/payment_policy_screen': (context) => PaymenyPolicyScreen(),
  '/discount_system_screen': (context) => DiscountSystemScreen(),
  '/following_list_screen': (context) => FollowingListScreen(),
  '/prohibted_goods_screen': (context) => ProhibitedGoodsScreen(),
  '/usage_treaty_screen': (context) => UsageTreatyScreen(),
  '/black_list_screen': (context) => BlackListScreen(),
  // '/mada_payment_screen': (context) => MadaPaymentScreen(),
  '/bank_transfer_screen': (context) => BankTransferScreen(),
  '/announced_profile_screen': (context) => AnnouncedProfileScreen(),
  '/evaluations_screen': (context) => EvaluationsScreen(),
  '/edit_username_screen': (context) => EditUsernameScreen(),
  '/edit_password_screen': (context) => EditPasswordScreen(),
  '/edit_email_screen': (context) => EditEmailScreen(),
  '/edit_phone_screen': (context) => EditPhoneScreen(),
  '/terms_and_rules_screen': (contetx) => TermsAndRulesScreen(),
  '/countries_screen': (contetx) => CountriesScreen(),
  'contact_us_screen': (context) => ContactUsScreen(),
  'check_number_screen': (context) => CheckNumberScreen(),
  '/cities_screen': (context) => CitiesScreen(),
  '/user_profile_screen': (context) => UserProfileScreen(),
  '/subscriptions_screen': (context) => SubscriptionsScreen()
};
