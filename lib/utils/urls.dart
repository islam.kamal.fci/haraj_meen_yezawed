class Urls {
//  static const String BASE_URL = "https://harajalmotamiz.com.sa/api/v1/";
  static const String BASE_URL = "https://menyzod.com/api/v1/";
  static const String LOGIN_URL = BASE_URL + "login";
  static const String SIGNUP_URL = BASE_URL + "register";
  static const String ALL_COUNTRIES_URL = BASE_URL + "addresses/countries";
  static const String ALL_CITIES_URL = BASE_URL + "addresses/all-cities";
  static const String ALL_BANKS_URL = BASE_URL + "get-ways/bank-transfer-data";
  static const String CONFIRM_USER_URL = BASE_URL + "register-confirmation";
  static const String RESEND_CODE_URL = BASE_URL + "resend-code";
static const String CHECK_CODE_URL = BASE_URL + "check-code";
  static const String NEW_PASSWORD_URL = BASE_URL + "new-password";
  static const String NOTIFICATION_URL =
      BASE_URL + "notifications/notifications-on-status/all";
  static const String ALL_CATEGORIES_URL = BASE_URL + "categories/all";
  static const String SUB_CATEGORIES_URL =
      BASE_URL + "categories/sub-categories/";
  static const String CHILD_CATEGORIES_URL =
      BASE_URL + "categories/child-categories/";
  static const String LOGOUT_URL = BASE_URL + "logout";
  static const String FAVOURITE_URL =
      BASE_URL + "favourite/all-favourite-products";
  static const String UPDATE_EMAIL_URL = BASE_URL + "user/update-email";
  static const String SEARCH_ALL_AUTH_URL = BASE_URL + "search-auth/all?page=";
    static const String SEARCH_ALL_UNAUTH_URL = BASE_URL + "search/all?page=";
  static const String ALL_MY_PRODUCTS =
      BASE_URL + "user/products/all-my-products";
  static const String ANNOUNCED_ADS_URL =
      BASE_URL + "user/products/get-user-products/";
  static const String UPDATE_PASSWORD_URL = BASE_URL + "user/update-password";
  static const String UPDATE_PHONE_URL = BASE_URL + "user/update-phone";
  static const String UPDATE_NAME_URL = BASE_URL + "user/update-name";
  static const String STATIC_PAGES_URL = BASE_URL + "static-pages/all";
  static const String BLOCKED_USERS_URL =
      BASE_URL + "static-pages/blocked-user";
  static const String SHOW_PRODUCT_URL = BASE_URL + "product/show/";
    static const String SHOW_AUTH_PRODUCT_URL = BASE_URL + "product/show-auth/";
  static const String TERMS_URL = BASE_URL + "terms-and-conditions";
  static const String FOLLOWING_USERS_URL =
      BASE_URL + "user/following/all-follow-up";
  static const String FOLLOWING_ADS_URL =
      BASE_URL + "user/following/all-follow-up-products";
  static const String TOGGLE_FOLLOWING_URL =
      BASE_URL + "user/following/toggle-following/";
  static const String TOGGLE_AD_URL =
      BASE_URL + "user/following/toggle-following-products/";
  static const String CREATE_RATE_URL = BASE_URL + "user/rating/create";
  static const String GET_ANNOUNCED_RATINGS =
      BASE_URL + "user/rating/get-all-ratings/";
  static const String GET_COMMETION_DETAILS =
      BASE_URL + "static-pages/get-commission";
  static const String ALL_PRODUCTS_UNAUTHENTCATED_URL =
      BASE_URL + "home/all-categories-tree?page=";
        static const String ALL_PRODUCTS_AUTH_URL =
      BASE_URL + "home-auth/all-categories-tree?page=";
  static const String ADD_COMMENT = BASE_URL + "comments/add";
  static const String TOGGLE_FAV_URL = BASE_URL + "favourite/toggle/";
  static const String REPORT_PRODUCT = BASE_URL + "reports/create";
  static const String CREATE_PRODUCT = BASE_URL + "product/store";
  static const String DELETE_PRODUCT = BASE_URL + "product/destroy/";
  static const String REFRESH_PRODUCT_URL = BASE_URL + "product/refresh/";
  static const String UPDATE_PRODUCT = BASE_URL + "product/update";
  static const String ALL_USERS_CHATS = BASE_URL + "chat/all-user-chats";
  static const String SINGLE_CHAT_URL = BASE_URL + "chat/specific-chat/";
  static const String SEND_CHAT_URL = BASE_URL + "chat/send-chat-msg";
  static const String DELETE_CHAT_URL = BASE_URL + "chat/delete-single-chat/";
  static const String USER_DATA = BASE_URL + "user/auth-user";
  static const String DELETE_IMG_URL = BASE_URL + "product/delete-image";
  static const String CREATE_BANK_TRANSFER =
      BASE_URL + "get-ways/create-bank-transfer";
  static const String ALLOWED_IMGS_NO_URL =
      BASE_URL + "setting/allowed-images-number";
  static const String CURRENT_PLAN_URL = BASE_URL + "plans/check/current";
  static const String PLANS_URL = BASE_URL + "plans";
  static const String CONTACT_URL = BASE_URL + "contact-us";
}
